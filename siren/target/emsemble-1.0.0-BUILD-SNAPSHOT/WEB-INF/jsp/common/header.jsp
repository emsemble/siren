<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>${title}</title>
<c:set var="JS_DIR" value="static/js" />
<c:set var="CSS_DIR" value="static/css" />
<c:set var="IMG_DIR" value="static/img" />
<link rel="stylesheet" href="${CSS_DIR}/common.css" />
</head>
<body>
