<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<c:set var="title" value="Main" />
<%@include file="common/header.jsp"%>
<link rel="stylesheet" href="${CSS_DIR}/main.css" />
<link rel="stylesheet" href="${CSS_DIR}/bootstrap.min.css"/>

<div id="container" role="main" >
	
	<div id="header">
		<div id="logoArea" class="table_cell">
			<a><img src="${IMG_DIR}/logo.png" id="img_logo"></a>
		</div>
		
		<div id="siteNameArea" class="table_cell">
			<div id="siteName">seiren</div>
		</div>
		
		<div id="aboutArea" class="table_cell">
			<a href="#seiren" > <div id="aboutSeiren">About seiren</div> </a>
		</div>
		
		<div id="featureArea" class="table_cell">
			<a href="#feature"><div id="feature">Feature</div></a>
		</div>
		
		<div id="publisherArea" class="table_cell">
			<a href="#publisher"><div id="publisher">Publisher</div></a>
		</div>
		
		<div id="signUpArea" class="table_cell">
			<button type="button" class="btn1 btn-danger" id="signUp">SIGN UP</button>
		</div>
	</div><!-- header 끝 --> 
	
	<div id="content1">
		<div id="listingArea" class="table_cell">
			<div>
				<img src="${IMG_DIR}/musicListing.png" id="img_musicListing">		
			</div>
			<div id="userListingInfo"></div>
		</div>
		
		<div id="loginArea" class="table_cell">
			<div id="loginHead" >Make your Music Collection</div>
			
			<div id="idArea" >
				<div>
					<img  src="${IMG_DIR}/Mail-icon.png" id="img_mailIcon" class="cLoginArea">
				</div>
				
				<div id="idMsg" class="cLoginArea" >yourname@email.com</div>		
			</div>
			
			<div id="passwordArea">
				<div >
					<img  src="${IMG_DIR}/Lock-icon.png" id="img_LockIcon" class="cLoginArea">
				</div>
				<div id="pwMsg" class="cLoginArea">Password</div>
			</div>
			
			<div id="signInArea">
				<div id="signInMsg">SIGN IN</div>
			</div>
			
		</div>
	</div><!--content1 끝  -->	
	
	<a name="seiren"><div id="content2" >
		<div id="biArea"><img src="${IMG_DIR}/bi.png" id="img_bi"></div>
		<div id="seiren_subject">About Seiren</div>
		<div id="seiren_content">* 세이렌(SEIREN)은 그리스 신화에 나오는 매우 아름답지만 치명적인 마력을 가진 님프이다.<br>
				     								그녀들이 하는 일이란 배를 타고 지가는 선원들을 향해 노래를 불러 유혹하는 것이었다. *</div>
	</div></a><!-- content2 끝 -->
	
	<div id="content4" name="publisher">	
		<div id="publisher" >
			<span>ABOUT US</span>
			<nav id="developer_card_area">
			<%-- 	<ul>
  					<li class="developer_card"><img src="${IMG_DIR}/img1.jpg" ></li>
  					<li class="developer_card"><img src="${IMG_DIR}/img2.jpg"></li>
  					<li class="developer_card"><img src="${IMG_DIR}/img3.jpg"></li>
				</ul> --%>
			</nav>
		</div>
	</div><!-- content4 끝 -->
	
	<div id="footer">
	</div>
</div>

<%@include file="common/footer.jsp"%>

<!--Template Area  -->
<script type="text/template" id="tpl_click_idArea"  >
	<input type="text" id="idInputBox" style="width:300px;height:55px;background-color:#50597b;z-index:3;border-radius:5px;border-color:#2e343e;color:white;padding-left:10px;" >	
	<div id="warningIdLayer" style="display:none;width:300px;height:55px;background-color:#50597b;z-index:3;border-radius:5px;border-color:#2e343e;color:white;padding-left:10px;">아이디가 존재하지 않습니다.</div>	
</script>

<script type="text/template" id="tpl_click_pwArea"  >
	<input type="password" id="pwInputBox" style="width:300px;height:55px;background-color:#50597b;z-index:3;border-radius:5px;border-color:#2e343e;color:white;padding-left:10px;" >	
	<div id="warningPwLayer" style="display:none;width:300px;height:55px;background-color:#50597b;z-index:3;border-radius:5px;border-color:#2e343e;color:white;padding-left:10px;">비밀번호가 일치하지 않습니다.</div>	
</script>

<!-- // TemplateArea -->


<script src="${JS_DIR}/jindo.desktop.all.js"></script>
<script src="${JS_DIR}/jindo_component.js"></script>
<script src="${JS_DIR}/main.js"></script>

<script type="text/javascript">
var main = new Main();
</script>