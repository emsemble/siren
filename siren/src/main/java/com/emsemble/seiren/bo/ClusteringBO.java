package com.emsemble.seiren.bo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.emsemble.seiren.dao.SongDataDAO;

/**
 * 1. 파일 이름 : ClusteringBO
 * 
 * 2. 작성자 : 고상우
 * 
 * 3. 목적 : Visualization을 위한 좌표값 / 군집 데이터 생성
 * 
 * 4. 이력 :
 * 
 * 2014. 04. 12. - 최초 작성 , 상관계수 추출 메소드 작성
 * 
 * 2014. 04. 13. - 거리 추출 메소드 작성
 * 
 * 2014. 04. 14. - 클러스터 중심점 추출, 클러스터 형성 메소드 작성
 * 
 * 2014. 04. 15. - 거리 추출 함수 수정 : DB단 처리
 * 
 * 2014. 04. 19. - 좌표 결정 메소드 오류 수정 (grad 초기화 문제) / 1.0v 완성
 * 
 * 2014. 04. 25. - 조화도 측정을 위한 메소드 작성 / 좌표 랜덤 값 조정
 * 
 * 2014. 04. 28. - 연결선 개선
 */
@Component
public class ClusteringBO {

    @Autowired
    private SongDataDAO songDataDAO;



    /**
     * 목적 : 콜렉션에서 songList를 받아와 DB로 부터 상관계수 데이터를 받는다. -> 2중 해시맵 형태로 형태를 바꿔 반환한다.
     * 
     * HashMap <row : Integer, HashMap <col : Integer, correlation: Double>> ex) <1번곡 , <3번곡, 0.33>>
     */
    public HashMap<Integer, HashMap<Integer, Double>> getCorrelation(ArrayList<Integer> songNumList) throws SQLException {
        HashMap<Integer, HashMap<Integer, Double>> corrTbl = null;      // 결과값을 저장할 상관계수 테이블
        HashMap tempData = null;                                        // DB의 한 행 값을 임시로 저장할 변수


        Iterator<HashMap> rowItr = songDataDAO.selectCorrelation(songNumList).iterator();
        corrTbl = new HashMap<Integer, HashMap<Integer, Double>>(); // 윗 라인에서 익셉션이 발생하지 않았을때 실행되도록.

        while (rowItr.hasNext()) {
            tempData = rowItr.next();

            if (!corrTbl.containsKey(tempData.get("source"))) {
                corrTbl.put((Integer) tempData.get("source"), new HashMap<Integer, Double>());
            }// end of if : 해당 row값이 없을 때만 새로 만든다.
            corrTbl.get(tempData.get("source")).put((Integer) tempData.get("target"), (Double) tempData.get("correlation"));
        }// end of while
        return corrTbl;
    }// end ofgetCorrelation()



    /**
     * 목적 : 상관계수로 부터 거리를 계산해 낸다.
     * 
     * 1 - 상관계수 = 거리 (상관계수는 -1에서 1사이이므로, 거리는 0에서 2까지로 나타난다)
     */
    public HashMap<Integer, HashMap<Integer, Double>> getRealDistance(ArrayList<Integer> songNumList) throws SQLException{
        HashMap<Integer, HashMap<Integer, Double>> distance = null;      // 결과값을 저장할 상관계수 테이블
        HashMap tempData = null;                                        // DB의 한 행 값을 임시로 저장할 변수


        Iterator<HashMap> rowItr = songDataDAO.selectDistance(songNumList).iterator();
        distance = new HashMap<Integer, HashMap<Integer, Double>>(); // 윗 라인에서 익셉션이 발생하지 않았을때 실행되도록.

        while (rowItr.hasNext()) {
            tempData = rowItr.next();

            if (!distance.containsKey(tempData.get("source"))) {
                distance.put((Integer) tempData.get("source"), new HashMap<Integer, Double>());
            }// end of if : 해당 row값이 없을 때만 새로 만든다.
            distance.get(tempData.get("source")).put((Integer) tempData.get("target"), (Double) tempData.get("distance"));
        }// end of while
        return distance;
    }// end of getRealDistance();



    /**
     * 목적 : 상호 거리에 기반해 좌표값을 뽑아낸다.
     * 
     * 곡마다 무작위로 좌표값을 부여한다 -> 투영된 좌표값에서 거리를 추출한다 -> 실제 거리와 비교한다. -> 오차를 추출해 거리를 조정해본다 -> 거리 조정전과 거리
     * 조정후의 결과를 비교해 더 좋은 좌표값을 선택한다 -> 조정의 여지가 있으면 반복 연산한다.
     * 
     */
    public HashMap<Integer, double[]> getLocation(ArrayList<Integer> songNumList, HashMap<Integer, HashMap<Integer, Double>> realDistance) {
        HashMap<Integer, HashMap<Integer, Double>> fakeDistance = new HashMap(); // 임시로 투영된 좌표에 따른 데이터간 거리

        HashMap<Integer, double[]> location = new HashMap();    // 좌표값
        HashMap<Integer, double[]> grad = new HashMap();        // 이동값

        double totError   = 0.0;                                // 실제 거리와, 임시 좌표 거리 간의 차이
        double lastError  = -1.0;                               // 이전번 시행의 totError
        double errorScale = 0.0;                                // 에러의 비율

        double rate = 0.01;                                    // 학습 비율

        int source = 0;                                         // 거리를 잴 데이터 1
        int target = 0;                                         // 거리를 잴 데이터 2
        double distance = 0.0;                                  // 두 데이터 간의 거리


        // 무작위 위치에 시작점을 초기화 + 이동값 초기화
        Iterator<Integer> songItr = songNumList.iterator();

        while (songItr.hasNext()) {
            Integer key = songItr.next();

            location.put(key, new double[2]);
            location.get(key)[0] = Math.random();
            location.get(key)[1] = Math.random();
                        
            grad.put(key, new double[2]);
            grad.get(key)[0] = 0.0;
            grad.get(key)[1] = 0.0;
        }

        // 좌표값 조정 반복 연산 시작 (100000번 까지 수행한다.
        for (int i = 0; i < 10000; i++) {
            songItr = songNumList.iterator();

            while (songItr.hasNext()) {
                Integer key = songItr.next();
                                
                grad.get(key)[0] = 0.0;
                grad.get(key)[1] = 0.0;
            }
            
            // 현재 투영된 거리를 계산
            for (int j = 1; j < songNumList.size(); j++) {
                source = songNumList.get(j);
                fakeDistance.put(source, new HashMap());

                for (int k = j - 1; k >= 0; k--) {
                    target = songNumList.get(k);
                    
                    distance =
                            Math.sqrt(Math.pow(location.get(source)[0] - location.get(target)[0], 2)
                                    + Math.pow(location.get(source)[1] - location.get(target)[1], 2));

                    fakeDistance.get(source).put(target, distance);
                }// end of for(k) : 개별 노래의 거리 계산.
            }// end of for(j) : 투영된 거리 계산 종료

            // 현재 투영 거리와 실제 거리간의 차이의 비율을 에러로 잡고, 오류 정도에 비례해서 현재 위치를 옮겨본다.
            for (int j = 1; j < fakeDistance.size(); j++) {
                source = songNumList.get(j);
                totError = 0.0;

                for (int k = j - 1; k >= 0; k--) {
                    target = songNumList.get(k);
                    
                    errorScale = (fakeDistance.get(source).get(target) - realDistance.get(source).get(target)) / realDistance.get(source).get(target);
                   
                    grad.get(target)[0] += ((location.get(target)[0] - location.get(source)[0]) / fakeDistance.get(source).get(target)) * errorScale;
                    grad.get(target)[1] += ((location.get(target)[1] - location.get(source)[1]) / fakeDistance.get(source).get(target)) * errorScale;

                    totError += Math.abs(errorScale);
                }// end of for(k)
            }// end of for(j)

            // 옮겨본 위치에 비해 옮기기 전이 에러가 적다면, 좌표 조정을 종료한다.
            if (lastError > 0 && lastError < totError) {

                //System.out.println("좌표조정 반복 횟수" + i);

                break;
            }// end of if() : break target == for(i)

            lastError = totError;

            for (int j = 0; j < fakeDistance.size(); j++) {
                target = songNumList.get(j);
                location.get(target)[0] -= rate * grad.get(target)[0];
                location.get(target)[1] -= rate * grad.get(target)[1];
            }// end of for(j) : 옮길 좌표값을 좌표에 반영한다.
        }// end of for(i) : 좌표값 반복 연산 종료
        
        return location;
    }// end of getLocation()



    /**
     * 목적 : k cluster의 k 초기값을 결정하기 위한 메소드
     * 
     * 상관관계가 음의 경향을 보이는 곡들은 서로 다른 군집에 속하는 것이 논리적임. 따라서 음의 경향을 보이는 곡들을 k 초기값으로 주도록 한다.
     */
    public ArrayList<Integer> getKCentroids(ArrayList songNumList) throws SQLException {
        ArrayList<Integer> kCentroids = null;       // 결과값을 저장할 상관계수 테이블
        HashMap<String, Integer> tempData = null;   // DB의 한 행 값을 임시로 저장할 변수

        Iterator<HashMap> rowItr = songDataDAO.selectKCentroids(songNumList).iterator();
        kCentroids = new ArrayList();               // 윗 라인에서 익셉션이 발생하지 않았을때 실행되도록.

        while (rowItr.hasNext()) {
            tempData = rowItr.next();
            
            if(!kCentroids.contains(tempData.get("source"))){
                kCentroids.add(tempData.get("source"));
            }
            
            if(!kCentroids.contains(tempData.get("target"))){
                kCentroids.add(tempData.get("target"));
            }            
        }// end of while
        return kCentroids;
    }//end of getKCentroids()



    /**
     * 목적 : 각 곡이 어느 군집에 속해야할지 결정하는 메소드.
     * 개별 곡이 전달받은 중심점 중 가장 가까운 중심점의 멤버가 된다 -> 중심점을 멤버들의 평균 좌표로 조정한다 -> 멤버들이 다시 중심점을 결정한다
     * -> 중심점을 멤버 평균 좌표로 조정한다 -> 반복연산 -> 반복수행한 결과가 동일하면 종료한다.
     */
    public HashMap getKCluster(ArrayList<Integer> songNumList, ArrayList<Integer> kCentroids, HashMap<Integer, double[]> location) {
        HashMap<Integer, ArrayList<Integer>> bestMatches = null;    // 중심점과 중심점에 속한 멤버 데이터를 보관
        HashMap<Integer, ArrayList<Integer>> lastMatches = null;    // 이전 수행 결과를 저장

        HashMap<Integer, double[]> centroidLocs = new HashMap();    // 중심점들의 좌표값을 기억할 변수

        int centroidNum = kCentroids.size();    // 중심점의 개수
        int songNum = songNumList.size();       // 데이터 개수

        double[] tempCtrLoc = null;             // 개별 중심점의 좌표
        double[] tempSongLoc = null;            // 개별 데이터의 좌표

        double bestDistance = 0.0;              // 데이터와 중심점간 거리가 가장 짧은 경우
        double tempDistance = 0.0;              // 이번 연산 에서 도출된 데이터와 중심점 사이의 거리

        int bestMatch = 0;                      // 개별 데이터가 어느 중심점에 속하는지 저장.

        ArrayList tempMember;                   // 개별 중심점의 멤버 리스트

        
        if(kCentroids.size() == 0){
            kCentroids.add(songNumList.get(0));
            centroidNum = 1;
        }
        
        
        // 중심점의 위치값 초기화
        for (int i = 0; i < centroidNum; i++) {
            centroidLocs.put(kCentroids.get(i), location.get(kCentroids.get(i)).clone());
        }

        // 데이터들의 소속 군집 결정 + 중심점 조정 반복 연산 시작 : 최대 연산 횟수 100
        for (int i = 0; i < 100; i++) {
            bestMatches = new HashMap();

            // bestMatches에 key : 군집, value : 군집에 속한 데이터로 초기화
            for (int j = 0; j < centroidNum; j++) {
                bestMatches.put(kCentroids.get(j), new ArrayList());
            }

            // 각 데이터 별로 가장 근접한 중심점을 찾는다.
            for (int j = 0; j < songNum; j++) {
                // 개별 데이터 위치 접근
                tempSongLoc = location.get(songNumList.get(j)).clone();

                /*
                 * 개별 데이터와 모든 중심점 사이의 거리중 가장 짧은 값. 초기값을 -1.0으로 초기화한다. 
                 * 거리는 절대 음수일수 없기때문에 한번도 연산되지 않았다는 플래그이다.
                 */
                bestDistance = -1.0;

                for (int k = 0; k < centroidNum; k++) {
                    tempCtrLoc = centroidLocs.get(kCentroids.get(k));

                    tempDistance = Math.sqrt(Math.pow(tempSongLoc[0] - tempCtrLoc[0], 2) + Math.pow(tempSongLoc[1] - tempCtrLoc[1], 2));

                    if (bestDistance == -1.0 || tempDistance < bestDistance) {
                        bestDistance = tempDistance;
                        bestMatch = kCentroids.get(k);
                    }// end of if :
                }// end of for(k) :
                bestMatches.get(bestMatch).add(songNumList.get(j));
            }// end of for(j) : 근접 중심점 탐색 완료

            
            // 만약 이전 클러스터링 결과와 이번 클러스터링 결과가 같다면 반복연산을 종료한다.
            if (lastMatches != null && bestMatches.equals(lastMatches)) {
                break;
            }// end of if : break target == for(i)

            lastMatches = (HashMap<Integer, ArrayList<Integer>>) bestMatches.clone();

            // 중심점의 위치를 멤버들 위치의 평균값으로 이동시킨다.
            for (int j = 0; j < centroidNum; j++) {
                tempCtrLoc = centroidLocs.get(kCentroids.get(j));
                tempCtrLoc[0] = 0.0;
                tempCtrLoc[1] = 0.0;

                tempMember = bestMatches.get(kCentroids.get(j));

                for (int k = 0; k < tempMember.size(); k++) {
                    tempCtrLoc[0] += location.get(tempMember.get(k))[0];
                    tempCtrLoc[1] += location.get(tempMember.get(k))[1];
                } // end of for(k)
                tempCtrLoc[0] /= tempMember.size();
                tempCtrLoc[1] /= tempMember.size();
            }// end of for(j) : 중심점 이동 완료
        }// end of for(i) : 소속 군집 결정 + 중심점 조정 완료
        return bestMatches;
    }
    
    
    
    /**
     * 목적 : 멤버들의 연결선을 뽑아내기 위한 메소드
     *
     */
    public HashMap<Integer, HashMap<Integer, Double>> getLines(HashMap<Integer, ArrayList> kCluster) throws SQLException {
        Iterator clusterItr = kCluster.keySet().iterator();
        ArrayList members = null;
        
        HashMap<Integer, HashMap<Integer, Double>> lines = null;       // 결과값을 저장할 상관계수 테이블
        HashMap tempData = null;                                        // DB의 한 행 값을 임시로 저장할 변수

        while(clusterItr.hasNext()){
            members = kCluster.get(clusterItr.next());
            
            Iterator<HashMap> rowItr = songDataDAO.selectLines(members).iterator();
            
            if(lines == null || lines.isEmpty()){
                lines = new HashMap<Integer, HashMap<Integer, Double>>(); // 윗 라인에서 익셉션이 발생하지 않았을때 실행되도록.
            }
            
            while (rowItr.hasNext()) {
                tempData = rowItr.next();
    
                if (!lines.containsKey(tempData.get("source"))) {
                    lines.put((Integer) tempData.get("source"), new HashMap<Integer, Double>());
                }// end of if : 해당 row값이 없을 때만 새로 만든다.
                lines.get(tempData.get("source")).put((Integer) tempData.get("target"), (Double) tempData.get("correlation"));
            }// end of while
        }
        return lines;
    }
    
    
    
    /**
     * 군집 내의 멤버들 간 조화도를 회수하여, 조화도를 고려해 선택정렬한 뒤 반환한다.
     * 
     * @param members
     * @return
     */
    public HashMap getPlayList(HashMap<Integer, ArrayList> kCluster) throws SQLException {
        Iterator clusterItr = kCluster.keySet().iterator();
        ArrayList<Integer> members = null;
        
        Iterator<HashMap> datum = null;
        HashMap<String, Integer> data = null;
        
        HashMap<Integer, ArrayList> playList = new HashMap();
        

        while(clusterItr.hasNext()){
            members = kCluster.get(clusterItr.next());

            if (members.size() == 1) {
                playList.put(members.get(0), new ArrayList());
                playList.get(members.get(0)).add(members.get(0));
            }

            else {
                datum = songDataDAO.selectHarmony(members).iterator();

                while (datum.hasNext()) {
                    data = datum.next();

                    if (!playList.containsKey(data.get("source"))) {
                        playList.put(data.get("source"), new ArrayList());
                    }
                    playList.get(data.get("source")).add(data.get("target"));
                }
            }
        }
        return playList;
    }
}