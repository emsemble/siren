package com.emsemble.seiren.bo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.emsemble.seiren.dao.CollectionDAO;
import com.emsemble.seiren.dao.SongDAO;
import com.emsemble.seiren.vo.CollectionVO;
import com.emsemble.seiren.vo.SongVO;

/**
 * 1. 파일 이름 : CollectionBO
 * 
 * 2. 작성자 : 김성철
 * 
 * 3. 목적 : 컬렉션 생성, 컬렉션의 이름 중복 검사
 * 
 * 4. 이력 :
 * 
 * 2014. 04. 14. - 최초 작성
 * 
 * 2014. 04. 18. - 내가 가지고 있는 콜렉션 조회하기 메소드 작성
 * 
 * 2014. 04. 19. - 콜렉션 만들기 메소드 수정
 * 
 * 이름 중복시 자동으로 뒤에 번호 붙여주는 메소드 작성
 * 
 * 2014. 04. 21. - 콜렉션 만들고 이름값 return 추가, 곡 추가 삭제 메소드 작성
 * 
 * 2014. 04. 23. - 콜렉션에 저장하고 있는 곡 보여주는 메소드 작성, 한번에 없에는 리셋메소드 작성
 * 
 * 2014. 04. 24. - 콜렉션에 설명, 공개여부 저장하는 메소드 작성
 * 
 * 2014. 04. 26. - 콜렉션에 이미 저장되어 있는 곡은 저장 안되도록 하는 메소드 작성
 * 
 * 2014. 04. 27. - 해당 리스트에 곡이 얼마나 있는지 판단하는 메소드 작성
 * 
 * 2014. 05. 13. - 리스트에 대한 접근 권한 검증 메소드 작성 (고상우)
 * 
 * 2014. 05. 18 - 로그인한 유저가 소유한 collection 반환(김시온)
 */

@Component
public class CollectionBO {

    @Autowired
    private CollectionDAO collectionDAO;
    
    @Autowired
    private SongDAO songDAO;



    /**
     * 목적 : Collection 만든후, 유저가 가지고 있는 리스트 중 이름이 중복되었는지 확인하고, 자동으로 (num) 을 달아서 생성 값을 전달하는 메소드
     */
    public HashMap createCollection(String owner, String listName) throws Exception {

        int searchRes = 0;
        int ispublic = 0; // 공개, 비공개 설정 DB에 not Null 설정으로 인해 임시로 받아두는 설정
        HashMap listNumNName = new HashMap();
        
        String originalName = listName;
        String rgex = listName + "%"; // listName에 wildcard 붙여서 변수 선언

        searchRes = checkCollectionName(owner, rgex);
        
        if(searchRes !=0){

            rgex = listName + "(%";  
            
            int wildCardSearch = checkCollectionName(owner, rgex); // listName에 와일드카드 붙여서 검색, 중복되었을 경우
            searchRes = checkCollectionName(owner, originalName);  // 원래 이름으로 검색을 한번더
            
            if(searchRes != 0){ // 한번더 검색해서 중복되어있으면..
                
                wildCardSearch += 1;
                originalName += "(" + wildCardSearch + ")";
                listName = originalName;
                
            }else if(searchRes == 0){
                
                listName = originalName;

            } // 원래이름 한번더 검사 종료
            
        } // 원래이름 검사 종료

        if(collectionDAO.createCollection(new CollectionVO(owner, listName, ispublic))==0){
            throw new Exception("컬렉션 생성에 실패했습니다 : DB 실행 오류");
        };
        
        listNumNName.put("listNum", getMyCollectionNum(listName, owner));
        listNumNName.put("listName", listName);
        
        return listNumNName;
    } // 콜렉션 만들기




    public int checkCollectionName(String owner, String listName) throws Exception {

        CollectionVO collectionvo = new CollectionVO(owner, listName);

        return collectionDAO.checkCollectionName(collectionvo);

    } // 콜렉션 리스트의 이름을 체크하는 메소드



    public List<CollectionVO> searchMycollection(String owner) throws Exception {

        return collectionDAO.searchMycollection(owner);

    } // 내가 가지고 있는 콜렉션 조회



    /**
     * 목적 : 현재 로그인한 유저와 Collection 의 이름을 가지고 DB에 저장된 CollectionNum을 반환하는 메소드
     * 
     * @param listName : 입력한 listName
     * @param owner : 현재 유저
     * @return : 해당하는 CollectionNum
     */
    public int getMyCollectionNum(String listName, String owner) throws Exception {

        List<CollectionVO> myCollection = searchMycollection(owner);
        Iterator<CollectionVO> collectionItr = myCollection.iterator();

        int collectionNum = 0;

        while (collectionItr.hasNext()) {

            CollectionVO collection = (CollectionVO) collectionItr.next();

            if (collection.getName().equals(listName)) {

                collectionNum = collection.getNum();

            } // ListName과 collectionList가 일치하는지 검증

        } // Collection에 다음이 있을 때 까지

        return collectionNum;

    } // 유저와 ListName을 만족하는 CollectionNum을 DB에서 찾은후 반환



    public String addSong(int collectionNum, int songNum) throws Exception {

        HashMap song = new HashMap();

        song.put("collectionNum", collectionNum);
        song.put("songNum", songNum);

        int result = searchDuplicateSong(song);

        if (result == 0) {

            collectionDAO.addSong(song);
            return "success";

        }
        else {

            return "fail";
        }

    }// 곡을 추가할때 중복되는 노래 있는지 확인한 후 없으면 저장, 있으면 fail 메시지를 넘겨줌



    public String removeSong(int collectionNum, int songNum) throws Exception {

        HashMap delSong = new HashMap();

        delSong.put("collectionNum", collectionNum);
        delSong.put("songNum", songNum);

        int result = collectionDAO.removeSong(delSong);


        if (result == 0) {

            return "fail";

        }
        else {

            return "success";

        }

    }// 곡을 삭제할때 HashMap 객체에 저장해두기, DAO로 전송 삭제한 곡이 없으면 Fail 메시지 전송해준다



    public int searchDuplicateSong(HashMap song) throws Exception {

        return collectionDAO.searchDuplicateSong(song);
    }// 선택한 콜렉션에 중복되는 노래 있는지 확인후 결과값 리턴



    public List<SongVO> saveSongList(int collectionNum) throws Exception {

        return collectionDAO.saveSongList(collectionNum);

    }// 현재 저장되어있는 곡을 보여주기
    
    
    
    public String getPlayer(int collectionNum) throws Exception{
        
        return collectionDAO.selectPlayer(collectionNum);
    }



    public void resetMyCollection(int collectionNum) throws Exception {

        collectionDAO.resetMyCollection(collectionNum);
    }// 현재 저장되어있는 곡 리셋



    public void inputCollectionInfo (int collectionNum, String description, int ispublic, ArrayList<String> tags) throws Exception {
        
        CollectionVO collectionVO = new CollectionVO(collectionNum, description, ispublic, tags);
        
        collectionDAO.inputCollectionInfo(collectionVO);
    }// 콜렉션 번호가지고 설명과 공개여부를 업데이트



    public CollectionVO selectedCollectionInfo(int collectionNum) throws Exception {

        return collectionDAO.selectedCollectionInfo(collectionNum);
    }// 콜렉션의 모든 정보를 리턴해주는 메소드



    public int clickMyList(int collectionNum) throws Exception {

        return collectionDAO.clickMyList(collectionNum);
    }



    public CollectionVO getCollectionHavingSong(int selCollection) throws Exception {

        CollectionVO selectedColl = selectedCollectionInfo(selCollection);
        List<SongVO> selectedSongs = saveSongList(selCollection);

        HashMap<Integer, SongVO> hMapSongList = new HashMap<Integer, SongVO>();

        Iterator<SongVO> itrSongs = selectedSongs.iterator();


        while (itrSongs.hasNext()) {

            SongVO songsInfo = itrSongs.next();

            hMapSongList.put(songsInfo.getNum(), songsInfo);
        }

        selectedColl.setSongList(hMapSongList);

        return selectedColl;
    }// MyList 에서 선택한 리스트로 들어가기



    public int saveThumbnail(int collectionNum, String thumbnail, String userId) {
        HashMap hMap = new HashMap();

        hMap.put("collectionNum", collectionNum);
        hMap.put("thumbnail", thumbnail);

        return collectionDAO.updateThumbnail(hMap);
    }
    
    
    
    public int deleteThumbnail(int collectionNum) {

        return collectionDAO.deleteThumbnail(collectionNum);
    }
    
    
    
    public int savePlayer(int collectionNum, String player) {
        HashMap hMap = new HashMap();

        hMap.put("collectionNum", collectionNum);
        hMap.put("player", player);
        
        return collectionDAO.updatePlayer(hMap);
    }
    
    
    
    public int deletePlayer(int collectionNum) {
        
        return collectionDAO.deletePlayer(collectionNum);
    }



    public List<SongVO> selectSongInfoBySongNum(int songNum) {

        return collectionDAO.selectSongInfoBySongNum(songNum);

    }


    public int getAuthority(int collectionNum, String id) {

        HashMap idAndNum = new HashMap();

        idAndNum.put("id", id);
        idAndNum.put("num", collectionNum);
        return collectionDAO.selectAutority(idAndNum);
    }



    public int incrPlayNum(int listNum, int songNum) {
        HashMap listAndSong = new HashMap();
        listAndSong.put("listNum", listNum);
        listAndSong.put("songNum", songNum);

        return collectionDAO.incrPlayNum(listAndSong);
    }
    
    
    
    public int incrTotPlayNum(int songNum){
        
        return songDAO.incrTotPlayNum(songNum);
    }



    public List<CollectionVO> showMyCollectionLists(String owner) throws SQLException{
        
        return collectionDAO.selectMyCollectionLists(owner);
    
    }
    
   public List<CollectionVO> getFollowList(String id){
        
        return collectionDAO.selectFollowList(id);
    }
}
