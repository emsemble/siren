package com.emsemble.seiren.bo;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.emsemble.seiren.dao.UserDAO;
import com.emsemble.seiren.vo.UserVO;

@Component
public class UserBO {

	@Autowired
	private UserDAO userDAO;

	
	/**
	 * 
	 * 목적 :  DAO에서 받아온 사용자 정보로 login validation하는 메소드
	 *             차후에 Validation한 값으로 view단에서 사용자에 맞는 화면 출력하는 용도
	 */
    public int loginValidate(String userId, String userPw) throws SQLException {
        
         UserVO userVO = userDAO.selectUserById(userId);
        
         
        if (userVO != null) {
            
            if (userVO.getPw().equals(userPw)) {
                return LoginValidationFlag.SUCCESS.getFlagNum();
            }
            
            else {
                return LoginValidationFlag.WRONG_PASSWORD.getFlagNum();
            }
            
        } 
        
        else {
            return LoginValidationFlag.WRONG_ID.getFlagNum();
        }
    }
    
    
    
    /**
     * 
     * @param userId
     * 목적 : User 정보 다가져다 주는 로직 
     */
    public UserVO selectUserVO (String userId) throws SQLException{
        return userDAO.selectUserById(userId);
    }
    
    
    
    /**
     * 
     * 목적 : 현재 비밀번호를 확인하기 위해서 session에 저장된 id로 user객체 가져 온 뒤 현재비밀번호와 비교.
     *          성공하면 SUCCESS(0), 실패하면 WRONG_PASSWORD(1)
     */
    public int selectPresentPasswordByUserId(String presentPassword, String userId) throws SQLException {
        
        UserVO userVO = userDAO.selectUserById(userId);
        
        
        if (userVO != null) {
            
            if (userVO.getPw().equals(presentPassword)) {
                return LoginValidationFlag.SUCCESS.getFlagNum();
            }
            
            else {
                return LoginValidationFlag.WRONG_PASSWORD.getFlagNum();
            }
      
        } 
        
        else {
            return LoginValidationFlag.WRONG_PASSWORD.getFlagNum();
        }
    }


    
    /**
     * 
     * 목적 : 회원의 계정을 삭제하기위해 session에 저장된 id로 계정을 삭제하게 한 후 삭제 유무 검증
     *          성공하면 true, 실패하면 fail
     */
    public boolean deleteUserByUserId(String userId) throws SQLException{
        
        int flag = userDAO.deleteUserByUserId(userId);
        System.out.println(flag);
        
        if (flag > 0) {
           return true; 
        } 
        
        else {
            return false;
        }
    }
    

    
    /**
     * 
     * 목적 : 회원의 이름을 수정하기 위해 id와 새로운 이름을 DAO에 보낸 후 수정 유무 검증
     *          성공하면 true, 실패하면 fail
     */
    public boolean updateNewNameByUserId(String newName, String userId) throws SQLException {
        int flag = userDAO.updateNewNameByUserId(newName,userId);
        
        if (flag > 0) {
            return true; 
         } else {
             return false;
         }
    }
    

    
    /**
     * 
     * 목적 : 회원의 비밀번호를 수정하기 위해 id와 새로운 비밀번호를 DAO에 보낸 후 수정 유무 검증
     *          성공하면 true, 실패하면 fail
     */
    public boolean updateNewPasswordByUserId(String newPassword, String userId) throws SQLException {
        
        int flag = userDAO.updateNewPasswordByUserId(newPassword,userId);
        
        if (flag > 0) {
            return true; 
         } else {
             return false;
         }
    }
    
    
    
    /**
     * 
     * @author Si-On
     * 목적 : enum으로 login validation 수행
     * 
     * Success(ID,PW일치) -> 0
     * WRONG_PASSWORD(ID일치, PW불일치) ->1
     * WRONG_ID(ID불일치 하거나 ID미존재) ->2
     * 
     */
    private enum LoginValidationFlag {
        SUCCESS(0),
        WRONG_PASSWORD(1),
        WRONG_ID(2);
     
        
        private int flagNum;
        
        
        LoginValidationFlag(int flagNum) {
            this.flagNum = flagNum;
        }
        
        
        public int getFlagNum() {
            return this.flagNum;
        }
    }
    
    
    /**
     * 목적 : 회원 가입정보를 DB에 저장시킨 후, DB에 있는 Status 정보를 반환
     * @param status : 가입한 유저의 Status정보
     */
    public int signUpService (String id, String pw, String name) throws Exception {
        
        UserVO uservo = new UserVO(id, pw, name);
        int status = 0;
        
        userDAO.signUpUser(uservo);
        status = userDAO.findUserStatus(id);
        
        return status;
        
    }
    
    
    
    /**
     * 목적 : DB에 중복되는 ID가 있는지 없는지 값을 CheckId에 True / false로 반환
     * 중복되는 ID가 있다면 true, 중복되는 ID가 없다면 false 반환
     * @param isDuplicate : 중복되는 아이디 값을 String으로 받음 / 중복 안되면 null로 포현
     */    
    public Boolean searchDuplicateId (String id) throws Exception {
        
        String isDuplicate = userDAO.searchDuplicateId(id);
        
        if (isDuplicate != null){
            
            return true;
            
        } else {
            
            return false;
        }
    }    
    
}
