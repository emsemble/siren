package com.emsemble.seiren.bo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.emsemble.seiren.dao.SharedCollectionDAO;
import com.emsemble.seiren.dao.SongDAO;
import com.emsemble.seiren.vo.CollectionVO;
import com.emsemble.seiren.vo.SongVO;

/**
* 1. 파일 이름 : SearchBO
* 
* 2. 작성자 : 김시온
* 
* 3. 목적 : SongDAO에서 전달한 정조 SearchController 에 전달
* 
* 4. 이력 :
* 
* 2014. 04. 16 최초작성
* 2014. 05. 11 random으로 song 정보 가져오는 메소드 생성
* 2014. 05. 22 songBroswe에서 filter기능 수행하는 메소드 생성
*
*/

@Component
public class SearchBO {
    
    @Autowired
    private SongDAO songDAO;
    @Autowired
    private SharedCollectionBO sharedCollectionBO;
    @Autowired
    private SharedCollectionDAO sharedCollectionDAO;
    
    public List<SongVO> selectAllSongInfo (int start, int count, String sortType, int collectionNum) throws SQLException{
        
        return songDAO.selectAllSongInfo(start, count, sortType, collectionNum);
        
    }
    
    
    
    public int countAllSongInfo (int collectionNum) throws SQLException {
        
        return songDAO.countAllSongInfo(collectionNum);
        
    }
    
    
    
    public List<SongVO> selectSongBySearchKeyword(String searchKeyword) throws SQLException {
        
        return songDAO.selectSongBySearchKeyword(searchKeyword);
    
    }



    public List<SongVO> selectAllSongList() throws SQLException{
      
        return songDAO.selectAllSongList();

    }

    
    
	public ArrayList<List> selectSongByGenre(ArrayList<String> gArray) throws SQLException {

		return songDAO.selectSongByGenre(gArray); 

	}//random으로 songs 가져오는 메소드



	public List<SongVO> submitFilterCB(HashMap<String, ArrayList> valuesMap)  throws SQLException{
		
		 return songDAO.selectFilterCB(valuesMap);
		 
	}//songBroswe filter 기능 메소드


	

	public List<SongVO> searchByGenre(String genre)  throws SQLException {
		
		return songDAO.searchByGenre(genre);
		
	}



	public List<SongVO> searchByYear(String year)  throws SQLException  {
		
		return songDAO.searchByYear(year);
		
	}



	public List<SongVO> searchKeyword(HashMap<String, String> keyMap) throws SQLException {

		return songDAO.searchKeyword(keyMap);
		
	}


	
    /**
     * 목적 : 필터태그를 리스트에 담아서 DB에 전송, 필터링에 해당하는 리스트 반환한다
     * @param emotionalTag 감정태그
     * @param attrTag      곡의 성격 태그
     * @param yearTag      년도 태그
     * @param userId       사용자 아이디
     * @return 필터링된 SharedList들
     * @throws Exception
     */
    public List<CollectionVO> filteringList(List<String> emotionalTag, List<String> attrTag, 
                                            List<String> yearTag, String userId) throws Exception{
        
        HashMap<String, Object> filterData = new HashMap<String, Object>();
        
        if(emotionalTag.contains("null")){
            emotionalTag.remove(0);
        }
        
        if(attrTag.contains("null")){
            attrTag.remove(0);
        }
        
        if(yearTag.contains("null")){
            yearTag.remove(0);
        }
        
        filterData.put("emotionalTag", emotionalTag);
        filterData.put("attrTag", attrTag);
        filterData.put("yearTag", yearTag);
        filterData.put("userId", userId);
        
        List<CollectionVO> filteredList = sharedCollectionDAO.filteringList(filterData);
        
        filteredList = sharedCollectionBO.inputMostPlayedSong(filteredList);
        
        return filteredList;
    }


    /**
     * 목적 : searchBox에 있는 String 을 통해 리스트 검색하는 메소드
     * @param searchString : searchBox에서 전달받은 String
     * searchString 앞뒤로 %를 붙여서 검색을 실시한다.
     */
    public List<CollectionVO> searchListByString(String searchString, String userId) throws Exception{

        HashMap<String, String> searchData = new HashMap<String, String>();    
        
        searchData.put("searchString", "%"+searchString+"%");
        searchData.put("userId", userId);
        
        List<CollectionVO> searchedList = sharedCollectionDAO.searchListByString(searchData);
        
        searchedList = sharedCollectionBO.inputMostPlayedSong(searchedList);
            
        return searchedList;
    }


    /**
     * 목적 : 내가 팔로우 하고있는 메소드 찾아보기.
     */
    public List<CollectionVO> myFollowingList(String userId) throws Exception{

        List<CollectionVO> searchedList = sharedCollectionDAO.myFollowingList(userId);
        
        searchedList = sharedCollectionBO.inputMostPlayedSong(searchedList);
            
        return searchedList;
    }
}
