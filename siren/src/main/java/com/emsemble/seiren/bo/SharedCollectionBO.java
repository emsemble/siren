package com.emsemble.seiren.bo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.emsemble.seiren.dao.SharedCollectionDAO;
import com.emsemble.seiren.vo.CollectionVO;
import com.emsemble.seiren.vo.SharedInfoVO;

/**
 * 1. 파일 이름 : SharedCollectionBO.java
 * 
 * 2. 작성자 : 김성철
 * 
 * 3. 목적 : SharedList에 관한 Read, Update등의 기능 제공
 * 
 * 4. 이력 : 2014. 04. 28. 최초 작성
 * 
 *           2014. 05. 14. 중간 완성
 *           
 *           2014. 05. 24. 필터를 통해 리스트 셀렉트하는 메소드 작성
 *           
 *           2014. 05. 26. 가장 많이 플레이한 노래 찾는 메소드 분리.
 *           
 *           2014. 06. 02. 나의 팔로우 리스트 불러오는 메소드 추가.
 *           
 *           2014. 06. 05. 내 리스트를 팔로우 하지 못하도록 addFollowList 수정, searchIsMyList 메소드 추가
 */

@Component
public class SharedCollectionBO {
    
    @Autowired
    private SharedCollectionDAO sharedCollectionDAO;

    
    /**
     * 목적 : 현재 유저가 조회할 수 있는 공개 리스트 조회
     * @param random : 랜덤을 사용했을때는 무작위로 DB에서 추출, 랜덤이 없다면 1~5위의 팔로우 된 리스트를 조회
     */
    public List<CollectionVO> searchPublicCollection(String user, String random) throws Exception {
        
        HashMap<String, Object> searchData = new HashMap<String, Object>();
        
        if(random == null) {
            
            searchData.put("user", user);
            
        } else if (random.equals("random")) {
            
            searchData.put("user", user);
            searchData.put("random", random);
 
        }// DB에서 조회시 random으로 조회할지, 1~5위를 조회할지 분류해준다
        
        List<CollectionVO> publicCollection = sharedCollectionDAO.searchPublicCollection(searchData);

        publicCollection = inputMostPlayedSong(publicCollection); // 가장 많이 플레이 한 노래 찾아서 넣어주는 메소드
        
        return publicCollection;
    }

    
    
    /**
     * 목적 : 해당 유저의 팔로우 리스트에 추가한다. 
     * 만일 DB상에 팔로우 하고 있는 리스트인 경우 duplicated 라는 스트링을 반환.
     */
    public String addFollowList(String user, int collectionNum) throws Exception {
        
        HashMap<String, Object> follow = new HashMap<String, Object>();
        int isDuplicated = 0;
        int result       = 0;
        String owner = searchIsMyList(collectionNum);
        
        if(owner.equals(user)){
            
            return "isMyList";
        }
        
        follow.put("user", user);
        follow.put("collectionNum", collectionNum);
        
        
        isDuplicated = searchDuplicateFollow(follow);
        
        if (isDuplicated > 0) {
            
            return "duplicated";
        }// 팔로우가 중복되어있다면 duplicate 를 반환
        
        result = sharedCollectionDAO.addFollowList(follow);
        
        if (result == 0) {
            
            return "fail";
        
        } else {
          
            return "success";
        }// 팔로우를 DB에 넣을때 성공 / 실패 여부 판단
    }

    
    /**
     * 목적 : 내 리스트인지 확인하기위해 리스트 넘버로 검색해서 owner를 가져온다
     */
    public String searchIsMyList(int collectionNum) throws Exception{
        
        return sharedCollectionDAO.searchIsMyList(collectionNum);
    }



    /**
     * 목적 : 해당 유저의 팔로우 리스트에서 삭제한다. 
     * 만일 DB상에 팔로우 하고 있지 않다면 duplicated 라는 스트링을 반환.
     */
    public String deleteFollowList(String user, int collectionNum) throws Exception {
        
        HashMap<String, Object> follow = new HashMap<String, Object>();
        int isDuplicated               = 0;
        int result                     = 0;
        
        follow.put("user", user);
        follow.put("collectionNum", collectionNum);
        
        isDuplicated = searchDuplicateFollow(follow);
        
        if (isDuplicated == 0) {
            
            return "duplicated";
        }//이미 내 팔로우 리스트가 아니라면 duplicate 를 반환.

        result = sharedCollectionDAO.deleteFollowList(follow);
        
        if (result == 0) {
            
            return "fail";
        
        } else {
            
            return "success";
        }// 팔로우 삭제를 성공 / 실패 여부 판단.
    }
    
    
    
    /** 목적 : 팔로우 테이블에 중복되는게 있는지 판단해주는 메소드
     * 
     */
    public int searchDuplicateFollow (HashMap<String, Object> follow) throws Exception {
        
        return sharedCollectionDAO.searchDuplicateFollow(follow);
    }

    

    /**
     *  목적 : 선택한 공유 리스트의 추가정보를 가져오는 메소드 
     */
    public List<SharedInfoVO> getAddtionalInfo(int collectionNum) throws Exception{
        
        return sharedCollectionDAO.getAddtionalInfo(collectionNum);
    }
    
    
    
    /**
     * 목적 : 가장 많이 플레이한 노래 찾아서 넣어주는 메소드
     */
    public List<CollectionVO> inputMostPlayedSong(List<CollectionVO> collections) throws Exception{
        
        Iterator<CollectionVO> itr = collections.iterator();
        
        while(itr.hasNext()){
            CollectionVO collList = itr.next();
            String mostPlaySong   = searchMostPlaySong(collList.getNum());
            
            if(mostPlaySong != null) {
                
                collList.setAlbumid(mostPlaySong);
            }// searchMostPlaySong 메소드를 이용해 해당 리스트가 가장 많이 플레이한 곡을 가져오고, 그 곡의 albumID를 가져와 이미지 사용
        }
        return collections;
    }


    
    /** 
     * 목적 : 해당 리스트에서 가장 많이 재생된 노래를 판단한다. 
     */
    public String searchMostPlaySong(int collectionNum) throws Exception {
        
        return sharedCollectionDAO.searchMostPlaySong(collectionNum);
    }
    
    
    
    /** 
     * 목적 : 해당 리스트의 총 팔로워 수를 반환한다 
     */
    public int getTotalFollower(int collectionNum) throws Exception {
        
        return sharedCollectionDAO.getTotalFollower(collectionNum);
    }
    
    
    
    /**
     * 목적 : 해당 리스트의 장르 랭킹을 반환한다.
     */
    public List<SharedInfoVO> getGenreRank (int collectionNum) throws Exception {
        
        return sharedCollectionDAO.getGenreRank(collectionNum);
    }    
}
