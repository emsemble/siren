package com.emsemble.seiren.bo;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.emsemble.seiren.dao.CommentDAO;
import com.emsemble.seiren.vo.CommentVO;


/**
 * 1. 파일 이름 : CommentBO
 * 
 * 2. 작성자 : 김시온
 * 
 * 3. 목적 : 댓글 관련 BO
 * 
 * 4. 이력 :
 * 
 *  2014.05.02 최초 생성
 *  
 */


@Component
public class CommentBO {
	
	@Autowired
	private CommentDAO commentDAO;

	
	/**
	 * 
	 * 목적 : collectionNum으로 해당 음원리스트에 등록된 모든 댓글 select
	 * @param collectionNum
	 * @return List<CommentVO>
	 */
	public List<CommentVO> selectAllComment(int collectionNum) throws SQLException {
		
		return commentDAO.selectAllComment(collectionNum);
	}
	
	
	
	/**
	 * 
	 * 목적 : comment를 DB에 INSERT 시키기 위한 메소드
	 * @param commentVO
	 * @return  String (Success : insert성공, fail : insert 실패)
	 */
	public String  insertComment(CommentVO commentVO) throws SQLException {
		
		int result = 0; 
		result = commentDAO.insertComment(commentVO);
		
		return validateResult(result);
		
	}


	/**
	 * 
	 * 목적 : CommnetVO로 해당 댓글 SELECT  
	 * @param commentVO
	 * @return List<CommentVO>
	 */
	public CommentVO selectCommentByCommentVO(CommentVO commentVO) throws SQLException {
		
		return commentDAO.selectCommentByCommentVO(commentVO);
		
	}


	/**
	 * 목적 : commentNum으로 해당 댓글 DELETE 
	 * @param commentNum
	 * @return
	 */
	public String deleteCommentByNum(int commentNum) throws SQLException{
		
		
		int result = 0; 
		result = commentDAO.deleteCommentByNum(commentNum);
		
		return validateResult(result);
		
	}


	/**
	 * 목적 : commentNum으로 해당 댓글 내용을 UPDATE 
	 * @param commentNum, contents
	 * @return
	 */
	public String updateComment(int commentNum, String contents) throws SQLException {
		
		HashMap cmtMap = new HashMap();
		cmtMap.put("commentNum", commentNum);
		cmtMap.put("contents", contents);
		
		int result = 0; 
		result = commentDAO.updateComment(cmtMap);
		
		return validateResult(result);
		
	}
	
	
	
	/**
	 * 목적 
	 * @param result
	 * @return String(success : 성공, fail : 실패) 
	 */
	public String validateResult (int result) {
		
		if ( result >= 1 ) {
			
			return "success";
		}
		
		else {
			
			return "fail";
		}
	}

}
