package com.emsemble.seiren.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.emsemble.seiren.bo.CollectionBO;
import com.emsemble.seiren.bo.SearchBO;
import com.emsemble.seiren.bo.SharedCollectionBO;
import com.emsemble.seiren.vo.CollectionVO;
import com.emsemble.seiren.vo.SongVO;
import com.google.gson.Gson;

/**
* 1. 파일 이름 : SearchController
* 
* 2. 작성자 : 김시온
* 
* 3. 목적 : SearchBO에서 전달한 정보를 전달해주는 CLASS
* 
* 4. 이력 :
* 
* 2014. 04. 16 최초작성
*   : !!!!!!!!!!!!!!!!!!!!!!!!!!
*     [페이징 처리]
*       1. 한 페이지에 들어갈 리스트 갯수 20개로 설정
*       2. 한 화면에 보여질 페이지의 INDEX는 10개로 설정
*       3. 발매일순, 곡 번호 순으로 소팅(DEFAULT 값은 곡 번호 순)
*       
* 2014. 04. 21. 검색메소드에 추가기능 구현 (DB에 저장된 곡들 빼고 검색해서 보여주기)
* 
* 2014. 05. 11. songBrowse.jsp 처음 들어왔을 때 처음으로 뿌려주는 요청처리
* 
* 2014. 05. 18 mySongLists.do 에이작스 request가 들어왔을 때 현재 로그인 유저가 보유하고 있는 리스트 이름을 가져가는 메소드기능 구현
* 
* 2014. 05. 19 현재 로그인한 유저의 collection num, collection name 반환
* 
* 2014. 05. 24 songBroswe.jsp에서 filter검색했을 때 요청처리
* 
* 2014. 05. 27 createCollection.jsp 에서 ajax 요청처리(사용자가 젹용한 filter 파라미터로 받아서 처리)
* 
*/

@Controller
@SessionAttributes({"userId","userName", "userStatus"})
public class SearchController {
    
    private static final Logger logger = LoggerFactory.getLogger(SearchController.class);
    private static final int COUNT_PER_PAGE = 20;
    private static final int PAGE_PER_PAGE = 10; 
    private String savedListName = null;
    
    @Autowired
    private SearchBO searchBO;
    @Autowired
    private CollectionBO collectionBO;
    @Autowired
    private SharedCollectionBO sharedCollectionBO;
    
    
    @RequestMapping(value="/createCollection.do", method=RequestMethod.GET)
    public String createCollection (@RequestParam(value="page", defaultValue="1", required=false) int page, 
            @RequestParam(value="sortType", defaultValue="num", required=false) String sortType,
            @RequestParam(value="listNum") int listNum,
            @RequestParam(value="listName") String listName,
            @ModelAttribute("userId") String owner,  Model model ) {
     
        int start = (page - 1) * COUNT_PER_PAGE;
        Gson gson = new Gson();
        ArrayList songArray = new ArrayList();
      
        System.out.println();
        try {
            int totalCount = searchBO.countAllSongInfo(listNum);
            
            if (totalCount > 0) {
               
            	List<SongVO> songInfo  =  searchBO.selectAllSongInfo(start, COUNT_PER_PAGE, sortType, listNum);
            	ArrayList<List> songList = searchBO.selectSongByGenre(rendomByGenre());
        		
                for (int i=0; i < songInfo.size(); i++) {
                    songArray.add(songInfo.get(i));
                }
                
                model.addAttribute("songInfo", gson.toJson(songInfo));
                model.addAttribute("songList", gson.toJson(songList));
                
            }
            
            // =================================
            // PAGE 구현 
            // =================================
            
            // 1~ 10은 1PageSetGroup, 11~20은 2PageSetGroup
            int pageSetGroup = (int)(Math.ceil((double)page / PAGE_PER_PAGE));
           
            // page의 총 갯수(현재20페이지)
            int pageCount = (int)(Math.ceil((double)totalCount  / COUNT_PER_PAGE));
            
            // 현재 pageSetGroup의 첫번째 페이지. 2PageSetGroup의 startPage는 11이다.
            int startPage = ((pageSetGroup - 1) * PAGE_PER_PAGE) + 1;
            
            // 현재 pageSetGroup의 마지막 페이지/ 2PageSetGroup의 endPage는 20이다.
            int endPage = pageSetGroup * PAGE_PER_PAGE;
            
            if (pageSetGroup * PAGE_PER_PAGE > pageCount) {
            
                endPage = pageCount;
            
            }
            
            model.addAttribute("listName", listName);
            model.addAttribute("listNum", listNum);
            model.addAttribute("sortType", sortType);
            model.addAttribute("startPage", startPage);
            model.addAttribute("endPage", endPage);
            model.addAttribute("currentPage", page);
            model.addAttribute("pageCount", pageCount);
            model.addAttribute("pagePerPage", PAGE_PER_PAGE);
            
        }
        
        catch (Exception e) {
            
            e.printStackTrace();
            
            return "errorPage";
        
        }
        
        return "createCollection";
    
    }        
    
    
    @RequestMapping(value="/selectAllSongList.do", method=RequestMethod.GET )
     public String selectAllSongList (Model model) {
  
        JSONObject json = new JSONObject();
        Gson gson = new Gson();
        ArrayList songArray = new ArrayList();
        
        try {
            List<SongVO> allSongList = searchBO.selectAllSongList();
           
            for (int i=0; i < allSongList.size(); i++) {
            	songArray.add(allSongList.get(i));
            }
            
            json.put("isSuccess", true);
            json.put("selectAllSongList", gson.toJson(allSongList));

        } 
        
        catch (SQLException e) {
            e.printStackTrace();
            json.put("isSuccess", false);
            json.put("errorMsg", "검색결과가 존재하지 않습니다.");
        }
        
        model.addAttribute("json", json);
        return "json";
        
    }
    
    
    /*
     *  목적 : random으로  6개의 장르를 뽑아주는 메소드
     */
    public ArrayList<String> rendomByGenre () {
    	
    	Gson gson = new Gson();
    	String[] genre = {"Ballad", "Country", "Dance", "Folk", "Jazz", "Pop", "R&B / Soul", "Rap / Hip-hop", "Rock", "Vocal/Choral", "Animation", "Foreign Movie"};
    	ArrayList<Integer> array = null;
    	ArrayList<String> gArray = null;
    	array = new ArrayList();
    	gArray = new ArrayList();
    	boolean flag = false;
    	int x = 0;
		
		random : while (true) {
    		 x = (int)(Math.random()*genre.length);
    		
    		 if(array.size() == 0) {
    			 array.add(x);
    		 }
    		 
    		flag = array.contains(x);
    		
    		if(!flag){
    			array.add(x);
    		}
    		
    		if ( array.size() == 6) {
    			break random;
    		}
       	}
    	
    	for(int i = 0; i < array.size(); i++ ) {
    		gArray.add(genre[array.get(i)]);
    	}
    	
    	return gArray;
    }
    
    
    
    //songBrowse.jsp로 가는 메소드
    //DB에 있는 곡 장르 중에 랜덤으로 6개 뽑아서 최대 15개의 곡을 가젹감.
    @RequestMapping(value = "/songBrowse.do", method=RequestMethod.GET)
    public String songBrowse (Model model) {
    	
    	Gson gson = new Gson();

    	try {
			ArrayList<List> songList = searchBO.selectSongByGenre(rendomByGenre());
		
			model.addAttribute("songList", gson.toJson(songList));
		} 
    	
    	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return "songBrowse";
    	
    }
    
    
    
    //로그인한 해당 유저의 collection num, collection name 반환
    @RequestMapping(value = "/myCollLists.do",method=RequestMethod.GET) 
    public String showMyCollectionLists(Model model, @ModelAttribute("userId") String owner) {
    	
    	List<CollectionVO> collVO = null;
    	JSONObject json = new JSONObject();
    	Gson gson = new Gson();
    	
    	
		try {
			collVO = collectionBO.showMyCollectionLists(owner);
			if ( collVO != null ) {
				json.put("isSuccess", true);
				json.put("myLists", gson.toJson(collVO));
			}
			
		} catch (SQLException e) {
			//e.printStackTrace();
			json.put("isSuccess" , false );
		}
    	
		model.addAttribute("json", json);
    	return "json";

    }
    
    
    
    /*
     * 목적: songBroswe.jsp에서 filter검색했을 때 요청처리
     */
    @RequestMapping(value="/submitFilterCB.do", method=RequestMethod.GET)
    public String submitFilterCB (Model model, @RequestParam("genres") String genres,
    		@RequestParam("years") String years, @RequestParam("NTL") String NTL, @RequestParam("sex") String sex){
    	
    	String[] values;
    	ArrayList genresValues = null;
    	ArrayList yearsValues = null;
    	ArrayList nationValues = null;
    	ArrayList sexValues = null;
    	ArrayList<SongVO> resultList = new ArrayList();
    	HashMap<String,ArrayList> valuesMap =  new HashMap<String,ArrayList>();
    	
    	JSONObject json = new JSONObject();
    	Gson gson = new Gson();
    	
    	if (genres.equals("noResult")) {
    		System.out.println("byebye");
    	}else {
       		genresValues = new ArrayList();
    		values = genres.split("_");
    		
    		for ( int i = 0; i < values.length; i ++ ) {
    			genresValues.add(values[i]);
    			System.out.println(values[i]);
    		}
    		
    		System.out.println("장르들어옴");
    		
    		valuesMap.put("genresValues", genresValues);
    		values = null;
 
    	}
    	
    	if (years.equals("noResult")) {
    		System.out.println("year 결과없음");
    	} else {
    		yearsValues = new ArrayList();
    		values = years.split("_");
    		
    		for ( int i = 0; i < values.length; i ++ ) {
    			yearsValues.add(values[i]);
    			System.out.println(values[i]);
    		}
    		valuesMap.put("yearsValues", yearsValues);
    		values = null;
    	}
    	
       	if (NTL.equals("noResult") ) {
       		System.out.println("NTL 결과없음");
    	} else {
    		nationValues = new ArrayList();
    		values = NTL.split("_");
    		
    		for ( int i = 0; i < values.length; i ++ ) {
    			nationValues.add(values[i]);
    			System.out.println(values[i]);
    		}
    		valuesMap.put("nationValues", nationValues);
    		values = null;
    	}
       	
       	if (sex.equals("noResult") ) {
       		System.out.println("sex 결과없음");
    	} else {
    		sexValues = new ArrayList();
    		values = sex.split("_");
    		
    		for ( int i = 0; i < values.length; i ++ ) {
    			sexValues.add(values[i]);
    		}
    		valuesMap.put("sexValues", sexValues);
    		values = null;
    	}
	       
		try {
			
			System.out.println(valuesMap);
			if ( valuesMap.equals("")) {
				json.put("isSuccess", false);
			} 
			
			else {
				resultList = (ArrayList<SongVO>) searchBO.submitFilterCB(valuesMap);
				System.out.println(resultList);
				
				if (resultList != null) {
					 json.put("isSuccess", true);
			    	 json.put("resultList", gson.toJson(resultList));
			    } 
			       
			    else {
			    	json.put("isSuccess", false);
			    }
			}	
		} //try 끝
		
		catch (SQLException e) {
			json.put("isSuccess", false);
			e.printStackTrace();
		}
	       
	 	model.addAttribute("json", json);
		
	 	return "json";

    } 
    
    
    
    //해당 장르에 해당하는 song 찾아주는 메소드
    @RequestMapping(value="/searchByGenre.do", method=RequestMethod.GET)
    public String searchByGenre(Model model, @RequestParam("genre") String genre) {
    	
    	Gson gson = new Gson();
    	JSONObject json = new JSONObject();
    	String[] arrayYear = {"1970's","1980's", "1990's", "2000's"};
    	String year = null;
    	System.out.println(genre);
    	
		for ( int i = 0; i < arrayYear.length; i++) {
    		
			if (genre.equals(arrayYear[i])) {
    			
    			if (genre.equals("2000's")) {
    				year = genre.substring(0, 2) + "%";
    			} 
    			else {
    				year = genre.substring(0, 3) + "%";
    			}
    		} 
    	}//년대인지 음악 장르인지를 판명해준다.
    	
    	try {
    		
    		if (year == null ) {
    			json.put("isSuccess", true);
        		json.put("songList", gson.toJson(searchBO.searchByGenre(genre)));
    		} 
    		
    		else {
    			System.out.println(year);
    			json.put("isSuccess", true); 
    			json.put("songList", gson.toJson(searchBO.searchByYear(year)));
    		}
    		
		} catch (SQLException e) {
			e.printStackTrace();
			json.put("isSuccess", false);
		}
    	
    	model.addAttribute("json", json);
    	
    	return "json";
    }
    

    
    @RequestMapping(value = "/searchKeyword.do",  method=RequestMethod.POST)
    public String searchKeyword (Model model, @RequestParam("keyword1") String keyword1,
    											@RequestParam("keyword2") String keyword2) {
    	
    	HashMap<String, String> keyMap = new HashMap<String, String>();
    	List<CollectionVO> searchedList = null;
        String searchedListJson = null;
    	Gson gson = new Gson();
		JSONObject json = new JSONObject();
    	
    	keyMap.put("artistName", "%"+keyword1+"%");
		keyMap.put("songName", "%"+keyword2+"%");
		
		try {
			json.put("isSuccess", true);
			json.put("resultList", gson.toJson(searchBO.searchKeyword (keyMap)));
		} catch (SQLException e) {
			e.printStackTrace();
			json.put("isSuccess", false);
		}
		
		model.addAttribute("json", json);
		
		return "json";
    }

    
    
    /**
     * 목적 : 태그로 필터링 하기 위한 메소드
     * @param emotionalTag 감정태그
     * @param attrTag 노래의 특성 태그
     * @param yearTag 연도별 태그
     */
    @RequestMapping(value = "/filteringList.do", method=RequestMethod.POST)
    public String filteringList (@RequestParam("emotional") List<String> emotionalTag,
                                 @RequestParam("songAttr") List<String> attrTag,
                                 @RequestParam("year") List<String> yearTag, 
                                 @ModelAttribute("userId") String userId, Model model) {
        
        List<CollectionVO> filteredList = null;
        String filteredListJson = null;
        Gson gson = new Gson();
        
        try {
         
            filteredList = searchBO.filteringList(emotionalTag, attrTag, yearTag, userId);
            filteredListJson = gson.toJson(filteredList);

        } catch (Exception e) {
        
            e.printStackTrace();
            return "errorPage";
        }
        
        model.addAttribute("json", filteredListJson);
        return "json";
    }
    
    

    /**
     * 목적 : searchBox에 있는 스트링 받아서 리스트 검색, 해당 리스트 반환
     * @param search searchBox에서 받은 스트링
     */
    @RequestMapping(value = "/searchListByString.do", method=RequestMethod.POST)
    public String searchListByString (@RequestParam("search") String search,
                                      @ModelAttribute("userId") String userId, Model model){
        
        List<CollectionVO> searchedList = null;
        String searchedListJson = null;
        Gson gson = new Gson();
        
        try {
            
            searchedList = searchBO.searchListByString(search, userId);
            searchedListJson = gson.toJson(searchedList);
        
        }catch (Exception e){
            
            e.printStackTrace();
            return "errorPage";
            
        }
        
        model.addAttribute("json", searchedListJson);
        return "json";
    }
    
    
    
    /**
     * 목적 : 내가 팔로우 하고있는 리스트들 불러오기
     */
    @RequestMapping(value = "/myFollowingList.do", method=RequestMethod.POST)
    public String myFollowingList (@ModelAttribute("userId") String userId, Model model){
        
        List<CollectionVO> searchedList = null;
        String searchedListJson = null;
        Gson gson = new Gson();
        
        try {
            
            searchedList = searchBO.myFollowingList(userId);
            searchedListJson = gson.toJson(searchedList);
        
        }catch (Exception e){
            
            e.printStackTrace();
            return "errorPage";
            
        }
        
        model.addAttribute("json", searchedListJson);
        return "json";
        
    }
}