package com.emsemble.seiren.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.emsemble.seiren.bo.CommentBO;
import com.emsemble.seiren.vo.CommentVO;
import com.google.gson.Gson;
import com.google.gson.JsonObject;


/**
 * 1. 파일 이름 : CommentController
 * 
 * 2. 작성자 : 김시온
 * 
 * 3. 목적 : 댓글 관련 request 처리
 * 
 * 4. 이력 :
 * 
 *  2014.05.02 최초 생성
 *  	selectAllComment -> 댓글 창 클릭시 해당 음원 리스트에 등록된 모든 댓글 select
 * 		saveComment -> insert  댓글
 * 
 *  2014. 05. 22. 댓글 수정, 삭제 메소드 수정 (김성철)
 */

@Controller
@SessionAttributes({"userId","userName", "userStatus"})
public class CommentController {

	private static final Logger logger = LoggerFactory.getLogger(CommentController.class);
	
	
	@Autowired
	private CommentBO commentBO;
	
	
	
	/**
	 * 목적 :  댓글 창 열렸을 때 해당 음원리스트에 등록된 모든 댓글들 반환
	 * 			writer == userId : 작성자와, 로그인한 유저가 같을 경우 수정, 삭제 권한 부여
	 * 			writer != userId : readonly
	 * @param collectionNum
	 * @param model
	 * @param userId(세션에 저장되어 있는 Id)
	 */
	@RequestMapping(value = "/selectAllComment.do", method = RequestMethod.POST)
	public String selectAllComment ( @RequestParam( "collectionNum" ) int collectionNum, Model model,
												@ModelAttribute("userId") String userId ) {
		
		Gson gson = new Gson();
		JSONObject json = new JSONObject();
		
		try {
			json.put("isSuccess", true);
			List<CommentVO> commentList = commentBO.selectAllComment(collectionNum);
			
			for (CommentVO comment : commentList) {
				comment.setMyComment(userId.equals(comment.getWriter()));
			}
			
			json.put("commentVO", gson.toJson(commentList));
		} 
		
		catch (SQLException e) {
			json.put("isSuccess", false);
		}
		
		model.addAttribute("json", json);
		
		return "json";
	
	}
	
	
	
	/**
	 * 목적 : 해당음원 리스트에 등록된 댓글 DB에 저장하는 요청 처리
	 * @param contents
	 * @param collectionNum
	 * @param writer
	 * @param model
	 */
	@RequestMapping(value="/saveComment.do", method = RequestMethod.POST)
	public String saveComment (@RequestParam( "contents" ) String contents, @RequestParam("collectionNum") int collectionNum,
												@ModelAttribute("userId") String writer, Model model) {
		
		String result = null;
		String writeTime = null;
		boolean isMyComment = true;
		JSONObject json = new JSONObject();
		Date today = new Date();
		SimpleDateFormat date = new SimpleDateFormat("yyyy/MM/dd" );
		SimpleDateFormat time = new SimpleDateFormat("hh:mm:ss");
		Gson gson = new Gson();
		
		try {
			
			writeTime = date.format(today) + " " + time.format(today);
			CommentVO commentVO = new CommentVO(writer, collectionNum, contents, writeTime);
			result = commentBO.insertComment(commentVO);
			
			
			if (result == "success") {
				
			    CommentVO comment = commentBO.selectCommentByCommentVO(commentVO);
				comment.setMyComment(isMyComment);

				json.put("isSuccess", true );
				json.put("commentVO", gson.toJson(comment));
				
			} 
			
			else if (result == "fail") {
				json.put("isSuccess", false );
			} 
		
		} 
		
		catch (SQLException e) {
			json.put("isSuccess", false );
		}
		
		model.addAttribute("json", json);
		
		return "json";
	
	}

	
	
	/**
	 * 목적 : 댓글 삭제 버튼 눌렀을 때 해당 댓글을 DB에서 삭제하는 요청처리(commentNum으로 삭제) 
	 * @param commentNum
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/deleteComment.do", method = RequestMethod.POST)
	public String deleteComment (@RequestParam("commentNum") int commentNum, Model model) {
		
		JSONObject json = new JSONObject();
		String result = null;
		
		try {
			 result = commentBO.deleteCommentByNum(commentNum);
			
			 if (result == "success") {
			
			     json.put("isSuccess", true);
			 
			 } else {
			 
			     json.put("isSuccess", false);
			 }
		} 
		
		catch (SQLException e) {
			json.put("isSuccess", false);
		}
		
		model.addAttribute("json", json);
	
		return "json";
		
	}
	
	
	
	/**
	 * 목적 : 해당 댓글의 내용 수정 버튼 눌렀을 때 DB에서 내용 수정을 하는 요청을 처리
	 * @param commentNum
	 * @param contents
	 * @param model
	 */
	@RequestMapping(value="/updateComment.do", method=RequestMethod.POST)
	public String updateComment (@RequestParam ("commentNum") int commentNum, 
			@RequestParam ("contents") String contents, Model model) {
	
		String result = null;
		JSONObject json = new JSONObject();
		
		try {
			result = commentBO.updateComment(commentNum, contents);
			
			if (result == "success") {
				json.put("isSuccess", true);
			} 
			
			else {
				json.put("isSuccess", false);
			}
		} 
		
		catch (SQLException e) {
			json.put("isSuccess", false);
		}
		
		model.addAttribute("json", json);
		
		return "json";
	}
	
}
