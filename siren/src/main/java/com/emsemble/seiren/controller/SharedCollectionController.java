package com.emsemble.seiren.controller;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.emsemble.seiren.bo.CommentBO;
import com.emsemble.seiren.bo.SharedCollectionBO;
import com.emsemble.seiren.vo.CollectionVO;
import com.emsemble.seiren.vo.CommentVO;
import com.emsemble.seiren.vo.SharedInfoVO;
import com.google.gson.Gson;

/**
 * 1. 파일 이름 : SharedCollectionController.java
 * 
 * 2. 작성자 : 김성철
 * 
 * 3. 목적 : SharedList에 관한 컨트롤러
 * 
 * 4. 이력 : 2014. 04. 28. 최초 작성
 * 
 *           2014. 05. 14. 중간 완성
 *           
 *           2014. 06. 02. 내 팔로우 리스트 불러오는 메소드 추가
 */

@Controller
@SessionAttributes({"userId","userName", "userStatus"})
public class SharedCollectionController {
    
    @Autowired
    private SharedCollectionBO sharedCollectionBO;
    @Autowired
    private CommentBO commentBO;
    
    
    /**
     * 목적 : SharedList 브라우징 시작시 나오는 페이지.
     * topFiveJsonString     : 공개된 리스트 중, 팔로잉 수를 기준으로 최상위 5개 리스트를 뽑아낸다. 
     * collectionsJsonString : 공개된 리스트를 DB에서 무작위로 추출하여 가져온다.
     * 
     * jsonString을 모델에 저장하여 브라우징을 초기화시 사용할 수 있도록 전달한다.
     */
    @RequestMapping(value = "/sharedList.do", method = RequestMethod.GET)
    public String sharedList (@ModelAttribute ("userId") String user, Model model) {
        
        List<CollectionVO> publicCollections = null;
        
        String topFiveJsonString      = null;
        String collectionsJsonString  = null;
        
        Gson gson = new Gson();
        
        try {
            
            publicCollections = sharedCollectionBO.searchPublicCollection(user, null);
            topFiveJsonString = gson.toJson(publicCollections);
            
            publicCollections     = sharedCollectionBO.searchPublicCollection(user, "random");
            collectionsJsonString = gson.toJson(publicCollections);

        } catch (Exception e) {

            e.printStackTrace();
            return "errorPage";
        }

        model.addAttribute("topFiveList", topFiveJsonString);
        model.addAttribute("sharedCollection", collectionsJsonString);
        return "sharedList";
    }
    
    
    /**
     * 목적 : 해당하는 유저의 팔로우 리스트에 추가하기
     * result      : DB업데이트가 성공했는지 판단
     * totFollower : 현재 팔로워수를 전달해준다. 
     */
    @RequestMapping(value = "/addFollowList.do", method = RequestMethod.POST)
    public String addFollowList (@ModelAttribute("userId") String user, 
                                 @RequestParam("collectionNum") int collectionNum, Model model) {
        
        JSONObject json = new JSONObject();
        String result   = null;
        int totFollower = 0;
        
        try {
        
            result = sharedCollectionBO.addFollowList(user, collectionNum);
            totFollower = sharedCollectionBO.getTotalFollower(collectionNum);
            
            json.put("result", result);
            json.put("totFollower", totFollower);
        
        } catch (Exception e) {
        
            e.printStackTrace();
            
            return "errorPage";
        }
        
        model.addAttribute("json", json);
        
        return "json";
    }
    
    
    
    /**
     * 목적 : 해당하는 유저의 팔로우 리스트에서 삭제하기
     * result      : DB업데이트가 성공했는지 판단
     * totFollower : 현재 팔로워수를 전달해준다. 
     */
    @RequestMapping(value = "/deleteFollowList.do", method = RequestMethod.POST)
    public String deleteFollowList (@ModelAttribute ("userId") String user,
                                    @RequestParam ("collectionNum") int collectionNum, Model model) {
        
        JSONObject json = new JSONObject();
        String result   = null;
        int totFollower = 0;
        
        try {
            
            result = sharedCollectionBO.deleteFollowList(user, collectionNum);
            totFollower = sharedCollectionBO.getTotalFollower(collectionNum);
            
            json.put("result", result);
            json.put("totFollower", totFollower);

        } catch (Exception e) {
      
            e.printStackTrace();
            return "errorPage";
        }
        
        model.addAttribute("json", json);
        
        return "json";
    }
    
    
    /**
     * 목적 : 리스트 브라우징 페이지에서 선택한 리스트의 상세보기 페이지로 이동
     *        이동하면서 해당 리스트의 곡 정보, 장르의 랭킹, 커멘트 정보를 모델에 담는다. 
     */   
    @RequestMapping(value = "/detailListInfo.do", method = RequestMethod.GET)
    public String detailListInfo (@RequestParam("collectionNum") int collectionNum,
    										@RequestParam("amiFollowing") int amiFollowing, 
                                  @ModelAttribute("userId") String userId, Model model) {
        
        List<SharedInfoVO> result = null;
        List<CommentVO> comments = null;
        
        String jsonDetailResult = null;
        String jsonRankResult   = null;
        String jsonComments     = null;
        
        Gson gson = new Gson();
        
        try {
            
            result = sharedCollectionBO.getAddtionalInfo(collectionNum);
            jsonDetailResult = gson.toJson(result);
            
            result = sharedCollectionBO.getGenreRank(collectionNum);
            jsonRankResult = gson.toJson(result);
            
            comments = commentBO.selectAllComment(collectionNum);
            
            for (CommentVO comment : comments) {
                comment.setMyComment(userId.equals(comment.getWriter()));
            }
            jsonComments = gson.toJson(comments);
            
        } catch (Exception e) {
            
            e.printStackTrace();
            return "errorPage";
        }
        
        model.addAttribute("detailResult", jsonDetailResult);
        model.addAttribute("rankResult", jsonRankResult);
        model.addAttribute("comments", jsonComments);
        model.addAttribute("amiFollowing", amiFollowing);
        
        return "detailListInfo";
    }
}