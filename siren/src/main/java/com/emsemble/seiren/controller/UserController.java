package com.emsemble.seiren.controller;

import java.sql.SQLException;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.emsemble.seiren.bo.CollectionBO;
import com.emsemble.seiren.bo.UserBO;
import com.emsemble.seiren.vo.UserVO;

@Controller
@SessionAttributes({"userId","userName", "userStatus"})
public class UserController {
	
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    
	@Autowired
	private UserBO userBO;
	@Autowired
    private CollectionBO collectionBO;
	@Autowired
	private CollectionController collectionController;

	
	@RequestMapping(value="/index.do", method=RequestMethod.GET)
	public String home (Model model) {
	    
		
		if(model.containsAttribute("userId")){			
			return collectionController.myList((String)model.asMap().get("userId"), model);
		}
		
	    return "index";
	        
		
	}//첫화면으로 넘어가는 메소드(index.jsp)
	
	

	@RequestMapping(value="/login.do", method=RequestMethod.GET)
    public String login () {
       
        return "login";
        
    }//로그인 화면으로 넘어가는 메소드(login.jsp)
	
	
	
   @RequestMapping(value="/logout.do", method=RequestMethod.GET)
    public String logout (Model model) {
        
        return "logout";
                
    }//로그아웃 하는 메소드

   
   
   @RequestMapping(value="/modifyUserPage.do", method=RequestMethod.GET)
   public String modifyUser () {
       
       return "modifyUser";
   
   }//회원정보 수정/삭제 페이지로 이동하는 메소드(modifyUser.jsp)
   
   
   
   /**\
    * 
    * @param userId (login.jsp에서 입력받은 아이디값, 아이디 검증에 필요)
    * @param userPw(login.jsp에서 입력받은 비밀번호값, 비밀번호 검증에 필요)
    * @return json.jsp(ajax로 실행했기 때문에, 처리후 json을 가지고 넘어가야함)
    */
   @RequestMapping(value="/loginValidate.do", method=RequestMethod.POST)
   public String loginValidate (@RequestParam("userId") String userId, 
           @RequestParam("userPw") String userPw , Model model ) {
       
       JSONObject json = new JSONObject();
      
       
       try {
           
           int flag = userBO.loginValidate(userId, userPw);
           UserVO userVO = userBO.selectUserVO(userId);
          
           if (flag == 0) {
               json.put("isSuccess", true);
               model.addAttribute("userId", userVO.getId());//회원 아이디를 session에 담음(userId)
               model.addAttribute("userName", userVO.getName());//회원의 이름정보를 session에담음(userName)
               model.addAttribute("userStatus", userVO.getStatus());//회원의 status 정보를 session에 담음(userStatus)
           } 

           else if (flag == 1) {
               json.put("isSuccess", false);
               json.put("errorMsg", "비밀번호가 일치하지 않습니다.");
           } 
           
           else {
               json.put("isSuccess", false);
               json.put("errorMsg", "아이디가 존재하지 않습니다.");
           }
       
       } catch (SQLException e) {
           json.put("isSuccess", false);
           json.put("errorMsg", "로그인에 실패하였습니다.");
       }
       
       
       model.addAttribute("json",  json.toJSONString());
       return "json"; 
   }
   
   
   
   
   /**
    * 
    * @param presentPassword(현재비밀번호를 받아서 검증해줌)
    * @param userId(session에 저장된 id)
    * @return json.jsp(aJax실행, modifyUser.jsp로 돌아감)
    */
   @RequestMapping(value="/presentPasswordConfirm.do", method=RequestMethod.POST)
   public String presentPasswordConfirm (@RequestParam("presentPassword") String presentPassword, 
       @ModelAttribute("userId") String userId, Model model ) {
       
       JSONObject json = new JSONObject();
       int flag;
       try {
           flag = userBO.selectPresentPasswordByUserId(presentPassword, userId);
        
        if (flag == 0) {
            json.put("isSuccess", true);
            json.put("successMsg", "현재 비밀번호와 일치합니다.");
        } 
        
        else if (flag == 1) {
            json.put("isSuccess", false);
            json.put("errorMsg", "현재 비밀번호와 일치하지 않습니다.");
        }
        
    } catch (SQLException e) {
        json.put("isSuccess", false);
        json.put("errorMsg", "현재 비밀번호와 일치하지 않습니다.");
    }
    
    model.addAttribute("json",  json.toJSONString());
    return "json"; 
      
   }
   
   
   
   /**
    * 
    * @param userId(session에저장된 id)
    * @return  json.jsp(modifyUser.jsp로 돌아감)
    */
   @RequestMapping(value="/deleteUser.do", method=RequestMethod.POST)
   public String deleteUser (@ModelAttribute("userId") String userId, Model model) {
       
       JSONObject json = new JSONObject();
       
        try {
            
            boolean flag = userBO.deleteUserByUserId(userId);
            
            if (flag == true ) {
                json.put("isSuccess", true);
                json.put("successMsg", "삭제되었습니다.");
            } 
            
            else {
                json.put("isSucess", false);
                json.put("errorMsg", "계정삭제에 실패하였습니다");
            }
            
        } catch (SQLException e) {
            json.put("isSucess", false);
            json.put("errorMsg", "계정삭제에 실패하였습니다");
       }
           
       model.addAttribute("json", json.toString());
       return "json";
            
    }
  
   
   
   /**
    * 
    * @param newName(변경할 새 이름)
    * @param userId(session에저장된 id)
    * @return json.jsp(modifyUser.jsp로 돌아감)
    */
   @RequestMapping(value="/updateNewName.do",  method=RequestMethod.POST)
   public String updateNewName (@RequestParam("newName") String newName,
           @ModelAttribute("userId") String userId, Model model ) {
       
       JSONObject json = new JSONObject();
       
       try {
           boolean flag = userBO.updateNewNameByUserId(newName,userId);
           if (flag == true ) {
               json.put("isSuccess", true);
               json.put("newName", newName);
               model.addAttribute("userName", newName);
           } 
           
           else {
               json.put("isSuccess", false);
               json.put("errorMsg", "수정에 실패했습니다.");
           }
       } catch (SQLException e) {
           json.put("isSuccess", false);
           json.put("errorMsg", "수정에 실패했습니다.");
       }
       
       model.addAttribute("json", json.toString());
       return "json";

   }

   
   
   /**
    * 
    * @param newPassword(바꿀 새 비밀번호)
    * @param userId(session에 저장된 userId)
    * @return(modifyUser.jsp로 돌아감)
    */
   @RequestMapping(value="/updateNewPassword.do",  method=RequestMethod.POST)
   public String updateNewPassword (@RequestParam("newPassword") String newPassword,
           @ModelAttribute("userId") String userId, Model model ) {
       
       JSONObject json = new JSONObject();
       
       try {
           boolean flag = userBO.updateNewPasswordByUserId(newPassword,userId);
           if (flag == true ) {
               System.out.println("1");
               json.put("isSuccess", true);
               json.put("newPassword", newPassword);
           } 
           
           else {
               System.out.println("2");
               json.put("isSuccess", false);
               json.put("errorMsg", "수정에 실패했습니다.");
           }
       } catch (SQLException e) {
           System.out.println("3");
           json.put("isSuccess", false);
           json.put("errorMsg", "수정에 실패했습니다.");
       }
       
       model.addAttribute("json", json.toString());
       return "json";

   }
   
   
   
   @RequestMapping(value="/clickLogo.do",  method=RequestMethod.GET)
   public ModelAndView clickLogo (Model model) {
	 
	   RedirectView rv = null;
	   
	   if(model.containsAttribute("userId")) {
		   
		   rv = new RedirectView("myList.do");
	       rv.setExposeModelAttributes(false);
		   
		   return new ModelAndView(rv);
	   }
	   
	   else {
		   
		   rv = new RedirectView("index.do");
	       rv.setExposeModelAttributes(false);
		   
		   return new ModelAndView(rv);
	   }
   }
   
   
   

   @RequestMapping(value="/signUp.do", method=RequestMethod.GET)
   public String signUp () {
       return "signUp";
   }
   
   
   
   @RequestMapping(value="/errorPage.do", method=RequestMethod.GET)
   public String errorPage() {
       return "errorPage";
   }
   
   
   
   /**
    * SignUp페이지에서 Parameter를 받아 DB에 저장후, Model에 세션을 저장
    * @param int Status : 유저가 Status를 입력하지 않으므로 DB에서 스테이터스값 받아서 다시 출력
    */
   @RequestMapping(value="/signUpService.do", method=RequestMethod.POST)
   public ModelAndView signUpService (@RequestParam("signUpId") String id, 
                                @RequestParam("signUpPw") String pw, 
                                @RequestParam("signUpName") String name,
                                Model model) {
       try {
           
           int Status = userBO.signUpService(id, pw, name);
           
           model.addAttribute("userId", id);
           model.addAttribute("userName", name);
           model.addAttribute("userStatus", Status);

           
       } catch (Exception e) {
           
           RedirectView rv = new RedirectView("errorPage.do");
           rv.setExposeModelAttributes(false);

           return new ModelAndView(rv);         
       }

       RedirectView rv = new RedirectView("myList.do");
       rv.setExposeModelAttributes(false);

       return new ModelAndView(rv);
   }
   

   
   /**
    * @param Boolean isDuplicate : true / false로 중복된 아이디값 있는지 확인. true일시 중복, false일시 중복되지 않음
    */
   @RequestMapping(value="/searchDuplicateId.do", method=RequestMethod.POST)
   public String searchDuplicateId (@RequestParam("signUpId") String id, Model model) {
       
       JSONObject json = new JSONObject();
       
       try {
           
           Boolean isDuplicate = userBO.searchDuplicateId(id);
           
           if (isDuplicate) {
               json.put("isDuplicate", true);
               json.put("msg", "중복되는 아이디가 존재합니다");
           } else {
               json.put("isDuplicate", false);
               json.put("msg", "사용 가능한 ID입니다");
           }
       
       } catch (Exception e) {
       
           e.printStackTrace();
           return "errorPage";
           
       }
       
       model.addAttribute("json", json.toJSONString());
       return "json";
   }
}
