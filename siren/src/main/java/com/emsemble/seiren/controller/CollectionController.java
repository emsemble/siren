package com.emsemble.seiren.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.emsemble.seiren.bo.CollectionBO;
import com.emsemble.seiren.vo.CollectionVO;
import com.emsemble.seiren.vo.SongVO;
import com.google.gson.Gson;

/**
 * 1. 파일 이름 : CollectionController
 * 
 * 2. 작성자 : 김성철
 * 
 * 3. 목적 : Collection 요청 처리 컨트롤러
 * 
 * 4. 이력 :
 * 
 * 2014. 04. 14. 최초 작성
 * 
 * 2014. 04. 18. myList에 있는 곡 확인작업 생성
 * 
 * 2014. 04. 19. 콜렉션 이름 입력받을때 메소드 수정
 * 
 * 2014. 04. 21. 콜렉션 만들고 model에 listName 추가, 곡 추가 삭제 메소드 작성
 * 
 * 2014. 04. 23  저장된 리스트 불러오기, 한번에 저장된곡 없에는 리셋 메소드 작성
 * 
 * 2014. 04. 24. 콜렉션에 설명, 공개여부 설정하는 메소드 추가
 * 
 * 2014. 04. 26. 곡저장 / 삭제시 검증 메소드 추가, Player로 이동할때 컨트롤러 추가
 * 
 * 2014. 04. 27. 내 리스트로 들어갈때 검증 메소드 추가
 * 
 * 2014. 05. 01. colleictionController 전반적 개선(김시온)
 * 
 * 2014. 05. 13. EnterMyList 제거. EnterList메소드로 변경 -> 권한 검증을 하고 새창에 Player를 로드.(고상우)
 * 
 * 2014. 05. 19. songBrowse.jsp에서 ajax로 리스트생성과 동시에 생성한 리스트에 곡 추가하는 메소드 구현(김시온)
 * 
 */

@Controller
@SessionAttributes({"userId","userName", "userStatus"})
public class CollectionController {

	@Autowired
    private CollectionBO collectionBO;
    @Autowired
    private SearchController searchController;
    @Autowired
    private PlayerController playerController;

    
    
    @RequestMapping(value = "/myList.do", method = RequestMethod.GET)
    public String myList(@ModelAttribute("userId") String owner, Model model) {
        List<CollectionVO> collectionList = null;
        List<CollectionVO> followList = null;
        
        
        Gson gson = new Gson();
        
        try {
            
            collectionList = collectionBO.searchMycollection(owner);
            followList = collectionBO.getFollowList(owner);
            model.addAttribute("collectionList", gson.toJson(collectionList));
            model.addAttribute("followList", gson.toJson(followList));
        } catch (Exception e) {
            
            
           
            e.printStackTrace();
            return "errorPage";
        }

        return "myList";  
    } // 해당 유저가 가지고있는 CollectionList를 불러오고, myList.jsp로 이동하기
    
    
    
    @RequestMapping(value = "/collectionInfo.do", method = RequestMethod.GET)
    public String collectionInfo (@RequestParam("listNum") String listNum,@RequestParam("listName") String listName, 
                                  @ModelAttribute("userId") String owner, Model model) {
        
        model.addAttribute("listNum", listNum);
        model.addAttribute("listName", listName);
        
        return "collectionInfo";
    } //콜렉션의 설명과 공개여부 입력하는 페이지로 이동하는 메소드    
    
    
    
    @RequestMapping(value = "/modifyList.do", method = RequestMethod.GET)
    public String modifyList (@RequestParam("listNum") int listNum,
                              @ModelAttribute("userId") String owner,@RequestParam("listName") String listName, Model model) {
        
        return searchController.createCollection(1, "num", listNum, listName, owner, model);
    } //컬렉션의 이름을 받아 수정하는 곳으로 이동


    
    
    /**
     * 목적 : listName을 받고 리스트 생성해주는 메소드
     * @param listName myList.jsp에서 입력받은 리스트 이름 값
     * @param changeName 중복되어서 리스트 이름값이 변했을때 listName을 반환해주는값.
     */
    @RequestMapping(value = "/createCollectionService.do", method = RequestMethod.GET)
    public ModelAndView createCollectionService(@RequestParam("inputValue") String listName,
                                          @ModelAttribute("userId") String owner) {
    	
    	System.out.println(listName);
        
        HashMap listNumNName = new HashMap();
        int listNum = 0;
        
        try {
        
            listNumNName = collectionBO.createCollection(owner, listName);
            listNum = (Integer) listNumNName.get("listNum");
            
        } catch (Exception e) {

            e.printStackTrace();
           
            //return "errorPage";
        }
        
        RedirectView rv = new RedirectView();
        System.out.println(listNumNName.get("listName"));
        rv.setContentType("text/html;charset=UTF-8");
        rv.setEncodingScheme("UTF-8");
        rv.setExposeModelAttributes(false);
        
        
        try {
			rv.setUrl("createCollection.do?listNum="+listNum +"&listName=" + URLEncoder.encode((String) listNumNName.get("listName"), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return new ModelAndView(rv);
        
    } //Collection 을 만들고 곡 선택으로 이동 메소드  
    
    
    
    @RequestMapping(value = "/addSong.do", method = RequestMethod.POST)
    public String addSong (@RequestParam("songNum") int songNum,
                           @RequestParam("listNum") int listNum,
                           @ModelAttribute("userId") String owner, Model model) {
        
        String result     = null;
        
        JSONObject json   = new JSONObject();
        Gson gson = new Gson();

        try {

            result  = collectionBO.addSong(listNum, songNum);
            json.put("result", result);
            json.put("addedSong", gson.toJson(collectionBO.selectSongInfoBySongNum(songNum)));
            
        } catch (Exception e) {
            
            e.printStackTrace();
            return "errorPage";
        
        }
        
        model.addAttribute("json", json);
        return "json";
    } //곡을 추가하는 메소드
    
    
    @RequestMapping(value = "/removeSong.do", method = RequestMethod.POST)
    public String removeSong (@RequestParam("songNum") int songNum,
                              @RequestParam("listNum") int listNum,
                              @ModelAttribute("userId") String owner, Model model) {
        
        String result     = null;
        List<SongVO> songInfo = null;
        JSONObject json = new JSONObject();
        Gson gson = new Gson();

        try {
            
            result = collectionBO.removeSong(listNum, songNum);
            songInfo = collectionBO.selectSongInfoBySongNum(songNum);
            
            json.put("songInfo", gson.toJson(songInfo));
            json.put("result", result);
            
        } catch (Exception e) {
            
            e.printStackTrace();
            return "errorPage";
            
        }
        
        model.addAttribute("json", json);
        return "json";
    } // 곡 삭제시 메소드
    
    
    
    /**
     * 목적 : Ajax통신을 통해 현재 저장되어있는 곡 목록을 뿌려주기
     * @param listName : 현재 List의 이름
     * @param owner    : 로그인한 유저
     * @return : Gson으로 jsonString 만들어서 전달해주기
     */
    @RequestMapping(value = "/saveSongList.do", method = RequestMethod.POST)
    public String saveSongList (@RequestParam("listNum") String listNum,
                                @ModelAttribute("userId") String owner, Model model) {
        
        List<SongVO> songList = null;
        
        JSONObject json = new JSONObject();
        Gson gson       = new Gson();
        
        try {
          
            songList = collectionBO.saveSongList(Integer.parseInt(listNum));
            
            json.put("result", "success");
            json.put("saveSongList", gson.toJson(songList));
            
        } catch (Exception e) {
            
            json.put("result", "fail");

            e.printStackTrace();
            return "errorPage";
        }
        
        model.addAttribute("json", json);
        return "json";
    }
    

    
    /**
     * 목적 : 리스트 초기화 버튼을 누를시 저장되어 있는 곡의 목록들이 전부 삭제되는 메소드
     *        모두 삭제된 후에 곡 정보를 나타내는 searchController의 초기형태로 돌아간다.
     */
    @RequestMapping(value = "/resetList.do", method = RequestMethod.GET)
    public String resetList (@RequestParam("listNum") int listNum, @RequestParam("listName") String listName, 
                             @ModelAttribute("userId") String owner, Model model) {

        
        try {
            
            collectionBO.resetMyCollection(listNum);
            
        } catch (Exception e) {
            
            e.printStackTrace();
            return "errorPage";
        }       
        
        return searchController.createCollection(1, "num", listNum, listName, owner, model);
    }
    
    
    
    
    /**
     * 목적 : 콜렉션의 설명과 공개여부를 받아서 DB에 업데이트 시켜주는 메소드
     * @param description : 콜렉션의 설명
     * @param ispublic    : 공개[기본값] / 비공개 설정 1이면 공개, 2일때 비공개
     * 
     * 저장이 완료되면 myList로 돌아간다.
     */
    @RequestMapping(value = "/inputCollectionInfo.do", method = RequestMethod.POST)
    public ModelAndView inputCollectionInfo (@RequestParam("inputDescription") String description,
                                       @RequestParam("isPublic") int ispublic,
                                       @RequestParam("tags") ArrayList<String> tags,
                                       @RequestParam("listNum") int listNum,
                                       @ModelAttribute("userId") String owner,
                                       Model model) {
        
        try {
            collectionBO.inputCollectionInfo(listNum, description, ispublic, tags);
            
        } catch (Exception e) {
            
            e.printStackTrace();
            
            RedirectView rv = new RedirectView("errorPage.do");
            rv.setExposeModelAttributes(false);
            
            return new ModelAndView(rv);
        }
        
        RedirectView rv = new RedirectView("myList.do");
        rv.setExposeModelAttributes(false);

        return new ModelAndView(rv);
    }
    
   

    /**
     * 목적 : myList에서 특정 리스트를 클릭했을때 곡의 개수가 30개 미만인지를 검증
     */
    @RequestMapping(value = "/validateThirtySong.do", method=RequestMethod.GET)
    public String clickMyList (@RequestParam("listNum") int listNum,
                               @ModelAttribute("userId") String owner, Model model) {
        int countSongResult = 0;
        JSONObject json     = new JSONObject();
        
        try {
            countSongResult = collectionBO.clickMyList(listNum);
            
            if (countSongResult > 0) {
                
                json.put("result", "success");
            
            } else {
                
                json.put("result", "fail");
            }
            
            model.addAttribute("json", json);
            
        } catch (Exception e) {
            
            e.printStackTrace();
            return "errorPage";
        }
        
        return "json";
    }
    
    
    
    /**
     * 목적 : 해당 리스트 요청에 대해 권한을 검증하고, 플레이어 창을 로드한다.
     */
    @RequestMapping(value = "/enterList.do", method=RequestMethod.GET)
    public String enterList (@RequestParam ("listNum") int listNum,  Model model) {                
        int res;
        String id = null;
        
        if(model.containsAttribute("userId")){
            id = (String)model.asMap().get("userId");
        }
        
        try {
            
            res = collectionBO.getAuthority(listNum, id);
            
        } catch (Exception e) {

            e.printStackTrace();
            return "errorPage";
        }
        
        if(res==0){
            return "noAuthority";
        }
        
        else{
             model.addAttribute("listNum", listNum);
        }
       
        return "player";
    }
    
    
    
    @RequestMapping(value = "/saveThumbnail.do", method=RequestMethod.POST)
    public String saveThummbnail (@RequestParam ("thumbnail") String thumbnail,
                                    @RequestParam ("collectionNum") int collectionNum,
                                   Model model) {
       
        if(model.containsAttribute("userId")){
            collectionBO.saveThumbnail(collectionNum, thumbnail, (String)model.asMap().get("userId"));
        }
        
        return "json";
    }
    
    
    
    @RequestMapping(value = "/deleteThumbnail.do", method=RequestMethod.POST)
    public String delteThummbnail (@RequestParam ("listNum") int collectionNum,
                                   Model model) {
       
        if(model.containsAttribute("userId")){
            collectionBO.deleteThumbnail(collectionNum);
        }
        
        return "json";
    }
    
    

    /*
     *  목적 : list 생성과 동시에 생성한 리스트에 해당 곡 추가하는 메소드
     */
    @RequestMapping(value = "/makeList.do", method=RequestMethod.GET) 
    public String makeColleListAddSong (@RequestParam("listName") String listName, 
    									@ModelAttribute ("userId") String owner, 
    									@RequestParam("songNum") int songNum, Model model ){
    	
    	 int collectionNum = 0;
    	 HashMap listNumNName = new HashMap();
         String result     = null;
         JSONObject json = new JSONObject();
         
         try {
         
        	 listNumNName = collectionBO.createCollection(owner, listName);
             
             result  = collectionBO.addSong((Integer) listNumNName.get("listNum"), songNum);
             
             json.put("isSuccess", true);
             
             
         } catch (Exception e) {

             e.printStackTrace();
            
             return "errorPage";
         }	
    	
        model.addAttribute("json", json);
    	return "json";
    }
    
    
}