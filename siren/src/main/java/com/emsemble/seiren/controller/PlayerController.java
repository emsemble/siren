package com.emsemble.seiren.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.emsemble.seiren.bo.ClusteringBO;
import com.emsemble.seiren.bo.CollectionBO;
import com.emsemble.seiren.vo.CollectionVO;
import com.emsemble.seiren.vo.PlayerVO;
import com.emsemble.seiren.vo.SongVO;
import com.google.gson.Gson;

/**
 * 1. 파일 이름 : PlayerController
 * 
 * 2. 작성자 : 고상우
 * 
 * 3. 목적 : Player생성 요청 처리
 * 
 * 4. 이력 :
 * 
 * 2014. 04. 12. 최초 작성
 * 
 * 2014. 04. 13. 클래스 이름 변경. ClusterContorller -> PlayerController
 * 
 * 2014. 04. 14. 클러스터링 관련 테스팅 코드 작성
 * 
 * 2014. 04. 17. CollectionVO와 연동을 위한 작업 시작
 * 
 * 2014. 04. 25. PlayerVO 연동 완료.
 * 
 * 2014. 04. 28. 연결선 개선
 * 
 * 2014. 05. 13. CollectionController와 의존성 분리.
 */
@Controller
public class PlayerController {

    private static final Logger logger = LoggerFactory.getLogger(PlayerController.class);
    

    @Autowired
    private ClusteringBO clusteringBO;

    @Autowired
    private CollectionBO collectionBO;


    
    @RequestMapping(value = "/getPlayer.do", method = RequestMethod.POST)
    public String getPlayer(@RequestParam ("listNum") int listNum,                    
                          @RequestParam ("width") int width,
                          @RequestParam ("height") int height,
                          Model model) {
        
        String json = null;
        
        try {
            json = collectionBO.getPlayer(listNum);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(json == null){
            return savePlayer(listNum, width, height, model);
        }       
        
        model.addAttribute("json", json);        
        return "json";
    }
    
 
    
    @RequestMapping(value = "/savePlayer.do", method = RequestMethod.POST)
    public String savePlayer(@RequestParam ("listNum") int listNum,                    
                          @RequestParam ("width") int width,
                          @RequestParam ("height") int height,
                          Model model) {

        CollectionVO collection = null;
        HashMap corrTbl = null;
        HashMap realDistance = null;
        HashMap<Integer, double[]> locations = null;
        ArrayList kCentroids = null;
        HashMap kCluster = null;
        HashMap<Integer, HashMap<Integer, Double>> lines = null;
        HashMap harmonyList = null;
        
        try {
           collection = collectionBO.getCollectionHavingSong(listNum);
        } catch (Exception e) {
            e.printStackTrace();
        }  
        
        
        //CollectionVO에서 전달받을 HashMap
        HashMap<Integer, SongVO> songList = collection.getSongList();
        
        //임시로 사용할 값들
//      songList.put(10, new SongVO(10, "Gerry Rafferty-Baker Street.mp3"));
//      songList.put(11, new SongVO(11, "Daniel Boone-Beautiful Sunday.mp3"));
//      songList.put(27, new SongVO(27, "Culture Club-Do You Really Want To Hurt Me.mp3"));
//      songList.put(38, new SongVO(38, "Irene Cara-Flashdance..What A Feeling.mp3"));
//      songList.put(55, new SongVO(55, "All-4-One-04-I Swear.mp3"));
//      songList.put(77, new SongVO(77, "Los Del Rio-06-Macarena (Bayside Boys Remix).mp3"));
//      songList.put(91, new SongVO(91, "Juice Newton-Queen Of Hearts.mp3"));
//      songList.put(99, new SongVO(99, "Vanessa Williams-14-Save The Best For Last.mp3"));
//      songList.put(123, new SongVO(123, "Toni Braxton-04-Un-Break My Heart.mp3"));
//      songList.put(141, new SongVO(141, "Bon Jovi-You Give Love A Bad Name.mp3"));
//      songList.put(190, new SongVO(190, "조덕배-02-꿈에.mp3"));

        // 1. 플레이어 원형 노드 생성
        //PlayerVO player = new PlayerVO(songList,  sizeWidth, sizeHeight);
        PlayerVO player = new PlayerVO(songList,  width, height);
        
        // 곡이 하나도 없으면 플레이어 정보를 제거한다.
        if(songList.size() == 0){
            collectionBO.deletePlayer(listNum);
            return "json";
        }
        
        else if(songList.size() != 1){
            //2. 대각 행렬을 이용해 거리를 뽑아내기 위해 songNum을 오름차순으로 정렬해 songNumList를 생성한다
            ArrayList songNumList = new ArrayList(new TreeSet(songList.keySet()));
            
            //3-1. 실제 거리를 추출한다        
            try {
                realDistance = clusteringBO.getRealDistance(songNumList);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            
            //3-2. 거리를 기반으로 좌표값을 뽑아낸다. -> 해당 좌표값을 PlayerVO.circles에 저장한다
            locations = clusteringBO.getLocation(songNumList, realDistance);
            player.initCircleLoc(locations);
            
            //4-1. 군집의 초기 중심점 값을 추출한다
            try {
                kCentroids = clusteringBO.getKCentroids(songNumList);
            } catch (SQLException e) {
                e.printStackTrace();
            }       
            
            //4-2. 초기 중심점과 좌표값을 기반으로 군집을 뽑아낸다. -> 해당 군집 정보를 PlayerVO.circles에 저장한다.
            kCluster = clusteringBO.getKCluster(songNumList, kCentroids, locations);      
            player.initCircleColor(kCluster);
            
            
            
            //5. 상관관계에 기반하여 연결선을 뽑아낸다. -> 해당 연결선 정보를 PlayerVO.lines에 저장한다.
            try {
                lines = clusteringBO.getLines(kCluster);
            } catch (SQLException e) {
                e.printStackTrace();
            }      
             
            player.initLines(lines, locations);
             
            
            //6. 좌표의 최대 최소값을 고려하여 scale을 결정한다.
            player.initScale();
        
        
            //7. 플레이리스트(군집내 재생 순서)를 초기화한다.
            try {
                harmonyList = clusteringBO.getPlayList(kCluster);
            } catch (SQLException e) {
                e.printStackTrace();
            }       
            
            player.setHarmonyList(harmonyList);
        }// end of if (songList.size() != 1) : 이상의 메소드는 곡이 두곡 이상일 때 가능함. 한곡일때는 군집을 따질 수없음
        
        //곡이 한곡일때는 다음을 수행함.
        else{
            int songNum = songList.keySet().iterator().next();
            SongVO oneSong = songList.get(songNum);
            
            double[] location = {(width * 3/4)/2.0, (height-250)/2.0};
            HashMap locMap = new HashMap();
            locMap.put(songNum, location);
            
            //1. 해당곡을 플레이어 중앙에 위치시킨다 -> 해당 좌표값을 PlayerVO.circles에 저장한다
            player.initCircleLoc(locMap);
            
            HashMap cluster = new HashMap();
            cluster.put(songNum, new ArrayList(Arrays.asList(songNum)));
            
            //2. 해당곡을 유일 군집으로 할당한다. 
            player.initCircleColor(cluster);
        }
        
        Gson gson = new Gson();
        String json = "{'player': '"+gson.toJson(player)
                +"', 'collectionNum' :'"+collection.getNum()
                +"', 'collectionName' : '"+collection.getName()
                +"', 'collectionOwner' : '"+collection.getOwner()
                +"'}";
      
        collectionBO.savePlayer(listNum, json);
       
        model.addAttribute("json", json);
        
        return "json";
    }
    
    
    
    @RequestMapping(value = "/incrPlayNum.do", method = RequestMethod.POST)
    public String incrPlayNum(@RequestParam ("listNum") int listNum,                    
                          @RequestParam ("songNum") int songNum,
                          Model model) {
        
        collectionBO.incrPlayNum(listNum, songNum);
        collectionBO.incrTotPlayNum(songNum);
        return "json";
    }
}