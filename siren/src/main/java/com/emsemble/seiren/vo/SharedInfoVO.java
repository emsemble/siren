package com.emsemble.seiren.vo;

/**
 * 1. 파일 이름 : SharedInfoVO.java 
 * 2. 작성자 : 김성철
 * 3. 목적 : SharedInfo 클래스 작성
 * 
 * 4. 이력 : 
 * 2014. 05. 14. 중간완성
 * 
 * 5. 설명 : SongVO에서 상속받은 변수들과 상세정보 보기에서 사용할 변수들을 작성한 클래스.
 * 
 *           year에는 해당 년도, count는 해당 장르가 몇곡있는지 판단해 저장 ,rank는 해당 장르가 몇등으로 많이 있는지 저장
 */

public class SharedInfoVO extends SongVO{

    private int collectionNum;
    private String collectionName;
    private String collectionDescription;
    private int year;
    private int count;
    private int rank;
    
    public SharedInfoVO() {
        super();
    }
    
    public SharedInfoVO(int collectionNum, String collectionName, String collectionDescription, int year, int count, int rank) {
        super();
        this.collectionName = collectionName;
        this.collectionNum = collectionNum;
        this.year = year;
        this.count = count;
        this.rank = rank;
    }
    
    public SharedInfoVO(int collectionNum, String genre, int rank, int count) {
        this.collectionNum = collectionNum;
        super.setGenre(genre);
        this.rank = rank;
        this.count = count;
    }



    public int getCollectionNum() {
        return collectionNum;
    }

    
    
    public void setCollectionNum(int collectionNum) {
        this.collectionNum = collectionNum;
    }

    
    
    public int getYear() {
        return year;
    }

    
    
    public void setYear(int year) {
        this.year = year;
    }

    
    
    public int getCount() {
        return count;
    }

    
    
    public void setCount(int count) {
        this.count = count;
    }

    
    
    public int getRank() {
        return rank;
    }

    
    
    public void setRank(int rank) {
        this.rank = rank;
    }

    
    
    public String getCollectionName() {
        return collectionName;
    }

    
    
    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    
    
    public String getCollectionDescription() {
        return collectionDescription;
    }

    
    
    public void setCollectionDescription(String collectionDescription) {
        this.collectionDescription = collectionDescription;
    }

    
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SharedInfoVO [collectionNum=");
        builder.append(collectionNum);
        builder.append(", collectionName=");
        builder.append(collectionName);
        builder.append(", collectionDescription=");
        builder.append(collectionDescription);
        builder.append(", year=");
        builder.append(year);
        builder.append(", count=");
        builder.append(count);
        builder.append(", rank=");
        builder.append(rank);
        builder.append("]");
        return builder.toString();
    }
}
