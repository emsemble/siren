package com.emsemble.seiren.vo;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 1. 파일이름 : userVo
 * 2. 작성자 : 김시온
 * 3. 목적 : user 클래스 생성
 * 
 */
@Component
@Scope(value="prototype")
public class UserVO {
    
    private int status;      //사용자 상태(관리자0, 사용자 1부여)
    private String id;        //사용자 아이디
    private String name;  //사용자 이름
    private String pw;      //사용자 비밀번호
    
    
    public UserVO() {}

    

    public UserVO (String id, String pw, String name) {
        super();
        this.id   = id;
        this.pw   = pw;
        this.name = name;
    }
    
    
    
    public UserVO(int status, String id, String name, String pw) {
        super();
        this.status = status;
        this.id = id;
        this.name = name;
        this.pw = pw;
    }
    
    
    
    public int getStatus() {
        return status;
    }

    

    public void setStatus(int status) {
        this.status = status;
    }

    

    public String getId() {
        return id;
    }

    

    public void setId(String id) {
        this.id = id;
    }

    

    public String getName() {
        return name;
    }


    
    public void setName(String name) {
        this.name = name;
    }

    

    public String getPw() {
        return pw;
    }


    
    public void setPw(String pw) {
        this.pw = pw;
    }


    
    @Override
    public String toString() {
        return "UserVO [status=" + status + ", id=" + id + ", name=" + name + ", pw=" + pw + "]";
    }
    
}
