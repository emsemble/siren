package com.emsemble.seiren.vo;

import org.springframework.stereotype.Component;


/**
 * 1. 파일 이름 : CommnetVO
 * 
 * 2. 작성자 : 김시온
 * 
 * 3. 목적 : 댓글 CRUD를 위한 객체 생성
 * 
 * 4. 이력 :
 * 
 *  2014.05.02 최초 생성
 *  	: 멤버변수  num, writer, collectionNum, contents, today 생성
 * 
 */

@Component
public class CommentVO {

	private int num;
	private String writer;
	private int collectionNum;
	private String contents;
	private String writeTime;
	private boolean isMyComment;
	
	
	public CommentVO() {}

	
	public CommentVO(String writer, int collectionNum, String contents,
			String writeTime) {
		this.writer = writer;
		this.collectionNum = collectionNum;
		this.contents = contents;
		this.writeTime = writeTime;
	}

	
	public CommentVO(String writer, int collectionNum, String contents,
	                 String writeTime, boolean isMyComment) {

	    this.writer = writer;
        this.collectionNum = collectionNum;
        this.contents = contents;
        this.writeTime = writeTime;
        this.isMyComment = isMyComment;
	}
	                    
	
	
	public CommentVO(int num, String writer, int collectionNum,
			String contents, String writeTime, boolean isMyComment) {
		this.num = num;
		this.writer = writer;
		this.collectionNum = collectionNum;
		this.contents = contents;
		this.writeTime = writeTime;
		this.isMyComment = isMyComment;
	}

	

	public int getNum() {
		return num;
	}


	
	public void setNum(int num) {
		this.num = num;
	}

	

	public String getWriter() {
		return writer;
	}

	

	public void setWriter(String writer) {
		this.writer = writer;
	}

	

	public int getCollectionNum() {
		return collectionNum;
	}


	
	public void setCollectionNum(int collectionNum) {
		this.collectionNum = collectionNum;
	}

	

	public String getContents() {
		return contents;
	}


	
	public void setContents(String contents) {
		this.contents = contents;
	}


	
	public String getWriteTime() {
		return writeTime;
	}


	
	public void setToday(String writeTime) {
		this.writeTime = writeTime;
	}


	
	public boolean isMyComment() {
		return isMyComment;
	}


	
	public void setMyComment(boolean isMyComment) {
		this.isMyComment = isMyComment;
	}


	@Override
	public String toString() {
		return "CommentVO [num=" + num + ", writer=" + writer
				+ ", collectionNum=" + collectionNum + ", contents=" + contents
				+ ", writeTime=" + writeTime + ", isMyComment=" + isMyComment + "]";
	}

	
}
