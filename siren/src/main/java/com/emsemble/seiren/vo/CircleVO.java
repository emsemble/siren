package com.emsemble.seiren.vo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 1. 파일 이름 : CircleVO
 * 
 * 2. 작성자 : 고상우
 * 
 * 3. 목적 : 특정 곡을 원형 노드로 표현하기 위한 클래스
 * 
 * 4. 이력 :
 * 
 *  2014.04.18 최초 생성
 * 
 */
@Component
@Scope(value="prototype")
public class CircleVO {

    // CollectionVO에서 받을 정보 필드값
    int songNum;

    // ClusteringBO에서 연산해낼 필드값
    double[] location;
    int color = -1;


    public CircleVO() {}

    public CircleVO(int songNum) {
        this.songNum = songNum;
    }

    
    public int getSongNum() {
        return songNum;
    }

        
    
    public void setSongNum(int songNum) {
        this.songNum = songNum;
    }

  
    public double[] getLocation() {
        return location;
    }

    
    
    public void setLocation(double[] location) {
        this.location = location;
    }

    
    
    public int getColor() {
        return color;
    }

    
    
    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "CircleVO [songNum=" + songNum + ", location=" + Arrays.toString(location) + ", color=" + color + "]";
    }
    
    
}
