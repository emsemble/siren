package com.emsemble.seiren.vo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 1. 파일 이름 : LineVO
 * 
 * 2. 작성자 : 고상우
 * 
 * 3. 목적 : 곡간의 상관관계를 연결선으로 표현하기 위한 클래스
 * 
 * 4. 이력 :
 * 
 * 2014. 04. 18. 최초 작성
 * 
 */
@Component
@Scope(value="prototype")
public class LineVO {
    
    int source;
    double[] sourceLoc;
    int target;
    double[] targetLoc;
    double corr;
 
    
    public LineVO(){}
    
    public LineVO(int source, int target, double corr, double[] sourceLoc, double[] targetLoc){
        this.source = source;
        this.target = target;
        this.corr = corr;
        this.sourceLoc = sourceLoc;
        this.targetLoc = targetLoc;
    }

    
    public int getSource() {
        return source;
    }
    
    

    public void setSource(int source) {
        this.source = source;
    }

    
    
    public int getTarget() {
        return target;
    }

    
    
    public void setTarget(int target) {
        this.target = target;
    }

    
    
    public double getCorr() {
        return corr;
    }

    
    
    
    public void setCorr(double corr) {
        this.corr = corr;
    }
}
