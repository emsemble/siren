package com.emsemble.seiren.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 1. 파일 이름 : PlayerVO
 * 
 * 2. 작성자 : 고상우
 * 
 * 3. 목적 : 플레이어 생성에 필요한 정보들을 담는 객체
 * 
 * 4. 이력 :
 * 
 * 2014. 04. 13. 최초 작성
 * 
 * 2014. 04. 19. Visualize를 위한 코드 작성
 * 
 * 2014. 04. 25. 플레이 순서 결정을 위한 코드 작성
 * 
 * 
 */
@Component
@Scope(value="prototype")
public class PlayerVO {
    
    //json 변환 이후 d3라이브러리에서 정상작동하려면 리스트 타입으로 만들어야함. 콜렉션 타입은 작동하지 않음
    ArrayList<CircleVO> circles = new ArrayList();
    ArrayList<LineVO> lines = new ArrayList();
    
    HashMap<Integer, SongVO> songList;
    HashMap<Integer, ArrayList> harmonyList;
    
    double width = 960; 
    double height = 600;
    
    
    double scaleX = 1;
    double scaleY = 1;
    double errorX = 15;
    double errorY = 15;
    
    
    public PlayerVO() {}
    
    /**
     * 리스트를 받아와 Player에 내장한다.
     * 리스트로 부터 Circle들의 기본 정보를 받아 Circle객체를 생성하는 부분
     * @param songList
     */
    public PlayerVO(HashMap<Integer, SongVO> songList, int width, int height) {
        this.songList = songList;
        this.width = width * 3/4;
        this.height = height-250;
        
        Iterator<Integer> itr = songList.keySet().iterator();
        int songNum;
        
        while (itr.hasNext()) {
            songNum = itr.next();
            circles.add(new CircleVO(songNum));
        }
    }

    
    
    /**
     * 대상 원형 노드들의 좌표값 초기화
     * @param locations
     */
    public void initCircleLoc(HashMap<Integer, double[]> locations) {
        for(CircleVO target : circles){
           target.setLocation(locations.get(target.getSongNum()));
        }//end of for()
    }//end of initCircleLoc


    
    /**
     * 대상 원형 노드들의 컬러값 초기화
     * @param kCluster
     */
    public void initCircleColor(HashMap<Integer, ArrayList<Integer>> kCluster) {
        Iterator<Integer> itr = kCluster.keySet().iterator();
        int centroid;
        

        for(int i = 0; i < kCluster.size(); i++){
            centroid = itr.next();

            for(CircleVO target : circles){
                
                if(kCluster.get(centroid).contains(target.getSongNum())){            
                    target.setColor(i);
                }
            }
        }       
    }
    
    
    
    /**
     * 연결선 초기화
     * @param hMaplines
     */
    public void initLines(HashMap<Integer, HashMap<Integer, Double>> hMaplines, HashMap<Integer, double[]> locations) {
        Iterator<Integer> rowKeyItr = hMaplines.keySet().iterator();
        Iterator<Integer> colKeyItr = null;
        int source;
        int target;
        
        while(rowKeyItr.hasNext()){
            source = rowKeyItr.next();
            colKeyItr = hMaplines.get(source).keySet().iterator();
            
            while(colKeyItr.hasNext()){
                target = colKeyItr.next();
                this.lines.add(new LineVO(source, target, hMaplines.get(source).get(target), 
                                            locations.get(source), locations.get(target)));
            }           
        }        
    }

    
    
    /**
     *  scale 초기화(좌표값 임의 부여에 따라, svg안에 꽉 차지 않을 경우와 음수 값이 좌표인 경우를 보정하기 위한 메소드)
     */
    public void initScale(){
        double minX = 0.0;
        double minY = 0.0;
        double maxX = 0.0;
        double maxY = 0.0;
        
        for(int i = 0; i < circles.size(); i++){
            CircleVO target = circles.get(i);
            
            if(i == 0){
                minX = target.location[0];
                minY = target.location[1];
                maxX = target.location[0];
                maxY = target.location[1];
            }//end of if
            
            else{
                
                if(maxX < target.location[0]){
                    maxX = target.location[0];
                }//end of if
                
                if(maxY < target.location[1]){
                    maxY = target.location[1];
                }//end of if
                
                if(minX > target.location[0]){
                    minX = target.location[0];
                }//end of if
                
                if(minY > target.location[1]){
                    minY = target.location[1];
                }//end of if
                
            }//end of else
        }//end of for()
       
        this.scaleX = (this.width - 60) / (maxX - minX);
        this.scaleY = (this.height - 60) / (maxY - minY);
                        
          
        if(minX < 0){
            errorX = (-1 * minX) * scaleX + 15;
        }
        
        if(minY < 0){
            errorY = (-1 * minY) * scaleY + 15;
        }
        
        if(scaleX * maxX > this.width){
            errorX = -1* (scaleX * maxX - this.width) - 15;
        }
        
        if(scaleY * maxY > this.height){
            errorY = -1* (scaleY * maxY - this.height) - 15;
        } 
    }

   
    
    public ArrayList<CircleVO> getCircles() {
        return circles;
    }
    
    

    public void setCircles(ArrayList<CircleVO> circles) {
        this.circles = circles;
    }
    
    

    public ArrayList<LineVO> getLines() {
        return lines;
    }
    
    

    public void setLines(ArrayList<LineVO> lines) {
        this.lines = lines;
    }
    
    

    public double getWidth() {
        return width;
    }
    
    

    public void setWidth(double width) {
        this.width = width;
    }
    
    

    public double getHeight() {
        return height;
    }
    
    

    public void setHeight(double height) {
        this.height = height;
    }
    
    

    public double getScaleX() {
        return scaleX;
    }
    
    

    public void setScaleX(double scaleX) {
        this.scaleX = scaleY;
    }
    
    
    
    public double getScaleY() {
        return scaleY;
    }
    
    

    public void setScaleY(double scaleY) {
        this.scaleY = scaleY;
    }    
    

    public double getErrorX() {
        return errorX;
    }
    
    

    public void setErrorX(double errorX) {
        this.errorX = errorX;
    }
    
    

    public double getErrorY() {
        return errorY;
    }
    
    

    public void setErrorY(double errorY) {
        this.errorY = errorY;
    }
    
    

    public HashMap<Integer, SongVO> getSongList() {
        return songList;
    }
    

    
    public void setSongList(HashMap<Integer, SongVO> songList) {
        this.songList = songList;
    }

    
    
    public HashMap<Integer, ArrayList> getHarmonyList() {
        return harmonyList;
    }

    
    
    public void setHarmonyList(HashMap<Integer, ArrayList> playList) {
        this.harmonyList = playList;
    }

    
    
    @Override
    public String toString() {
        return "PlayerVO [circles=" + circles + ", lines=" + lines + ", songList=" + songList + ", harmonyList=" + harmonyList + ", width=" + width
                + ", height=" + height + ", scaleX=" + scaleX + ", scaleY=" + scaleY + ", errorX=" + errorX + ", errorY=" + errorY + "]";
    }
}
