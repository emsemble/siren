package com.emsemble.seiren.vo;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 1. 파일 이름 : CollectionVO 
 * 2. 작성자 : 김성철
 * 3. 목적 : CollectionVO 클래스 작성
 * 
 * 2014. 04. 24. 설명, 공개여부 업데이트시 쓸 생성자 추가
 * 
 * 2014. 04. 26. 콜렉션 안에 HashMap 추가, 생성자 변경 및 getter/ Setter 작성
 * 
 * 2014. 04. 30. 내가 팔로우 하고있는지 판단하는 변수 추가
 * 
 * 2014. 05. 13. 가장 많이 플레이한 곡의 albumid 넣는 변수 추가, 총 follower 수 넣는 변수 추가, vote 변수 삭제
 * 
 * 2014. 05. 23. 사용자가 콜렉션의 특성 입력해주는 tags변수 추가
 */

@Component
@Scope(value="prototype")
public class CollectionVO {

    private int     num;            // Collection의 DB Number
    private String  owner;          // 해당 Collection의 사용자 ID
    private String  name;           // 사용자가 지정한 Collection의 이름
    private String  description;    // 사용자가 작성한 Collection의 간단한 설명
    private int     ispublic;       // 사용자의 Collection 공개 / 비공개 설정
    private String  amiFollowing;   // 내가 팔로우 하고있는지 나타내는 변수
    private HashMap<Integer, SongVO> songList;       // 현재 음악 리스트를 담는 SongList
    private String  thumbnail;
    private String  albumid;         // 컬렉션의 가장 많이 플레이된 곡의 albumID
    private int     totFollower;     // 총 팔로워 수 집계
    private ArrayList<String> tags;       // 사용자가 추가로 입력하는 tag 정보
    
    public CollectionVO () {}

    public CollectionVO (int num, String owner, String name, String description, 
                         int ispublic, String amiFollowing, HashMap<Integer, SongVO> songList, 
                         String thumbnail, String albumid, int totFollower, ArrayList<String> tags) {
        super();
        this.num = num;
        this.owner = owner;
        this.name = name;
        this.description = description;
        this.ispublic = ispublic;
        this.amiFollowing = amiFollowing;
        this.songList = songList;
        this.thumbnail = thumbnail;
        this.albumid = albumid;
        this.totFollower = totFollower;
        this.tags = tags;
    }
    
    public CollectionVO (int num, String owner, String name, String description, String amiFollowing) {
        
        this.num = num;
        this.owner = owner;
        this.name = name;
        this.description = description;
        this.amiFollowing = amiFollowing;
    }
    
    public CollectionVO (String owner, String name, String description, int ispublic) {
        super();
        this.owner = owner;
        this.name = name;
        this.description = description;
        this.ispublic = ispublic;
    }
    
    public CollectionVO (String owner, String name, int ispublic) {
        super();
        this.owner = owner;
        this.name = name;
        this.ispublic = ispublic;
    }
    
    public CollectionVO (int num, String description, int ispublic, ArrayList<String> tags) {
        super();
        this.num = num;
        this.description = description;
        this.ispublic = ispublic;
        this.tags = tags;
    }
    
    public CollectionVO (int num, String description, int ispublic) {
        super();
        this.num = num;
        this.description = description;
        this.ispublic = ispublic;
    }
    
    public CollectionVO (String owner, String name) {
        super();
        this.owner = owner;
        this.name = name;
    }
    
    
    
    public int getNum() {
        return num;
    }

    
    
    public void setNum(int num) {
        this.num = num;
    }

    
    
    public String getOwner() {
        return owner;
    }

    
    
    public void setOwner(String owner) {
        this.owner = owner;
    }

    
    
    public String getName() {
        return name;
    }

    
    
    public void setName(String name) {
        this.name = name;
    }

    
    
    public String getDescription() {
        return description;
    }

    
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    
    public int getIspublic() {
        return ispublic;
    }

    
    
    public void setIspublic(int ispublic) {
        this.ispublic = ispublic;
    }

    
    
    public HashMap<Integer, SongVO> getSongList() {
        return songList;
    }


    
    public void setSongList(HashMap<Integer, SongVO> songList) {
        this.songList = songList;
    }
    
    
    
    public String getAmiFollowing() {
        return amiFollowing;
    }

    
    
    public void setAmiFollowing(String amiFollowing) {
        this.amiFollowing = amiFollowing;
    }

    
    
    public String getThumbnail() {
        return thumbnail;
    }

    
    
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    
    
    public String getAlbumid() {
        return albumid;
    }

    
    
    public void setAlbumid(String albumid) {
        this.albumid = albumid;
    }

    
    
    public int getTotFollower() {
        return totFollower;
    }

    
    
    public void setTotFollower(int totFollower) {
        this.totFollower = totFollower;
    }

    
    
    public ArrayList<String> getTags() {
        return tags;
    }

    
    
    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }
    
}
