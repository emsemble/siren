package com.emsemble.seiren.vo;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 
 * 1. 파일 이름 : SongVO 
 * 
 * 2. 작성자 : 김시온
 * 
 * 3. 목적 : 곡 검색에 필요한 SongVO객체 생성
 * 
 * 4. 특이사항 : song table 과 artist table  칼럼 합친 VO
 * 
 * 5. 이력 : 2014. 04. 15 최초 작성
 * 			   2014. 05. 15 isTitleSong, isAdultSong 타입변경(String -> bollean)
 * 								 isHitSong 변수 추가
 */
@Component
@Scope(value="prototype")
public class SongVO {
    
    private int num;
    private int playTime;
    private int playNum;
    private int totPlayNum;

    private String songName;
    private String artistName;
    private String genre;
    private String issueDate;
    private String fileName;
    private String artistSex;
    private String nationalityName;
    private String actTypeName;
    
    private boolean isTitleSong;
    private boolean isAdultSong;
    private boolean isHitSong;
    private String albumId;
    
    public SongVO () {}


    public SongVO(int num, String songName, String artistName, String genre, String issueDate,
            String nationalityName, String actTypeName, String albumId) {
        
        this.num = num;
        this.songName = songName;
        this.artistName = artistName;
        this.genre = genre;
        this.issueDate = issueDate;
        this.nationalityName = nationalityName;
        this.actTypeName = actTypeName;
        this.albumId = albumId;
    } //곡 검색했을 시 기본적으로 보여질 필드들만 생성자에 추가 (곡 넘버, 곡이름, 가수이름, 장르, 발매일, 국적, 활동타입)


    public SongVO(int num, int playTime, int playNum, int totPlayNum, String songName,
            String artistName, String genre, String issueDate, String fileName, String artistSex,
            String nationalityName, String actTypeName, boolean isTitleSong, boolean isAdultSong,boolean isHitSong, String albumId) {
   
        this.num = num;
        this.playTime = playTime;
        this.playNum = playNum;
        this.totPlayNum = totPlayNum;
        this.songName = songName;
        this.artistName = artistName;
        this.genre = genre;
        this.issueDate = issueDate;
        this.fileName = fileName;
        this.artistSex = artistSex;
        this.nationalityName = nationalityName;
        this.actTypeName = actTypeName;
        this.isTitleSong = isTitleSong;
        this.isAdultSong = isAdultSong;
        this.isHitSong = isHitSong;
        this.albumId = albumId;
        
    }//모든 필드값을 가지고 있는 생성자


    public SongVO(int songNum, String fileName) {
        this.num = songNum;
        this.fileName = fileName;
    }//playerVO 관련 테스트용 생성자.


    public int getNum() {
    
        return num;
    
    }


    
    public void setNum(int num) {
    
        this.num = num;
    
    }


    
    public int getPlayTime() {
    
        return playTime;
    
    }


    
    public void setPlayTime(int playTime) {
    
        this.playTime = playTime;
    
    }


    
    public int getPlayNum() {
    
        return playNum;
    
    }

    

    public void setPlayNum(int playNum) {
    
        this.playNum = playNum;
    
    }


    
    public int getTotPlayNum() {
    
        return totPlayNum;
    
    }


    
    public void setTotPlayNum(int totPlayNum) {
    
        this.totPlayNum = totPlayNum;
    
    }


    
    public String getSongName() {
    
        return songName;
    
    }

    

    public void setSongName(String songName) {
    
        this.songName = songName;
    
    }


    
    public String getArtistName() {
    
        return artistName;
    
    }


    
    public void setArtistName(String artistName) {
    
        this.artistName = artistName;
    
    }


    
    public String getGenre() {
    
        return genre;
    
    }

    

    public void setGenre(String genre) {
    
        this.genre = genre;
    
    }

    

    public String getIssueDate() {
    
        return issueDate;
    
    }

    

    public void setIssueDate(String issueDate) {
    
        this.issueDate = issueDate;
    
    }

    

    public String getFileName() {
    
        return fileName;
    
    }


    
    public void setFileName(String fileName) {
    
        this.fileName = fileName;
    
    }


    
    public String getArtistSex() {
    
        return artistSex;
    
    }


    
    public void setArtistSex(String artistSex) {
    
        this.artistSex = artistSex;
    
    }


    
    public String getNationalityName() {
    
        return nationalityName;
    
    }


    
    public void setNationalityName(String nationalityName) {
    
        this.nationalityName = nationalityName;
    
    }


    
    public String getActTypeName() {
    
        return actTypeName;
    
    }


    
    public void setActTypeName(String actTypeName) {
    
        this.actTypeName = actTypeName;
    
    }

    

    public boolean getIsTitleSong() {
    
        return isTitleSong;
    
    }


    
    public void setIsTitleSong(boolean isTitleSong) {
    
        this.isTitleSong = isTitleSong;
    
    }

    

    public boolean getIsAdultSong() {
    
        return isAdultSong;
    
    }


    
    public void setIsAdultSong(boolean isAdultSong) {
    
        this.isAdultSong = isAdultSong;
    
    }
    
    
    
    public boolean getIsHitSong() {
        
        return isHitSong;
    
    }


    
    public void setIsHitSong(boolean isHitSong) {
    
        this.isAdultSong = isHitSong;
    
    }


    
    public String getAlbumId() {
      
        return albumId;
    
    }

    

    public void setAlbumId(String albumId) {
    
        this.albumId = albumId;
    
    }


    
    @Override
    public String toString() {
        return "SongVO [num=" + num + ", playTime=" + playTime + ", playNum=" + playNum
                + ", totPlayNum=" + totPlayNum + ", songName=" + songName + ", artistName="
                + artistName + ", genre=" + genre + ", issueDate=" + issueDate + ", fileName="
                + fileName + ", artistSex=" + artistSex + ", nationalityName=" + nationalityName
                + ", actTypeName=" + actTypeName + ", isTitleSong=" + isTitleSong
                + ", isAdultSong=" + isAdultSong + ", isHitSong=" +isHitSong +", albumId=" + albumId + "]";
    }
    
}
