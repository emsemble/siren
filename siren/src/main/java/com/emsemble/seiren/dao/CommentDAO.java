package com.emsemble.seiren.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.emsemble.seiren.vo.CommentVO;


/**
 * 1. 파일 이름 : CommentDAO
 * 
 * 2. 작성자 : 김시온
 * 
 * 3. 목적 : 댓글 관련 DAO
 * 
 * 4. 이력 :
 * 
 *  2014.05.02 최초 생성
 *
 */

@Component
public class CommentDAO extends SqlSessionDaoSupport{

	@Autowired
    private SqlSession sqlsession;


	//해당 collection에 등록된 모든 댓글 SELECT
	public List<CommentVO> selectAllComment(int collectionNum) throws SQLException {
		
		return getSqlSession().selectList("commentDAO.selectAllComment", collectionNum);
		
	} 
	
	
	
	//댓글 INSERT
	public int insertComment(CommentVO commentVO) throws SQLException {
		
		return getSqlSession().insert("commentDAO.insertComment", commentVO);
		
	}


	
	//특정 댓글 SELECT
	public CommentVO selectCommentByCommentVO(CommentVO commentVO) throws SQLException {
		
		return getSqlSession().selectOne("commentDAO.selectCommentByCommentVO", commentVO);
		
	}



	//특정 댓글 DELETE
	public int deleteCommentByNum(int commentNum) throws SQLException {
		
		return getSqlSession().delete("commentDAO.deleteCommentByNum", commentNum);
		
	}



	//특정 댓글 UPDATE
	public int updateComment(HashMap cmtMap) throws SQLException {
	
		return getSqlSession().update("commentDAO.updateComment", cmtMap);
		
	}

	
}
