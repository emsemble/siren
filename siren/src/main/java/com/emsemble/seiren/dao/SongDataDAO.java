package com.emsemble.seiren.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 1. 파일 이름 : SongDataDAO
 * 
 * 2. 작성자 : 고상우
 * 
 * 3. 목적 : song_data 테이블로 부터 데이터 추출
 * 
 * 4. 이력 :
 * 
 * 2014. 04. 12 최초 작성
 * 
 * 2014. 04. 14 중심점 회수 메소드 추가
 */
@Component
public class SongDataDAO extends SqlSessionDaoSupport {

    @Autowired
    private SqlSession sqlSession;  // 세션받음.

    

    /**
     * 목적 : 콜렉션에서 보유곡 id 목록을 받아서 상관계수를 해쉬맵으로 받아내는 메소드
     */
    public List selectCorrelation(ArrayList<Integer> songList) throws SQLException {
        return getSqlSession().selectList("songDataDAO.selectCorrelation", songList);
    }


    /**
     * 목적 : 콜렉션에서 보유곡 id 목록을 받아서 상관계수를 거리화하여 받아내는 메소드
     */
    public List selectDistance(ArrayList<Integer> songList) throws SQLException{
        return getSqlSession().selectList("songDataDAO.selectDistance", songList);
    }



    /**
     * 목적 : 상관계수에 기반해 K-cluster의 centroid를 결정하기 위한 메소드
     */
    public List<HashMap> selectKCentroids(ArrayList<Integer> songList) throws SQLException {
        List<HashMap> kCentroids = getSqlSession().selectList("songDataDAO.selectKCentroidStrictly", songList);
        
        //뚜렷한 음적 관계를 보이는 대상이 없는 경우 약한 음적 관계를 가진 대상을 중심점으로 사용한다.
        if (kCentroids.size() == 0) {
            kCentroids = getSqlSession().selectList("songDataDAO.selectKCentroidWeakly", songList);
        }
        return kCentroids;
    }
    
    
    
    /**
     * 목적 : 상관계수에 기반해 연결선을 결정하기 위한 메소드
     */
    public List<HashMap> selectLines(ArrayList<Integer> songList) throws SQLException {
        List<HashMap> kCentroids = getSqlSession().selectList("songDataDAO.selectLinesWeakly", songList);
        
        return kCentroids;
    }
    
    
    
    /**
     * 목적 : 멤버들의 조화도를 회수하기 위한 메소드
     */
    public List<HashMap> selectHarmony(ArrayList<Integer> members) throws SQLException {
        List<HashMap> harmony = getSqlSession().selectList("songDataDAO.selectHarmony", members);
   
        return harmony;
    }
}