package com.emsemble.seiren.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.emsemble.seiren.vo.CollectionVO;
import com.emsemble.seiren.vo.SongVO;
/**
 * 1. 파일 이름 : CollectionDAO
 * 
 * 2. 작성자 : 김성철
 * 
 * 3. 목적 : Collection과 관련한 데이터 전송
 * 
 * 4. 이력 :
 * 
 * 2014. 04. 14 최초 작성
 * 
 * 2014. 04. 18 내 콜렉션 DB에서 조회 메소드 작성
 * 
 * 2014. 04. 21 콜렉션 만드는 메소드 수정, 곡 추가 삭제 메소드 작성
 * 
 * 2014. 04. 23 저장된 리스트 불러오기, 한번에 저장된곡 없에는 리셋 메소드 작성
 * 
 * 2014. 04. 24 콜렉션 DB에 공개여부, 설명 추가 메소드 작성
 * 
 * 2014. 04. 26 해당 콜렉션에 중복되는 노래 있는지 여부 체크하는 메소드 작성, 
 *              선택한 리스트 정보 불러오기 작성
 *              
 * 2014. 04. 27 해당 리스트에 곡이 몇개있는지 판단 메소드 작성    
 * 
 * 2014. 05. 13 권한 검증 메소드, playNum 증가 추가(고상우)
 */

@Component
public class CollectionDAO extends SqlSessionDaoSupport{

    @Autowired
    private SqlSession sqlsession; 
    
    
    
    public int createCollection (CollectionVO collectionVO) throws Exception {
        
        return getSqlSession().insert("collection.createCollection", collectionVO);
                
    }//바뀐 이름을 가지고 곡 집어넣는 작업시 정보조회. 
    


    public int checkCollectionName (CollectionVO collectionVO) throws Exception {
        
        return getSqlSession().selectOne("collection.checkCollectionName", collectionVO);
    
    }// 콜렉션의 이름 중복되었는지 확인 
    
    
    
    public List<CollectionVO> searchMycollection (String owner) throws Exception {
        
        return getSqlSession().selectList("collection.searchMycollection", owner);
        
    }// 콜렉션 표시할시에 정보 가져오기
    
    
    
    public int addSong (HashMap song) throws Exception {
        
        return getSqlSession().insert("collection.addSong", song);
    }// Song 집어넣기
    
    
    
    public int removeSong (HashMap delSong) throws Exception {
        
        return getSqlSession().delete("collection.removeSong", delSong);
    }// Song 삭제하기

    

    public int searchDuplicateSong(HashMap song) throws Exception{
        
        return getSqlSession().selectOne("collection.searchDuplicateSong", song);
    }// 해당 리스트에 중복되는 노래있는지 체크
    
    
    
    public List<SongVO> saveSongList (int collectionNum) throws Exception {
        
        return getSqlSession().selectList("collection.saveSongList", collectionNum);
    }//  저장된 song의 List 반환
    
    
    
    public int resetMyCollection (int collectionNum) throws Exception {
        
        return getSqlSession().delete("collection.resetMyCollection", collectionNum);
    }// Collection에 저장되어 있는 리스트들 한번에 삭제



    public int inputCollectionInfo(CollectionVO collectionVO) throws Exception {
        
        return getSqlSession().update("collection.inputCollectionInfo", collectionVO);
    }// 콜렉션의 설명, 공개여부 업데이트 메소드
    
    
    
    public CollectionVO selectedCollectionInfo (int collectionNum) throws Exception {
        
        return getSqlSession().selectOne("collection.selectedCollectionInfo", collectionNum);
    }//  선택된 콜렉션의 정보 보여주기



    public int clickMyList(int collectionNum) throws Exception{

        return getSqlSession().selectOne("collection.countSongs", collectionNum);
    }// 클릭한 리스트의 곡 수를 반환해주는 메소드

    
    
    public int updateThumbnail(HashMap hMap) {
        
        return getSqlSession().update("collection.updateThumbnail", hMap);
        
    }
    
    
    
    public int deleteThumbnail(int collectionNum) {
        
        return getSqlSession().update("collection.deleteThumbnail", collectionNum);
        
    }
    
    
    
    public int updatePlayer(HashMap hMap) {

        return getSqlSession().update("collection.updatePlayer", hMap);
        
    }
    
    
    
    public int deletePlayer(int collectionNum) {

        return getSqlSession().update("collection.deletePlayer", collectionNum);
        
    }
    
    
    
    public String selectPlayer(int collectionNum){
        
        return getSqlSession().selectOne("collection.selectPlayer", collectionNum);
    }



    public List<SongVO> selectSongInfoBySongNum(int songNum) {

        return getSqlSession().selectList("collection.selectSongInfoBySongNum", songNum);
        
    }



    public int selectAutority(HashMap idAndNum) {

        return getSqlSession().selectOne("collection.selectAuthority", idAndNum);
    }

    
    
    public int incrPlayNum(HashMap listAndSong) {

        return getSqlSession().selectOne("collection.incrPlayNum", listAndSong);
    }
    
    
    
    public List<CollectionVO> selectMyCollectionLists(String owner) throws SQLException {
        
        return getSqlSession().selectList("collection.selectMyCollectionLists", owner);
    
    }
    
    
    
    public List<CollectionVO> selectFollowList(String id){
        
        return getSqlSession().selectList("collection.selectFollowList", id);
    }
}
