package com.emsemble.seiren.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.emsemble.seiren.vo.CollectionVO;
import com.emsemble.seiren.vo.SharedInfoVO;

/**
 * 1. 파일 이름 : SharedCollectionDAO.java
 * 
 * 2. 작성자 : 김성철
 * 
 * 3. 목적 : SharedList에 관한 DB와의 통신시 메소드 작성
 * 
 * 4. 이력 : 2014. 04. 28. 최초 작성
 * 
 *           2014. 05. 14. 중간 완성
 *           
 *           2014. 06. 02. 필터, 검색스트링, 내 리스트 찾는 메소드 등 추가
 * 
 */
@Component
public class SharedCollectionDAO extends SqlSessionDaoSupport{
    
    @Autowired
    private SqlSession sqlsession;
    
    
    
    /**
     * 목적 : 공개된 컬렉션을 조회해 리스트로 가져오는 메소드
     */
    public List<CollectionVO> searchPublicCollection(HashMap<String, Object> searchData) throws Exception {
        
        return getSqlSession().selectList("shared.searchPublicCollection", searchData);
    }

    
    
    /**
     * 목적 : DB에 팔로우를 추가하는 메소드
     */    
    public int addFollowList(HashMap<String, Object> follow) throws Exception {

        return getSqlSession().insert("shared.addFollowList", follow);
    }
    
    
    
    /**
     * 목적 : DB에 팔로우를 삭제하는 메소드
     */    
    public int deleteFollowList(HashMap<String, Object> follow) throws Exception {
        
        return getSqlSession().delete("shared.deleteFollowList", follow);
    }


    
    /**
     * 목적 : 팔로우가 중복되어있는지 확인하는 메소드
     */    
    public int searchDuplicateFollow(HashMap<String, Object> follow) throws Exception{

        return getSqlSession().selectOne("shared.searchDuplicateFollow", follow);
    }


    
    /**
     * 목적 : 선택된 리스트의 상세정보를 보여주는 메소드
     */    
    public List<SharedInfoVO> getAddtionalInfo(int collectionNum) throws Exception{
        
        return getSqlSession().selectList("shared.getAddtionalInfo", collectionNum);
    }
    
    
    
    /**
     * 목적 : 선택된 리스트에서 곡의 장르가 가장 많은 순서대로 순위를 나타내주는 메소드
     */    
    public List<SharedInfoVO> getGenreRank(int collectionNum) throws Exception{
        
        return getSqlSession().selectList("shared.getGenreRank", collectionNum);
    }


    
    /**
     * 목적 : 가장 많이 재생되는 노래를 반환하는 메소드,
     * 
     * DB에서는 HashMap으로 반환하고, album_id만 가져와서 mostPlayedSong 스트링값만 전달해준다.
     */    
    public String searchMostPlaySong(int collectionNum) throws Exception{
        
        HashMap<String, Object> mostPlay = getSqlSession().selectOne("shared.searchMostPlaySong", collectionNum);
        String mostPlayedSong            = null;
        
        if(mostPlay != null){
            
            mostPlayedSong = (String)mostPlay.get("album_id");
        }
                
        return mostPlayedSong;
    }

    

    /**
     * 목적 : 해당 리스트의 총 팔로워 수를 반환하는 메소드
     */    
    public int getTotalFollower(int collectionNum) throws Exception{
        
        return getSqlSession().selectOne("shared.getTotalFollower", collectionNum);
    }


    
    /**
     * 목적 : 공유된 리스트 추가하려고 할때 내 리스트인지 검색하는 메소드.
     */
    public String searchIsMyList(int collectionNum) throws Exception{
        
        return getSqlSession().selectOne("searchIsMyList", collectionNum);
    }
    
    /**
     * 목적 : 필터 파라미터로 필터링한후 해당하는 리스트를 반환하는 메소드
     */
    public List<CollectionVO> filteringList(HashMap<String, Object> filterData) throws Exception{
        
        return getSqlSession().selectList("shared.filteringList", filterData);
    }


    /**
     * 목적 : 검색한 String 으로 검색한후 해당하는 리스트 반환
     */
    public List<CollectionVO> searchListByString(HashMap<String, String> searchData) throws Exception{

        return getSqlSession().selectList("shared.searchListByString", searchData);
    }


    /**
     * 목적 : 내가 팔로우 하고있는 리스트 검색후 해당하는 리스트 반환
     */
    public List<CollectionVO> myFollowingList(String userId) throws Exception{
        
        return getSqlSession().selectList("shared.myFollowingList", userId);
    }	
    
}
