package com.emsemble.seiren.dao;

import java.sql.SQLException;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.emsemble.seiren.vo.UserVO;

@Component
public class UserDAO extends SqlSessionDaoSupport {

	@Autowired
	private SqlSession sqlSession;//세션받음.
	HashMap userInfoHm = null;
    
	
	
	public UserVO selectUserById(String userId) throws SQLException {
        
	    return getSqlSession().selectOne("userDAO.selectUserById", userId);
    
	}//User의 Id로, 매칭되는 사용자 정보 가져오는 메소드



    public int deleteUserByUserId(String userId) {
       
        return getSqlSession().delete("userDAO.deleteUserByUserId", userId);
    
    }//User의 Id로 매칭되는 사용자 계정 삭제하는 메소드



    public int updateNewPasswordByUserId(String newPassword, String userId) {
        
        userInfoHm = new HashMap();
        
        userInfoHm.put("newPassword", newPassword);
        userInfoHm.put("userId", userId);
        
        return getSqlSession().update("userDAO.updateNewPasswordByUserId", userInfoHm);
    
    }//사용자 비밀번호와 id를 map에 저장시킨 후 해당 사용자의 비밀번호를 수정해주는 메소드



    public int updateNewNameByUserId(String newName, String userId) {
        
       userInfoHm = new HashMap();
        
       userInfoHm.put("newName", newName);
       userInfoHm.put("userId", userId);
        
        return getSqlSession().update("userDAO.updateNewNameByUserId", userInfoHm);
    }//사용자 비밀번호와 id를 map에 저장시킨 후 해당 사용자의 비밀번호를 수정해주는 메소드

    
    
    public int signUpUser (UserVO userVO) throws Exception {
        
        return getSqlSession().insert("userDAO.signUpUser", userVO);
        
    }//회원가입 메소드
    
    
    
    public int findUserStatus (String id) throws Exception {
        
        return getSqlSession().selectOne("userDAO.findUserStatus", id);
        
    }//User의 상태 가져오기
    
    
    
    public String searchDuplicateId(String id) throws SQLException {
        
        return getSqlSession().selectOne("userDAO.searchDuplicateId", id);
        
    }//입력한 ID로 DB에 중복되는 아이디 있는지 검사
}
