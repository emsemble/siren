package com.emsemble.seiren.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.emsemble.seiren.vo.CollectionVO;
import com.emsemble.seiren.vo.SongVO;

 
 /**
 * 1. 파일 이름 : SongDAO
 * 
 * 2. 작성자 : 김시온
 * 
 * 3. 목적 : song 테이블에서 곡 정보 추출
 * 
 * 4. 이력 :
 * 
 * 2014. 04. 16 최초작성
 *  : song table 과 artist table 조인작업, 기본적인 곡정보(곡 넘버, 곡이름, 가수이름, 장르, 발매일, 국적, 활동타입)
 *      만 가지고 오는 메소드 생성
 *      
 * 2014. 05. 11 random으로 song 정보 빼오는 메소드 생성
 * 2014. 05. 22 songBroswe에서 filter기능 수행하는 메소드 생성
 * 
 */
 
@Component
public class SongDAO extends SqlSessionDaoSupport {
    
    @Autowired
    private SqlSession sqlSession;//세션받음.
    
    
    public List<SongVO> selectAllSongInfo (int start, int count, String sortType, int collectionNum) throws SQLException {
       
        HashMap<String, Object> pHm = null;
        pHm = new HashMap<String, Object>();
        
        pHm.put("start", start); 
        pHm.put("count", count);
        pHm.put("sortType", sortType);
        pHm.put("collectionNum", collectionNum);
        
        return getSqlSession().selectList("SongDAO.selectAllSongInfo", pHm);
    
    }// 기본적인 곡정보 반환하는 메소드
    
    
    
    public int countAllSongInfo (int collectionNum) throws SQLException {
        
        return getSqlSession().selectOne("SongDAO.countAllSongInfo", collectionNum);
    
    } // 전체 곡 갯수 반환 하는 메소드
    

    
    public List<SongVO> selectSongBySearchKeyword( String searchKeyword ) throws SQLException {
        
        return getSqlSession().selectList( "SongDAO.selectSongBySearchKeyword",  "%"+searchKeyword+"%");
    
    }



    public List<SongVO> selectAllSongList() throws SQLException{
    
        return getSqlSession().selectList("SongDAO.selectAllSongList");    
    }

    
    
    public int incrTotPlayNum(int listNum){
        
        return getSqlSession().selectOne("SongDAO.incrTotPlayNum", listNum); 
    }

    
    
	public ArrayList<List> selectSongByGenre(ArrayList<String> gArray) throws SQLException {
		
		ArrayList<List> songArray = new ArrayList();
		
		for(int i = 0; i < gArray.size(); i++) {
			songArray.add(getSqlSession().selectList("SongDAO.selectSongByGenre", gArray.get(i)));
		}
		
		return songArray;
		
	}//random으로 song SELECT



	public List<SongVO> selectFilterCB(HashMap<String, ArrayList> valuesMap)  throws SQLException{
		
		return getSqlSession().selectList("SongDAO.selectFilterCB", valuesMap);
	
	}



	public List<SongVO> searchByGenre(String genre)  throws SQLException{
		
		return getSqlSession().selectList("SongDAO.searchByGenre", genre);
				
	}



	public List<SongVO> searchByYear(String year)  throws SQLException{
		
		return getSqlSession().selectList("SongDAO.searchByYear", year);
				
	}



	public List<SongVO> searchKeyword(HashMap<String, String> keyMap) throws SQLException{
		
		return getSqlSession().selectList("SongDAO.searchKeyword", keyMap);

	}



}
