/**
 *  1. 파일 이름 : signUp.js
 *  
 *  2. 작성자 : 김성철
 *
 *  3. 목적 : 회원가입에서 버튼 클릭 및 입력 변경시 제어 
 * 			  회원가입시 아이디 중복체크 / 회원가입 유효한지 체크
 * 
 * _onClickSignUpButton : 회원가입 버튼 눌렀을때 유효성 확인 / 유효하다면 DB에 넣는 이벤트 발생
 * _searchDuplicateID : ID입력하여 가입하는 TextBox값의 변경시 AJAX 통해 ID중복 되어있는지 확인
 * _checkVaildPW : PW입력하여 패스워드 유효한지 Key입력할때마다 화면에 보여주기
 * _checkVailName : 이름 입력하여 길이가 적당한지 화면에 보여주기
 *
 *  4. 이력 : 2014. 04. 11 최초 작성
 *  		  2014. 04. 14 유효성 검증 작업 등 완료
 */

var SignUp = $Class({

	_oElement : null,
	_oEvent : null,
	
	
	
	$init : function() {
		$Fn(this._initialize, this).attach(window, 'load');
		$Fn(this._destroy, this).attach(window, 'unload');
	},
	
	
	
	_initialize : function() {
		this._oElement = {};
		this._oEvent = {};
		
		this._setElement();
		this._setEvent();
	},
	
	
	
	_destroy : function(){
		
	},
	
	
	
	_setElement : function(){
		this._oElement['signUpButton']   = $('signUpButton');
		this._oElement['watchInputID']   = $('signUpId');
		this._oElement['watchInputPW']   = $('signUpPw');
		this._oElement['watchInputName'] = $('signUpName');
	},
	
	
	
	_setEvent : function(){
		this._oEvent['clickSignUpButton'] = $Fn(this._onClickSignUpButton, this).attach(this._oElement['signUpButton'],'click');
		this._oEvent['WatchInputID'] 	  = $Fn(this._searchDuplicateID, this).attach(this._oElement['watchInputID'], 'change');
		this._oEvent['WatchInputPW'] 	  = $Fn(this._checkVaildPW, this).attach(this._oElement['watchInputPW'], 'change');
		this._oEvent['WatchInputName']	  = $Fn(this._checkVaildName, this).attach(this._oElement['watchInputName'], 'change');
	},
	
	
	/**
	 * 목적 : 회원가입 버튼을 누른후 유효성 검증 
	 * @param form 			   : Submit할 파라미터들 Form에 담는 객체
	 * @param isClickIdCheck   : ID중복확인을 했는지 확인하는 변수, [0 = 확인 안함, 1 = 중복, 2 = 사용가능 아이디]
	 * @param isPwCheck 	   : 입력한 Pw를 사용했는지 확인하는 변수, [0 = 중복 혹은 사용불가능한 비밀번호, 1 = 사용가능] 
	 * @param stringByteLength : Name의 Byte수를 확인하는 변수
	 */
	_onClickSignUpButton : function(oEvent) {

		var form = $Form(signUpForm);

		var signUpId	   = $('signUpId').value;
		var	signUpPw	   = $('signUpPw').value;
		var signUpPw2	   = $('signUpPw2').value; 
		var	signUpName     = $('signUpName').value;
		
		var isClickIdCheck   = $('isClickIdCheck').value;
		var isPwCheck	     = $('isPwCheck').value;
		var stringByteLength = $('stringByteLength').value;
		
		if(signUpId != "" && signUpId.match(/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+\.([a-zA-Z0-9]){1,3}$/)){
			
			if(isPwCheck == 1){
				
				if(signUpPw == signUpPw2){

					if(signUpName != "" && stringByteLength < 19){
						
						if (isClickIdCheck == 0) {
							alert("ID중복체크를 하지 않으셨습니다");
						
						} else if (isClickIdCheck == 1) {
							alert("중복되는 아이디가 있습니다");							
						
						} else if (isClickIdCheck == 2) {
							
							if (confirm("해당 정보로 가입하시겠습니까?") == true){
								form.submit();
							
							} else {
								$('signUpId').focus();

							}; //LastCheck
							
						}; //end of clickIDCheck
													
					} else {
						alert('이름은 한글 6자, 영문 18자 이내로 입력해주세요.');
						
						$('signUpName').focus();
					}; // end of NameCheck

				} else {
					alert('패스워드가 일치하지 않습니다');
					
					$('signUpPw').value = "";
					$('signUpPw2').value = "";
					$('signUpPw').focus();
				}; // end of PWduplicate Check
				
			} else {
				alert('패스워드는 6-12자리의 영문과 숫자를 혼용해서 써주세요');
				
				$('signUpPw').value = "";
				$('signUpPw2').value = "";
				$('signUpPw').focus();
			}; // end of PWvalidate Check
			
		} else {
			alert('ID가 이메일 형식이 아닙니다.');
			
			$('signUpId').value = "";
			$('signUpId').focus();
		}; // end of IDCheck
		
	},
	
	
	
	/**
	 * 목적 : DB에 AJAX를 이용해 바로 중복된 아이디가 있는지 없는지 체크한 후,
	 * 		  중복되었는지, 사용가능한 아이디인지 isClickIdCheck 파라미터를 변경한다.
	 */
	_searchDuplicateID : function(oEvent) {
		var oAjaxParam = {
			"signUpId" : $('signUpId').value
		};
		
		var oAjaxOption = {
			timeout : 5,
			method  : 'post',
			onload  : function(oResponse) {
				
				var oJson = oResponse.json();
				
				if (oJson) {
					
					if (oJson.isDuplicate) {
						
						var sMsg = oJson.msg;
						$Element('ajaxCheck').html(sMsg);
						
						$('signUpId').focus();
						
						$('isClickIdCheck').value = 1;

					} else {
						var sMsg = oJson.msg;
						$Element('ajaxCheck').html(sMsg);
						
						$('isClickIdCheck').value = 2;
					};
				};
			},
			
			ontimeout : function(oResponse) {
				alert("아이디 중복 체크에 실패했습니다.");
			},
			
			onerror : function(oResponse) {
				alert("아이디 중복 체크에 실패했습니다.");
			}
		};
		
		var sUrl = "/seiren/searchDuplicateId.do";
		var oAjax = new $Ajax(sUrl, oAjaxOption);
		oAjax.request(oAjaxParam);
		
		oEvent.stop();
	},
	
	
	
	/**
	 * 목적 : signUpPw의 값을 받아 사용가능한 비밀번호인지 확인하기 위함
	 * @param signUpPwChecknum : 비밀번호에 숫자가 들어가 있는지 확인
	 * @param signUpPwCheckeng : 비밀번호에 영어 대/소문자가 들어있는지 확인
	 */
	_checkVaildPW : function(oEvent) {
		var signUpPwValue = $('signUpPw').value;
		var signUpPwChecknum = signUpPwValue.search(/[0-9]/); 
		var signUpPwCheckeng = signUpPwValue.search(/[a-zA-Z]/);
			
		if( signUpPwValue.match(/^[a-zA-Z0-9]{6,12}$/) && signUpPwChecknum > -1 && signUpPwCheckeng > -1 ){
			$Element('pwCheck').html("비밀번호를 사용할 수 있습니다.");
			$('isPwCheck').value = 1;
			
		} else if (signUpPwValue == "") {
			$Element('pwCheck').html("비밀번호는 6-12자리의 영문+숫자로 입력해주세요");
			$('isPwCheck').value = 0;
			
		} else {
			$Element('pwCheck').html("사용할 수 없는 비밀번호 입니다.");
			$('isPwCheck').value = 0;
		}
	},
	
	
	
	/**
	 * 목적 : DB에 저장시 이름이 20Byte길이가 넘어가면 안되기 때문에 저장시 이름을 확인 
	 * @param stringByteLength : UTF-8형태의 한글 (3Byte) 구별하여 계산한 값, 파라미터로 보내 유효성체크에 사용
	 */
	_checkVaildName : function(oEvent) {
		var signUpNameValue = $('signUpName').value;
		var stringByteLength = ( function (s,b,i,c) {
			for(b=i=0;c=s.charCodeAt(i++);b+=c>>11?3:c>>7?2:1);
			return b;
			}) (signUpNameValue);
		
		if (stringByteLength > 18) {
			$Element('nameCheck').html("이름이 너무 깁니다.");
			$('stringByteLength').value = stringByteLength;
		} else {
			$Element('nameCheck').html("이름은 한글 6자, 영문 18자 이내로 입력해주세요.");
			$('stringByteLength').value = stringByteLength;
		}
		
	}
});
	