/**
 * 1. 파일 이름 : myList.js 
 * 
 * 2. 작성자 : 고상우
 * 
 * 3. 목적 : 나의 리스트를 웹상에서 보여주는 스크립트.
 * 
 * 4. 특이사항 : WebGL(Three.js) / 3d Animation(Tween.js) 등 사용
 * 
 * 5. 이력 : 
 * 
 * 2014. 05. 24. : 페이지 전면 개편.
 * 
 */
window.onload = function() {
	  var element = document.getElementsByTagName('text');
	  element.onselectstart = function () { return false; }; // ie
	  element.onmousedown = function () { return false; }; // mozilla
}

//GLOBAL VAR
var scene;
var cssScene;
var camera;
var clock;
var engine;
var controls;
var renderer;
var rendererCSS;
var mSphere;
var fSphere;
var isCamLookAtMList = true;
var isRotating = true;
var rotatingObjAry = [];
var htmlObjAry = {};
var cameraPosition = {
	mSphere : new THREE.Vector3(-140, -10, 200) ,	
	fSphere : new THREE.Vector3(160, 90, 200)
};
var smallSphereTexture = [];

var engineValues = {
	    positionStyle    : Type.CUBE,
	    positionBase     : new THREE.Vector3( 0, 0, 0 ),
	    positionSpread   : new THREE.Vector3( 800, 600, 600),
	                
	    velocityStyle    : Type.CUBE,
	    velocityBase     : new THREE.Vector3( 0, 0, 0 ),
	    velocitySpread   : new THREE.Vector3( 0.5, 0.5, 0.5 ), 

	    angleBase               : 0,
	    angleSpread             : 720,
	    angleVelocityBase       : 0,
	    angleVelocitySpread     : 4,
		
	    particleTexture : THREE.ImageUtils.loadTexture( 'static/img/particle.png' ),
	    
	    sizeBase    : 2.0,
	    sizeSpread  : 1.0,				
	    colorBase   : new THREE.Vector3(0.15, 1.0, 0.9), // H,S,L
	    colorSpread : new THREE.Vector3(0.2, 0.2, 0.2),
	    opacityBase : 1,

	    particlesPerSecond : 20000,
	    particleDeathAge   : 60.0,		
	    emitterDeathAge    : 0.1
	};


//METHOD
function initWebGl(){
	scene = new THREE.Scene();
	cssScene = new THREE.Scene();
	
	camera = new THREE.PerspectiveCamera(45, window.innerWidth/ window.innerHeight, 0.1, 20000);
	camera.position.copy(cameraPosition.mSphere);
	
	scene.add(camera);
	
	renderer = new THREE.WebGLRenderer({alpha : true});
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);
	
	
	// htmlObj를 렌더링하기위한 렌더러
	rendererCSS	= new THREE.CSS3DRenderer({alpha : true});
	rendererCSS.setSize( window.innerWidth, window.innerHeight );
	rendererCSS.domElement.style.position = 'fixed';
	rendererCSS.domElement.style.top	  = 0;
	rendererCSS.domElement.style.left	  = 0;
	rendererCSS.domElement.style.margin	  = 0;
	rendererCSS.domElement.style.padding  = 0;
	renderer.setClearColor(0xffffff,1);
	
	
	document.body.appendChild( rendererCSS.domElement );

	rendererCSS.domElement.appendChild( renderer.domElement );

	
	controls = new THREE.OrbitControls(camera, renderer.domElement);
	controls.autoRotate = true;
	controls.autoRotateSpeed = 0.1;
	controls.maxDistance = 500;


	var skyBoxGeometry = new THREE.CubeGeometry(10000, 10000, 10000);
/*	var imagePrefix = "static/img/sky-";
	var directions  = ["xpos", "xneg", "ypos", "yneg", "zpos", "zneg"];
	var imageSuffix = ".png";
	
	var materialArray = [];
	for (var i = 0; i < 6; i++)
		materialArray.push( new THREE.MeshBasicMaterial({
			map: THREE.ImageUtils.loadTexture( imagePrefix + directions[i] + imageSuffix ),
			side: THREE.BackSide
		}));
	var skyBoxMaterial = new THREE.MeshFaceMaterial( materialArray );	*/	
	var skyBoxMaterial = new THREE.MeshBasicMaterial({
		color : 0x000000,
		side : THREE.BackSide
	});
	var skyBox = new THREE.Mesh(skyBoxGeometry, skyBoxMaterial);
	scene.add(skyBox);
	
	scene.fog = new THREE.FogExp2(0x2e343e, 0.00025);	

	var light = new THREE.PointLight(0xffffff);
	light.position.set(-100, 250, 100);
	scene.add(light);
	var ambientLight = new THREE.AmbientLight(0x111111);
	scene.add(ambientLight); 	
	
	 
	//mSPHERE : myList 중심
	var mSphereTexture = THREE.ImageUtils.loadTexture('static/img/mPlanetTexture.png');
	var mSphereGeometry = new THREE.SphereGeometry(20, 50, 50);
	var mSphereMaterial = new THREE.MeshLambertMaterial({
		map : mSphereTexture,
		color : 0xffffff
	});

	mSphere = new THREE.Mesh(mSphereGeometry, mSphereMaterial);
 
	mSphere.position.x = -200;
	mSphere.position.y = 0;
	mSphere.position.z = 0;
	camera.lookAt(mSphere.position);
	controls.center.copy(mSphere.position);
	scene.add(mSphere); 
	
	 
	 
	//fSphere : followList 중심
	var fSphereTexture = THREE.ImageUtils.loadTexture('static/img/fPlanetTexture.png');
	var fSphereGeometry = new THREE.SphereGeometry(20, 50, 50);
	var fSphereMaterial = new THREE.MeshLambertMaterial({
		map : fSphereTexture,
		color : 0xffffff
	});
	fSphere = new THREE.Mesh(fSphereGeometry, fSphereMaterial);

	fSphere.position.x = 200;
	fSphere.position.y = 100;
	fSphere.position.z = 0;
	scene.add(fSphere);
	
	for(var i = 6; i > 0; i--){
		smallSphereTexture.push("small" + i +".png");
	}
	
	//SPHERE : mSphere를 중심으로 회전하는 세개짜리 메뉴 행성
	mkRotatingObj(100, 60.0, (1 / 3.0) * 2 * Math.PI, mSphere);
	mkRotatingObj(100, 60.0, (2 / 3.0) * 2 * Math.PI, mSphere);
	mkRotatingObj(100, 60.0, (3 / 3.0) * 2 * Math.PI, mSphere);


	//SPHERE : fsphere를 중심으로 회전하는 세개짜리 메뉴 행성
	mkRotatingObj(100, 60.0, (1 / 3.0) * 2 * Math.PI, fSphere);
	mkRotatingObj(100, 60.0, (2 / 3.0) * 2 * Math.PI, fSphere);
	mkRotatingObj(100, 60.0, (3 / 3.0) * 2 * Math.PI, fSphere);
	

	for(var i = 0; i < $(".mList").size(); i++){
	   		mkHtmlObj(100, 30.0, (i / $(".mList").size()) * 2 * Math.PI, mSphere, $(".mList").eq(i));
	}
	   
	for(var i = 0; i < $(".fList").size(); i++){
	  		mkHtmlObj(100, 30.0, (i / $(".fList").size()) * 2 * Math.PI, fSphere, $(".fList").eq(i));
	}
	 
	clock = new THREE.Clock();

	engine = new ParticleEngine();	
	engine.setValues(engineValues);
	engine.initialize();
	setInterval(function(){initParticleEngine();}, 30000);
	 
	render();
}

	

//순환하는 행성 만들기. 반지름, 주기(초), 초기각(라디안), 순환 타겟을 파라미터로 받는다.
function mkRotatingObj(r, t, angle, targetO){
	var sSphereTexture = THREE.ImageUtils.loadTexture('static/img/' + smallSphereTexture.pop());
	var sphereMaterial = new THREE.MeshLambertMaterial({
		map : sSphereTexture,
		color : 0xffffff
	});
	
	var sphereGeometry = new THREE.SphereGeometry(5, 50, 50);
	var sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);


	sphere.position.x = getXPos(r, angle, targetO.position.x);
	sphere.position.y = targetO.position.y;
	sphere.position.z = getZPos(r, angle, targetO.position.z);
	
	var obj = { obj : sphere , angle : angle, time : t, r : r, target : targetO};
	
	rotatingObjAry.push(obj);
	
	scene.add(sphere);
	sphere = null;
}



//html태그를 렌더링 하기위한 메소드. 기본적으로 mkRotatingObj와 비슷한 로직을 가지고 있음.
//TO-DO : rotatingOBJ와 중복 되는 메소드 해소하기.
function mkHtmlObj(r, t, angle, targetO, $_element){
	//뼈대 세우기
	var planeMaterial   = new THREE.MeshNormalMaterial( { transparent: true, opacity: 0 } );
	
	var planeHeight = 50;
	var planeWidth = 80;
		
	var planeGeometry = new THREE.PlaneGeometry( planeWidth,  planeHeight);
	var plane= new THREE.Mesh( planeGeometry, planeMaterial);
	
	plane.position.x = getXPos(r, angle, targetO.position.x);
	plane.position.y = (targetO.position.y + Math.floor(Math.random() * 50) - 25);
	plane.position.z = getZPos(r, angle, targetO.position.z); 
	
	var obj = { obj : plane , angle : angle, time : t, r : r, target : targetO};

	scene.add(plane);	
	
	var element = $_element[0];
	
	var elementWidth = screen.width * (3/4);
	var aspectRatio = planeHeight / planeWidth;
	var elementHeight = elementWidth * aspectRatio;
	element.style.width  = elementWidth + "px";
	element.style.height = elementHeight + "px";

	var cssObject = new THREE.CSS3DObject( element );
	
	cssObject.position = plane.position;
	
	var percentBorder = 0.05;
	cssObject.scale.x /= (1 + percentBorder) * (elementWidth / planeWidth);
	cssObject.scale.y /= (1 + percentBorder) * (elementWidth / planeWidth);
	cssObject.rotation.y -= (angle - Math.PI/2);
	
	cssScene.add(cssObject);
	
	obj["eleObj"] = cssObject;
	htmlObjAry[$_element.attr("listNum")] = obj;
}



//애니메이션이 실행되는 부분 (매 프레임 갱신되어야할 부분)
function render() {
	requestAnimationFrame(render);

	var dt = clock.getDelta();
	engine.update(dt * 0.5);
	
	if(controls != null){
		controls.update();
		
	}
	
	//전역변수에 따라서 로테이팅 렌더링 여부를 결정
	if(isRotating){
		
		for(var i = 0; i < rotatingObjAry.length ;i++){
			var rotatingObj = rotatingObjAry[i].obj;
			
			rotatingObjAry[i].angle += dt * Math.PI / rotatingObjAry[i].time;
			rotatingObj.position.x = getXPos(rotatingObjAry[i].r, rotatingObjAry[i].angle, rotatingObjAry[i].target.position.x);
			rotatingObj.position.z = getZPos(rotatingObjAry[i].r, rotatingObjAry[i].angle, rotatingObjAry[i].target.position.z);
		}
		
		for(var key in htmlObjAry){
			
			htmlObjAry[key].angle += dt * Math.PI / htmlObjAry[key].time;
			htmlObjAry[key].obj.position.x = getXPos(htmlObjAry[key].r, htmlObjAry[key].angle, htmlObjAry[key].target.position.x);
			htmlObjAry[key].obj.position.z = getZPos(htmlObjAry[key].r, htmlObjAry[key].angle, htmlObjAry[key].target.position.z);
			htmlObjAry[key].eleObj.rotation.y -= dt * Math.PI/ htmlObjAry[key].time;
		}
		
		mSphere.rotation.y += 0.005;
		fSphere.rotation.y += 0.005;
	}
	TWEEN.update();
	rendererCSS.render(cssScene, camera);
	renderer.render(scene, camera);
}



//반지름과, 중심각, 원점 x좌표를 통해 x좌표를 구하는 삼각함수.
function getXPos(r, angle, Ox) {
	return r * Math.cos(angle) + Ox;
}



//반지름과, 중심각, 원점 z좌표를 통해 z좌표를 구하는 삼각함수.
function getZPos(r, angle, Oz) {
	return r * Math.sin(angle) + Oz;
}



//파티클 엔진 초기화
function initParticleEngine(){
	engine.destroy();
	engine = new ParticleEngine();
    engine.setValues(engineValues);
    engine.initialize();
}



//이벤트 바인딩
function bindEvent(){
	$("#mousePanel")[0].addEventListener( 'mousewheel', function(event){
		  event.preventDefault();
		  
		  var evt = document.createEvent("MouseEvents");
		  evt.initEvent('mousewheel', true, true);
		  evt.wheelDelta = event.wheelDelta;
		  controls.domElement.dispatchEvent(evt);
		}, false );
	
	$("#mousePanel")[0].addEventListener( 'mousedown', function(event){
		 event.preventDefault();
		 
		 var evt = new CustomEvent('mousedown');
		 evt.button = event.button;
		 evt.clientX = event.clientX;
		 evt.clientY = event.clientY;
		  controls.domElement.dispatchEvent(evt);
		}, false );   
	
	$("svg").click(function(){
		    if(isRotating){
		    	$("#rotatingToggle").click();
		    }			
		
			var container = $(this).parent(); 
		    var listBttn = container.children(".listBttn");
		    var listNum = container.attr("listNum");
		    var listName = container.attr("listName");
		    var targetObj;
		    
		    if(container.attr("class") == "mList"){
		    	targetObj = mSphere;
		    }
		    else{
		    	targetObj = fSphere;
		    }
		    
		    //특정 리스트에 클릭이 들어왔을때.
			if(container.attr("isSelected") == 0){
				
				$(".mList").attr("isSelected", 0);
				container.attr("isSelected", 1);
				
				new TWEEN.Tween(camera.position).to({
					x : getXPos(70,  htmlObjAry[listNum].angle, htmlObjAry[listNum].obj.position.x),
					y : targetObj.position.y + (htmlObjAry[listNum].obj.position.y - targetObj.position.y) * 1.5,
					z : getZPos(70,  htmlObjAry[listNum].angle, htmlObjAry[listNum].obj.position.z)
				}, 1000).easing(TWEEN.Easing.Cubic.InOut).start(); 
					
		 		new TWEEN.Tween(controls.center).to({
					x : targetObj.position.x,
					y : targetObj.position.y,
					z : targetObj.position.z
				}, 1000).easing(TWEEN.Easing.Cubic.InOut)
				.start();
		 		
		 		$(".listBttn").not(listBttn).fadeOut(1000);
		 		
				listBttn.fadeIn(1000);
				
				listBttn.children(".play").on("click",function(){
			   		$.ajax({
							url : "validateThirtySong.do",
							data : {
								listNum : listNum
							},
							type : "GET",
							dataType : "text",
							success : function(responseData){
								var response = eval("("+responseData+")");
								
								if(response.result == "success"){
									var newWindow = window.open("enterList.do?listNum="+ listNum);
									newWindow.resizeTo(screen.width, screen.height);
									newWindow.moveTo(0, 0);
								}else{
									if(container.attr("class") == "mList"){
										alert("해당 리스트에 곡이 하나도 들어있지 않습니다. \n 곡 수정 페이지로 이동합니다.")
										location.href = "modifyList.do?listNum="+listNum+"&listName="+listName;							
									}
									else{
										alert("해당 리스트에 곡이 하나도 들어있지 않습니다.")
									}
		
								}				
							}//end of success
					});//end of $.ajax 	   
			   })//end of play on click
			   
			   listBttn.children(".more").on("click",function(){
				   
				   if(container.attr("class") == "mList"){
					   location.href = "detailListInfo.do?collectionNum=" + listNum + "&amiFollowing=2";
				   }
				   
				   else{
					   location.href = "detailListInfo.do?collectionNum=" + listNum + "&amiFollowing=1";;
				   }
			   })
			   
			   if(targetObj == mSphere){
				   listBttn.children(".modify").on("click",function(){
					   location.href = "modifyList.do?listNum="+listNum+"&listName="+listName;
				   })//end of modify on click
			   }//end of if				   
			}
		
			
			//특정 리스트가 다시 클릭 되었을 때 -> 포커스를 뺀다.
			else{
				if(!isRotating){
					$("#rotatingToggle").click();
				}
				container.attr("isSelected", 0);
				
				if(targetObj == mSphere){
					new TWEEN.Tween(camera.position).to({
						x : cameraPosition.mSphere.x,
						y : cameraPosition.mSphere.y,
						z : cameraPosition.mSphere.z
					}, 1000).easing(TWEEN.Easing.Cubic.InOut).start();
				}			
				else{
					new TWEEN.Tween(camera.position).to({
						x : cameraPosition.fSphere.x,
						y : cameraPosition.fSphere.y,
						z : cameraPosition.fSphere.z
					}, 1000).easing(TWEEN.Easing.Cubic.InOut).start();	
				}
				
				new TWEEN.Tween(controls.center).to({
					x : targetObj.position.x,
					y : targetObj.position.y,
					z : targetObj.position.z
				}, 1000).easing(TWEEN.Easing.Cubic.InOut).start();
				
				container.children(".listBttn").fadeOut(1000);
				container.children(".listBttn").children(".play").off("click");
				container.children(".listBttn").children(".more").off("click");
				
				if(targetObj == mSphere){
					container.children(".listBttn").children(".modify").off("click");
				}			
			}		
		});
	   
	   
	   $(".mList").hover(function(){
		   $(this).attr("isMouseOver", 1)
		   $(".mList:not([isMouseOver=1])").css("opacity", 0.2);
	   }, function(){
		   $(this).attr("isMouseOver", 0);
		   $(".mList").css("opacity", 1);
	   })	
	   
	   
	   $(".fList").hover(function(){
		   $(this).attr("isMouseOver", 1)
		   
		   $(".fList:not([isMouseOver=1])").css("opacity", 0.2);
	   }, function(){
		   $(this).attr("isMouseOver", 0)
			
		   $(".fList").css("opacity", 1);
	   });
	  
	   
	   $("#listToggle").click(function(){

					
			if(isCamLookAtMList){
				isCamLookAtMList = false;
				
				$(this).css("background", "#e64c65")
				.css("text-align", "left")
				.text("◀    FOLLOW");	
				
				new TWEEN.Tween(camera.position).to({
					x : cameraPosition.fSphere.x,
					y : cameraPosition.fSphere.y,
					z : cameraPosition.fSphere.z
				}, 2000).easing(TWEEN.Easing.Cubic.InOut).start();
				
			    new TWEEN.Tween(controls.center).to({
					x : fSphere.position.x,
					y : fSphere.position.y,
					z : fSphere.position.z
				}, 2000).easing(TWEEN.Easing.Cubic.InOut).start();			    
			}
			
			else{
				isCamLookAtMList = true;
				
				$(this).css("background", "#00cd99")
				.css("text-align", "right")
				.html("MY &nbsp;&nbsp;&nbsp;▶");
				
				new TWEEN.Tween(camera.position).to({
					x : cameraPosition.mSphere.x,
					y : cameraPosition.mSphere.y,
					z : cameraPosition.mSphere.z
				}, 2000).easing(TWEEN.Easing.Cubic.InOut).start();
	 			
				new TWEEN.Tween(controls.center).to({
					x : mSphere.position.x,
					y : mSphere.position.y,
					z : mSphere.position.z
				}, 2000).easing(TWEEN.Easing.Cubic.InOut).start();			
			}
			
			$(".mList").attr("isSelected", 0);
			$(".fList").attr("isSelected", 0);
			
			if(!isRotating){
				$("#rotatingToggle").click();
			}			
			
			$(".listBttn").fadeOut(1000);
	   });
	   
	   
	   $("#rotatingToggle").click(function(){
	 	   if(isRotating){
			   isRotating = false;
			   $(this).css("background-image", "url(static/img/rotatex.png)");
			   controls.autoRotate = false;
		   }
		   
		   else{
			   isRotating = true;
			   $(this).css("background-image", "url(static/img/rotate.png)");
			   controls.autoRotate = true;
		   }
	   });
	   
	   
	   //JQUERY - UI : 모달 폼 만들기
	   $( "#dialog-form" ).dialog({
		      autoOpen: false,
		      height: 135,
		      width: 300,
		      modal: true,
		      buttons: {
		        "Create": function() {
		        	var inputValue = $('#inputValue').val();
		    		var stringByteLength = ( function (s,b,i,c) {
		    			for(b=i=0;c=s.charCodeAt(i++);b+=c>>11?3:c>>7?2:1);
		    			return b;
		    			}) (inputValue);
		    		
		    		if (stringByteLength < 28 && stringByteLength != 0) {
		    			
		    			location.href = "createCollectionService.do?inputValue="+inputValue;
		    		
		    		} else if (stringByteLength == 0){
		    		
		    			alert("리스트 이름이 빈칸입니다")
		    			
		    		} else {
		    			
		    			alert("리스트는 한글 9자, 영문 27자 미만으로 작성해 주세요")
		    			
		    		}
		        },		          
		        Cancel: function() {
		          $( this ).dialog( "close" );
		        }
		      },
		      close: function() {
		    	  $( this ).dialog( "close" );
		      }
	    });
	   
	   $("#createList").click(function(){
		   $("#dialog-form").dialog("open");
	   })
}