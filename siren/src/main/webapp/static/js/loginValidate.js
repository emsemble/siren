var LoginValidate = $Class({
	_oElement : null,
	_oEvent : null,
	_bIsLogin : false,
	
	$init : function() {
		$Fn(this._initialize, this).attach(window, 'load');
		$Fn(this._destroy, this).attach(window, 'unload');
	},
	
	_initialize : function() {
		this._oElement = {};
		this._oEvent = {};
		
		this._setElement();
		this._setEvent();
	},
	
	_destroy : function() {
		
	},
	
	_setElement : function() {
		this._oElement['headerArea'] = $('header');
		this._oElement['contentArea'] = $('content');
		this._oElement['footerArea'] = $('footer');
		this._oElement['submitLogin'] = $('submitLogin');
	},

	_setEvent : function() {
		this._oEvent['clickLoginButton'] = $Fn(this._onClickLoginButton, this).attach(this._oElement['submitLogin'], 'click');
	},
	
	_onClickLoginButton : function(oEvent) {
		var oAjaxParam = {
			"userId" : $('userId').value,
			"userPw" : $('userPw').value
		};
		
		var oAjaxOption = {
			timeout : 5,
			method : 'post',
			onload : function(oResponse) {
				var oJson = oResponse.json();
				if (oJson) {
					if (oJson.isSuccess) {
						location.href = "/seiren/myList.do";
					} else {
						var sErrorMsg = oJson.errorMsg || "로그인에 실패하였습니다.";
						$Element('text1').html(sErrorMsg);
					}
				} 
			},
			ontimeout : function(oResponse) {
				alert("로그인에 실패하였습니다.");
			},
			onerror : function(oResponse) {
				alert("로그인에 실패하였습니다.");
			}
		};
		
		var sUrl = "/seiren/loginValidate.do";
		var oAjax = new $Ajax(sUrl, oAjaxOption);
		oAjax.request(oAjaxParam);
		
		oEvent.stop();
	}
});