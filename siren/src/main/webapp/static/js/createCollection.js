/**
 *  1. 파일 이름 : createCollection.js
 *  
 *  2. 작성자 : 김시온
 *
 *  3. 목적 : 콜렉션을 만들때 곡 추가, 삭제 등  기능
 *
 *  4. 이력 : 
 *  	2014. 05. 26 : 최초생성
 *  			  ~ 27 : list에 곡 추가, 삭제 , fiilter, 저장 기능 구현
 *
 */

//load시 해당리스트가 보유하고 있는 노래들 가져오는 메소드
function _onloadMySongs() {
	var listNum = $("#listNum").val();
	var savedSongInfo;
	
	$.ajax({
	   data : { listNum : listNum},
		url : "saveSongList.do",
		type : "post",
		dataType : "json",
		success : function(responseData){
			if(responseData.result == "success") {
				var jsonSavedSong = eval(responseData.saveSongList);
				
				for (var i = 0; i < jsonSavedSong.length; i++) {
				
					savedSongInfo = "<div class='"+jsonSavedSong[i].num+" addedSongContent' >"
												+ "<img src='"+getImgSrc(jsonSavedSong[i].albumId)+"' style='padding-left:2px;position:absolute;width:40px;height:40px;'>"
												+ "<div class='songInfo' style='font-size:13px;font-weight: bold;'>"+jsonSavedSong[i].songName+"</div>"
												+ "<div class='songInfo' style='font-size:10px;padding-top: 2px;'>"+jsonSavedSong[i].artistName+"</div>"
												+ "</div>";
					$(".savedWrapper").append(savedSongInfo);
				}
				
				$(".addedSongContent").hover(function(){
					$(this).append("<img class='xBox' onclick='_clickDeleteSong(this)' src='static/img/xbox.png'>");
				}, function() {
					$(this).children(".xBox").remove();
				});//추가한 노래 hover 시 x표시 나옴.
				
			}
			
			else {
				alert("곡 불러오기에 실패했습니다");	
			}
			
		}//end of success
	})	
	
	.fail(function(){
		alert("곡 불러오기에 실패했습니다");	
	})
	.error(function(){
		alert("곡 불러오기에 실패했습니다");
	});
}



//랜덤으로 장르뿌려주는 메소드
function _insertSongByGenre (songList) {
		
	var jsonString = songList;
	var arraySongList = eval(jsonString);
	var index = 0;
	
	for (var i = 0; i < arraySongList.length; i++) {
		
		
		wrapper = "<div class='songList' isSelected='0'>"
							+ "<div  isSelected='0'  class='genreDIV' style=''>"
							+ "<div  isSelected='0' class='triangle'></div>"
							+ "<span  isSelected='0' class='genreTitle'>"+arraySongList[i][0].genre+"</span>"
							+ (arraySongList[i].length > 5 ? "<img src='static/img/rightBtn.png' isSelected='0' class='nextBtn btn' >" : "")
							+ (arraySongList[i].length > 5 ? "<img src='static/img/leftBtn.png' isSelected='0' class='preBtn btn' >" : "")
							+ "</div>"
							+ "<div isSelected='0' class='songWrapper index_1'>" 
							+ "</div>"
							+ "<hr  class='hr'></div> ";
		
		$(".songArea").append(wrapper);

		for(var j = 0; j < arraySongList[i].length; j++) {
			temp = "<div  isSelected='0' class='songContent _"+(arraySongList[i])[j].fileName+"' >"
						+ "<div class='album_border "+(arraySongList[i])[j].num+"' style='display:table;'>"
						+ "<img  isSelected='1' class='albumImg' src = '"+getImgSrc((arraySongList[i])[j].albumId)+"' ></img></div>"
						+ "<div isSelected='0' class='albumInfo'>" 
						+ "<span class='artistInfo'  isSelected='0'>"+arraySongList[i][j].songName+"</span><br>"
						+ "<span class='titleInfo'  isSelected='0'>"+arraySongList[i][j].artistName+"</span>"
						+ "</div>"
						+ "</div>";
			$($(".songWrapper")[i]).append(temp);
		}
	}//end of for () : songList	
	
	_hoverAlbumBorder ();

}//end of insertBy


//hover 됫을 때 overlay 되면서 곡 추가, 재생, 상세보기 버튼 보이는 메소드
function _hoverAlbumBorder () {
	$(".album_border").hover(function(){
		
		var _streamServer = "http://seiren.gonetis.com:8081/";
		var songClass = $(this).parent().attr("class");
		var splitClass = songClass.split("_");
		var _fileName = splitClass[1];
		var sSrc = _streamServer + _fileName;
		
		var sImageUrl = "static/img/play-Icon.png";
		var sId = "play";
		
		if (sSrc == $("#player").attr("src") && $("#player")[0].paused == false) {
			sImageUrl = "static/img/pause.png";
			sId = "pause";
		}
		
		$(this).prepend("<div class='overlayRect'></div>");
		$(this).prepend("<img class='overlayAdd' id='more' src='static/img/more.png'></img>");
		$(this).prepend("<img class='overlayAdd' onclick='_addSong(this)' src='static/img/Add.png' id='plus'></img>");
		$(this).prepend("<img class='overlayAdd' src='"+sImageUrl+"' onclick='_playSong(this)' id='"+sId+"'></img>");
	}, function(){
		$(this).children(".overlayAdd").remove();
		$(this).children(".overlayRect").remove();
	});
}


//해당 장르에 속해있는 곡들을 slide로 보여주기 위한 버튼 - pre버튼 클릭시 발생하는 이벤트
function _showPreSong(btn) {
	var sClassName = $(btn).parent().siblings(".songWrapper")[0].className;
	var nIndex = parseInt(sClassName.split("_")[1]);
	
	if( nIndex != 1 ) {
		$(btn).parent().siblings(".songWrapper").removeClass("index_" + nIndex);
		nIndex--;
		$(btn).parent().siblings(".songWrapper").css("transform","translateX("+ ((nIndex - 1) * -760) +"px)");
		$(btn).parent().siblings(".songWrapper").addClass("index_" + nIndex);
	}
}


//해당 장르에 속해있는 곡들을 slide로 보여주기 위한 버튼 - next버튼 클릭시 발생하는 이벤트
function _showNextSong(btn) {
	var sClassName = $(btn).parent().siblings(".songWrapper")[0].className;
	var nIndex = parseInt(sClassName.split("_")[1]);
	var nChild = $(btn).parent().siblings(".songWrapper").children().length;
	nChild = Math.ceil(nChild/5);

	if (nIndex != (nChild)) {
	
		$(btn).parent().siblings(".songWrapper").css("transform","translateX("+ (nIndex * -760) +"px)");
		$(btn).parent().siblings(".songWrapper").removeClass("index_" + nIndex);
		nIndex++;
		$(btn).parent().siblings(".songWrapper").addClass("index_" + nIndex);
	}
}

//이벤트 리스너 붙이는 메소드
function _bindEventListener() {
	
	 $('.preBtn').click(function() {
		 var btn = this;
		 _showPreSong(btn);
	 });//앨범 slide 왼쪽으로 넘어가는 버튼

	 
	$('.nextBtn').click(function() {
		var btn = this;
		_showNextSong(btn);
	});// 앨범 slide 오른쪽으로 넘어가는 버튼
	
	$(".box").click (function() {
		var searchBox = this;
		_clickSearchBox(searchBox);
	});//serarch box 선택시 발생하는 이벤트
	
	$('.option').click(function () {
		var genre = this;
		_clickShowGenre(genre);
	})//genre 선택시 해당  genre에 해당하는 노래들 보여주는 메소드

}

//해당 나의 리스트에 선택한 곡 추가하가는 메소드
function _addSong(_target) {
	
	var imgClass = $(_target).parent().attr("class");
	var splitClass = imgClass.split(" ");
	var songNum = parseInt(splitClass[1]);
	var listNum = $("#listNum").val();
	
	$.ajax({
	   data : {songNum : songNum,
		   		  listNum : listNum
		},
		url : "addSong.do",
		type : "post",
		dataType : "json",
		success : function(responseData){
			if(responseData.result == "success") {
				
				$(_target).attr("src", "static/img/check.png");
				
				setTimeout(function () {
					$(_target).attr("src", "static/img/Add.png");
				}, 1000); //노래추가시 선택 되었다고 표시하는 img show 
				
				//added된 song 정보를 fixed된 이미지에 보여줌
				var jsonAddedSong = eval(responseData.addedSong);
				
				var addedSongInfo = "<div class='"+jsonAddedSong[0].num+" addedSongContent' >"
											+ "<img src='"+getImgSrc(jsonAddedSong[0].albumId)+"' style='padding-left:2px;position:absolute;width:40px;height:40px;'>"
											+ "<div class='songInfo' style='font-size:13px;font-weight: bold;'>"+jsonAddedSong[0].songName+"</div>"
											+ "<div class='songInfo' style='font-size:10px;padding-top: 2px;'>"+jsonAddedSong[0].artistName+"</div>"
											+ "</div>";
				
				$(".savedWrapper").append(addedSongInfo);
				
				
				$(".addedSongContent").hover(function(){
					$(this).append("<img class='xBox' onclick='_clickDeleteSong(this)' src='static/img/xbox.png'>");
				}, function() {
					$(this).children(".xBox").remove();
				});//추가한 노래 hover 시 x표시 나옴.
				
				
			} else if ( responseData.result == "fail" ) {
				alert("이미 추가하신 곡 입니다.");
			}
			
		}//end of success
	})	
	
	.fail(function(){
		alert("곡 추가에 실패했습니다");	
	})
	.error(function(){
		alert("곡 추가에 실패했습니다");
	});
}  


//엑박 클릭시 노래 삭제하는 메소드
function _clickDeleteSong(_xbox) {
	
	var imgClass = $(_xbox).parent().attr("class");
	var splitClass = imgClass.split(" ");
	var songNum = parseInt(splitClass[0]);
	var listNum = $("#listNum").val();
	
	$.ajax({
		   data : {songNum : songNum,
			   		  listNum : listNum
			},
			url : "removeSong.do",
			type : "post",
			dataType : "json",
			success : function(responseData){
				if (responseData.result == "success") {
					console.log($(_xbox));
					console.log($(_xbox).parent());
					console.log($(_xbox).parent(".addedSongContent"));
					$(_xbox).parent().remove();
				} 
				else {
					alert("곡 삭제에 실패했습니다");	
				}
			}//end of success
		})	
		
		.fail(function(){
			alert("곡 삭제에 실패했습니다");	
		})
		.error(function(){
			alert("곡 삭제에 실패했습니다");
		});
	
}


//장르별로 보여주는 메소드
function _clickShowGenre(_genre) {
	
	var genre = $(_genre).children(".genre").html();
	
	$("li").css("background", "transparent");

	$(_genre).css("background", "#e64c65");
	
	$.ajax({
	   data : {genre : genre},
		url : "searchByGenre.do",
		type : "get",
		dataType : "json",
		success : function(responseData){
			
			if (responseData.isSuccess) {
				var result = eval(responseData.songList);
				var sResultList;
				
				var temp = "<div class='temp' style='top:110px;left:33px;width:799px;height:1000px;max-width:799px;max-height:1000px;margin-right:20px;position:absolute;'></div>";
				
				$(".right_content").html(temp);
				
				$(".right_content").prepend("<div style='left:33px;position:absolute;top:28px;'>" 
														+ "<div style='top:24px;' isSelected='0' class='triangle'></div>" 
														+ "<h2 style='margin-left:14px;font-size: 19px;padding-top:4px;'>"+genre+"</h2></div>");
				
				for ( var i = 0; i < result.length; i++) {
					sResultList  = "<div isSelected='0' class='songContent _"+result[i].fileName+"'>"
										+ "<div class='album_border "+result[i].num+"'>"
										+ "<img  isSelected='1' class='albumImg "+result[i].num+"' src = '"+getImgSrc(result[i].albumId)+"' ></img></div>"
										+ "<div isSelected='0' class='albumInfo'>" 
										+ "<span class='artistInfo'  isSelected='0'>"+result[i].songName+"</span><br>"
										+ "<span class='titleInfo'  isSelected='0'>"+result[i].artistName+"</span>"
										+ "</div>"
										+ "</div>";	
					
					if ( i%5 == 0) {
						$(".temp").append("<br>");
					}
					
					$(".temp").append(sResultList);
				}
				
				_hoverAlbumBorder (); 
			}
			
			else {
				alert("곡 불러오기에 실패했습니다");	
			}
		}//end of success
	})	
	.fail(function(){
		alert("곡 불러오기에 실패했습니다");	
	})
	.error(function(){
		alert("곡 불러오기에 실패했습니다");
	});
	
}

//play 메소드
function _playSong(song) {
	
	var _streamServer = "http://seiren.gonetis.com:8081/";
	
	var songClass = $(song).parent().parent().attr("class");
	var splitClass = songClass.split("_");
	var _fileName = splitClass[1];
	
	if ($("#player")[0].paused == false) {
		if($("#player").attr("src") == _streamServer + _fileName ){
			$(song).attr("src", "static/img/play-Icon.png");
			$(song).attr("id", "play");
			$("#player")[0].pause();
		} 
		
		else {
			$(song).attr("src", "static/img/pause.png");
			$(song).attr("id", "pause");
			$("#player").attr("src", _streamServer + _fileName);
		}
	} 
	
	else {
		
		if($("#player").attr("src") == _streamServer + _fileName ){
			$("#player")[0].play();
		} 
		
		else {
			$("#player").attr("src", _streamServer + _fileName);
		}
		
		$(song).attr("src", "static/img/pause.png");
		$(song).attr("id", "pause");
	}
}