/**
 * 1. 파일이름 : comment.js
 * 2. 작성자 : 김시온
 * 3. 목적 : player.jsp에서 댓글에 관련된 스크립트 
 * 4. 이력 : 
 * 		2014.05.02: 최초작성(댓글 로드, 삭제, 입력 기능 구현)
 * 			- 댓글로드 : 댓글화면버튼 클릭시 해당리스트에 등록된 모든 댓글을 ajax로 가져옴
 * 							로그인한 유저와 댓글 작성자가 같으면 수정, 삭제 기능 부여
 * 							같지않다면 readonly
 * 			- 삭제 : commentNum으로 삭제
 * 			- 입력 :
 
 * 		2014.05.03: player.jsp에서 분리, 수정기능 구현
 * 		
 */


function _initializeCmt() {

	$("#showCmt").click(function () {
		_clickCommentButton();
		return false;
	});
	
	
	$("#submitComment").click(function () {
		_onClickSubmitCommentBtn();
		return false;
	});

}


//comment 버튼 눌렀을 때 slide 방식으로 comment창이 나오는 메소드
function _clickCommentButton(){
	var nLeftOfCommentArea = jindo.$Document().clientSize().width - 500;
	jindo.$Element('toggle').css('left', nLeftOfCommentArea);
	
	$("#toggle").toggle( "slide", {direction : 'down'});
	
	var collectionNum = $("#collectionNum").text();
	
	var oAjaxParam = {
		'collectionNum' : collectionNum
	};
		
	var oAjaxOption = {
		timeout : 5,
		method : 'post',
		onload : function(oResponse) {//ajax 응답
		
			var oJson = oResponse.json();
			
			if (oJson) {
				if ( oJson.isSuccess ) {
					var commentVO = oJson.commentVO; 
					var jsonCommentVO = eval(commentVO);
					var splitWriter;
					var splitedWriter;
					
					$('.contentsArea').text(""); //댓글 로드시키기전에 초기화 시킴
					for ( var i = 0; i < jsonCommentVO.length; i++ ) {
						splitWriter = jsonCommentVO[i].writer.split('@'); 
						splitedWriter = splitWriter[0];
					    
						var sComment = "<div id='" + jsonCommentVO[i].num + "' class='commentWrapper'>"
												+ (jsonCommentVO[i].isMyComment ? "<div class='deleteComment' id='delete_"+jsonCommentVO[i].num+" ' style='float:right'>X</div>" : "")
												+ (jsonCommentVO[i].isMyComment ? "<div class='modifyComment' id='modify_"+jsonCommentVO[i].num+"' style='float:right'>M</div>" : "")
												+ "<div class='commentIdArea'>"
												+ "<span class='commentId'>" + splitedWriter + "</span>"
												+ "<span class='commentWriteTime'>" + jsonCommentVO[i].writeTime + "</span>"
												+ "</div>"
												+ "<div id='contents_"+jsonCommentVO[i].num+"' class='commentContentArea'>"
												+ "<span>" + jsonCommentVO[i].contents + "</span>"
												+ "</div>"
												+ "</div>";
						
						$(".contentsArea").append(sComment);
 					}
					
					
					//삭제권한을 부여받은 이용자가 삭제버튼 눌를 시 실행
					$('.deleteComment').click (function () {
						_deleteComment(this);
					});
					
					
					//수정권한을 부여받은 이용자가 삭제버튼 눌를 시 실행
					$('.modifyComment').click(function () {
						_modifyComment(this);
					});
					
					
				} else {
					
					alert("댓글 로드에 실패하였습니다.");
					
				}
			} 
		},//oAjaxOption 끝
		
		ontimeout : function(oResponse) {
		},
		
		onerror : function(oResponse) {
		}
	};
		
	var sUrl =  '/seiren/selectAllComment.do';
	var oAjax = new jindo.$Ajax(sUrl, oAjaxOption);
	
	oAjax.request(oAjaxParam);
		
}


//TEXTAREA에 contetns입력후 댓글버튼 누르면 ajax로 댓글을 DB에 저장하는 메소드  
function _onClickSubmitCommentBtn () {
	
	var contents = jindo.$("contents").value; 
	var collectionNum = $("#collectionNum").text();

	if(contents == "" ||  null) {
		return;
	} 
	
	var oAjaxParam = {
		'contents' : contents,
		'collectionNum' : collectionNum
	};
		
	var oAjaxOption = {
		timeout : 5,
		method : 'post',
		onload : function(oResponse) {
			var oJson = oResponse.json();
			
			if (oJson) {
				if ( oJson.isSuccess ) {
					var commentVO = oJson.commentVO;
					var jsonCommentVO = eval(commentVO);
					var splitWriter = jsonCommentVO[0].writer.split('@');
					var writer = splitWriter[0];
					
					var sComment = "<div id='" + jsonCommentVO[0].num + "' class='commentWrapper'>"
											+ "<div class='deleteComment' id='delete_"+jsonCommentVO[0].num+"' style='float:right'>X</div>"
											+ "<div class='modifyComment' id='modify_"+jsonCommentVO[0].num+"' style='float:right'>M</div>"
											+ "<div class='commentIdArea'>"
											+ "<span class='commentId'>" + writer + "</span>"
											+ "<span class='commentWriteTime'>" + jsonCommentVO[0].writeTime + "</span>"
											+ "</div>"
											+ "<div id='contents_"+jsonCommentVO[0].num+"' class='commentContentArea'>"
											+ "<span>" + jsonCommentVO[0].contents + "</span>"
											+ "</div>"
											+ "</div>";
					
					$(".contentsArea").append(sComment);
					
					jindo.$("contents").value = "";
				} else {
					
					alert("댓글 등록에 실패하였습니다.");
					
				}
			} 
		},
		
		ontimeout : function(oResponse) {
		},
		
		onerror : function(oResponse) {
		}
	};//oAjaxOption 끝
		
	var sUrl =  '/seiren/saveComment.do';
	var oAjax = new jindo.$Ajax(sUrl, oAjaxOption);
	
	oAjax.request(oAjaxParam);
		
}



//댓글 삭제 메소드
function _deleteComment(_target) {
	
	var sId = $(_target).attr('id');
	var nCommentId = sId.split('_')[1]*1;
	
	var oAjaxParam = {
		'commentNum' : nCommentId
	};
		

	var oAjaxOption = {
		timeout : 5,
		method : 'post',
		onload : function(oResponse) {
			var oJson = oResponse.json();
			
			if (oJson) {
				if ( oJson.isSuccess ) {

					$(jindo.$(nCommentId)).remove();
				
				} else {
					alert("댓글 삭제에 실패하였습니다.");
					
				}
			} 
		},
		
		ontimeout : function(oResponse) {
		},
		
		onerror : function(oResponse) {
		}
	};//oAjaxOption 끝 
		
	var sUrl =  '/seiren/deleteComment.do';
	var oAjax = new jindo.$Ajax(sUrl, oAjaxOption);
	
	oAjax.request(oAjaxParam);

}


//수정한 contetns반환 하는 메소드
function modifySubmitComment (oEvent) {
	
	 $(oEvent).siblings().eq(0).val();

}


//댓글 수정하는 메소드
function _modifyComment (_target) {
		
	var sId = $(_target).attr('id');
	nCommentId = sId.split('_')[1]*1;
	
	var sModifyArea = jindo.$Template('tpl_modifyContents').process();
	var sContentsId = "#contents_" + nCommentId;
	$(sContentsId).html(sModifyArea);
	
	$(".modifySubmitComment").click(function () {
		sContents = _modifySubmitComment(this);
		
		if(sContents == "" ||  null) {
			return ;
		} 
		
		var oAjaxParam = {
			'commentNum' : nCommentId,
			'contents' : sContents
		};
		
		var oAjaxOption = {
			timeout : 5,
			method : 'post',
			onload : function(oResponse) {
				var oJson = oResponse.json();
				
				if (oJson) {
					if ( oJson.isSuccess ) {
						var sModifyResultArea = jindo.$Template('tpl_modifyContentsResult').process({
							contents : sContents 
						});
						var sContentsId = "#contents_" + nCommentId;
						$(sContentsId).html(sModifyResultArea);
					} 
					else {
						alert("댓글 수정에 실패하였습니다.");
						
					}
				} 
			},
			
			ontimeout : function(oResponse) {
			},
			
			onerror : function(oResponse) {
			}
		};
			
		var sUrl =  '/seiren/updateComment.do';
		var oAjax = new jindo.$Ajax(sUrl, oAjaxOption);
		
		oAjax.request(oAjaxParam);

	});
		
}



function _modifySubmitComment (_target) {
	return $(_target).siblings().eq(0).val();
} 