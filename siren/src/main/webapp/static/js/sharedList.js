/**
 *  1. 파일 이름 : sharedList.js
 *  
 *  2. 작성자 : 김성철
 *
 *  3. 목적 : 공유 리스트 페이지에서 초기화, 재생, 팔로우 리스트로 등록 등을 담당.
 * 
 *  4. 이력 : 
 *  
 *  2014. 05. 14. : 중간 완성
 *  
 *  2014. 05. 29. : 필터, 검색기능 완성
 *  
 *  2014. 06. 02. : ajax이용 메소드 전반적 수정
 *  
 *  2014. 06. 05. : 공유된 리스트 아무것도 없을때 나타나는 초기화면 수정, 리스트 받아서 세팅해주는 메소드수정, 
 */

function _noLists(){
	
	alert("공유중인 리스트가 하나도 없습니다");
	
	$('#topListLabel').hide();
	$('#topList').remove();
	$('#sharedListLabel').hide();
	$('#sharedPublicList').remove();

	$('#filteredListLabel').show();
	$('#filteredListLabel').css("display", "block");
	$('#filtered#filteredListTriangle').show();
	$('#filteredList').attr("isSharedListsRemoved", 1);

	$("#filteredList").html("<div id = 'noFilterResult'>공유중인 리스트가 하나도 없습니다!</div>");

};

var SharedList = jindo.$Class({

	_oElement : null,
	_oEvent : null,
	
	
	
	$init : function(topList, sharedPublicList) {
		jindo.$Fn(this._initialize, this).attach(window, 'load');
		jindo.$Fn(this._setList(topList, sharedPublicList), this);
		jindo.$Fn(this._destroy, this).attach(window, 'unload');
	},
	
	
	
	_initialize : function() {
		this._oElement = {};
		this._oEvent = {};
		
		this._setElement();
		this._setEvent();
	},
	
	
	
	//초기화하면서 리스트 불러오기
	_setList : function(topList, sharedPublicList) {
		
		showListNum = 4;
		
		this._setListIntoHTML(topList, 0, topList.length, "topList");

		if (showListNum < sharedPublicList.length) {

			this._setListIntoHTML(sharedPublicList, 0, showListNum, "sharedPublicList");

			$('#topList .lists').append();

		} else {
			
			this._setListIntoHTML(sharedPublicList, 0, sharedPublicList.length, "sharedPublicList");
		}
	},
	
	
	
	_destroy : function(){

	},


	
	_setElement : function() {
		this._oElement['lists'] 	   = jindo.$('lists');
		this._oElement['moreButton']   = jindo.$('moreButton');
		this._oElement['topButton']	   = jindo.$('topButton');
		this._oElement['sideBar']	   = jindo.$('sideBar');
		this._oElement['searchString'] = jindo.$('searchString');
	},

	

	_setEvent : function() {
		this._oEvent['clickLists'] 		= jindo.$Fn(this._onClickLists, this).attach(this._oElement['lists'], 'click');
		this._oEvent['clickTopButton']	= jindo.$Fn(this._onClickTopButton, this).attach(this._oElement['topButton'], 'click');
		this._oEvent['clickSideBar']	= jindo.$Fn(this._onClickSideBar, this).attach(this._oElement['sideBar'], 'click');
		this._oEvent['focusSearchBox']	= jindo.$Fn(this._onFocusSearchBox, this).attach(this._oElement['searchString'], 'focus');
		this._oEvent['submitSearchBox'] = jindo.$Fn(this._submitSearchBox, this).attach(this._oElement['searchString'], 'keyup');
		this._oEvent['scrollEvent']		= jindo.$Fn(this._isScrollEnd, this).attach(window, 'scroll');
	},
	
	
	
	//jsonString 상태로 저장해놓은 리스트들을 HTML형식으로 바꿔서 해당하는 Div에 append 시켜준다.
	//파라미터 설명
	// list   : TOP4리스트인지, 전체 리스트 목록 데이터중 (jsonString 값) 가져온다
	// start  : 가지고 온 리스트의 몇번째 배열부터 시작할것인가
	// finish : 마지막 배열의 번호
	// flag   : TOP4리스트인지, 전체 리스트 목록, 필터링, 검색된 리스트인지 판단하는 String 값
	_setListIntoHTML : function(list, start, finish, flag) {
		
		for (var i = start; i < finish; i++) {
			var imgsrc = null;

			if(list[i].albumid != null){
				
				imgsrc = getImgSrc(list[i].albumid);
			}
			
			var div			   = "<div id ='"+ flag + i + "' class = lists>";
			var img			   = "<img id = img src ='"+ imgsrc + "' class = 'albumImg'></img>";
			var collNum		   = "<span class = collNum style='display:none'>" + list[i].num + "</span>";
			var amIfollowing   = "<span class = amiFollowing style='display:none'>" + list[i].amiFollowing + "</span>";
			var collName 	   = "<div class = 'elementsDiv'><div style='display: table;height:23px;'><span class = collName>" + list[i].name + "</span>";
			var followerImg	= "<img id ='followerImg' src='static/img/heart.png' class ='followerImg'></img>";
			var collFollower   = "<span class = collFollower>" + list[i].totFollower + "</span></div>";
			var collDescript   = "<span class = collDescript>" + list[i].description + "</span></div>";
			var divEnd 		   = "</div>";
			
			if(flag == "topList") {
				
				//var rankElements = "<span id ='" + (i+1) + "'class='rank'>#"+(i+1)+"</span><img class='ribbon' src='static/img/ribbon-white.png'></img>";
				
				jindo.$Element(flag).appendHTML(div + img + collNum + amIfollowing + collName + followerImg + collFollower + collDescript  + divEnd);

			} else {
				
				jindo.$Element(flag).appendHTML(div + img + collNum + amIfollowing + collName + followerImg + collFollower + collDescript  + divEnd);
			}

			jindo.$Fn(this._isHoverImage, this).attach(jindo.$$('#'+flag+i+' .albumImg'), 'mouseover');
		}
	},
	
	
	
	//현재 내가 팔로우 하고 있는 리스트인지, 아닌지를 확인하여 팔로우 버튼을 나타나게할것인가 / 언팔로우 버튼을 나타나게할것인가 판단.
	_setFollowButton : function(num, flag, list) {
		
		var followingTest = null;
		
		if(flag == "topList"){
			followingTest = topList[num].amiFollowing;
		}else if(flag == "sharedPublicList"){
			followingTest = sharedPublicList[num].amiFollowing;
		}else if(flag == "filteredList"){
			followingTest = list[num].amiFollowing;
		}
		
		if(followingTest == 1) {
			
			jindo.$Element(jindo.$$('#'+flag+' .unFollow')[num]).show();

		} else if(followingTest == 0) {
			
			jindo.$Element(jindo.$$('#'+flag+' .follow')[num]).show();

		}
	},
	
	
	
	// PublicLists Div를 클릭했을때 이벤트 발생한다, 4가지 이벤트가 있음
	// Follow, UnFollow 버튼 클릭시 팔로우와 언팔로우 가능
	// 재생하기 버튼 클릭시 해당 공유 리스트 재생하는 페이지로 이동
	// 상세정보 띄워주는 버튼 클릭시 해당 리스트의 상세정보 페이지로 이동
	_onClickLists : function (oEvent) {
		var el 	    = oEvent.element;
		var choice  = jindo.$Element(el).attr("class");
		var child   = jindo.$Element(el).parent().child();
		var collNum = 0;
		
		for (var i = 0; i < child.length; i++) {
			
			if (child[i].attr("class") == "collNum") {
				
				collNum = child[i].html(); // collNum에 내가 누른 리스트의 콜렉션 넘버를 저장함.
			} 
		}
		
		if(choice == "follow"){

			this._onClickFollowButton(collNum, el); // 팔로우버튼 클릭시
			
		} else if(choice == "unFollow") {

			this._onClickUnFollowButton(collNum, el); // 언팔로우 버튼 클릭시
			
		} else if(choice == "playSharedList") {
			
			this._onClickPlaySharedList(collNum); // 재생하기 버튼 클릭시
			
		} else if(choice == "moreInfoButton"){
			
			this._onClickMoreInfoButton(collNum); // 상세보기 버튼 클릭시
		}

	},
	
	
	
	_onClickSideBar : function (oEvent) {
		
		var element = oEvent.element;
		var choice  = jindo.$Element(element).attr("id");
		
		if(choice == "noTags"){
			
			this._settingSideBarResultList();
			this._onClickSubmitFilter(choice);
			
		}else if(choice == "submitFilter"){
			
			this._settingSideBarResultList();
			this._onClickSubmitFilter();
		
		}else if(choice == "myFollowingList"){
			
			this._settingSideBarResultList();
			this._myFollowingList();

		}else {
			
			return false;
		}
	},
	
	
	
	// 팔로우 버튼을 눌렀을 때 Ajax통신으로 업데이트 발생, 
	// 성공시 공유리스트 추가했다는 alert창과 함께 json으로 받아온 총 팔로워수로 업데이트
	// 실패시 이미 추가된 리스트라서 추가가 안되었는지, DB에 문제가 생겨 오류가 발생했는지 판단해 alert창 반환.
	_onClickFollowButton : function(collNum, el) {
		var self = this;
		var oAjaxParam = {
				"collectionNum" : collNum
			};
		var oAjaxOption = {
			timeout : 5,
			method  : 'post',
			onload  : function(oResponse) {
				
				var oJson = oResponse.json();
				if (oJson) {
					
					if (oJson.result =="success") {
						
						alert("공유 리스트에 추가했습니다.");
						$(el).siblings(".amiFollowing").html(1);
						self._updateFollowers(el, oJson.totFollower);

					} else if (oJson.result == "duplicated"){
						
						alert("이미 추가된 리스트입니다.");
						
					} else if (oJson.result == "isMyList"){
						
						alert("자신의 리스트는 팔로우 할수 없습니다.");
					
					} else {
						
						alert("공유 리스트 추가에 실패했습니다.");
					}
					
				} else {
					
					alert("공유 리스트 추가에 실패했습니다.");
				}
				
			},
			
			ontimeout : function(oResponse) {
				alert("공유 리스트 추가에 실패했습니다."); 
			},
			onerror : function(oResponse) {
				alert("공유 리스트 추가에 실패했습니다.");
			}
		};
			
		var sUrl = "addFollowList.do";
		var oAjax = new jindo.$Ajax(sUrl, oAjaxOption);
		oAjax.request(oAjaxParam);
	},
	
	
	
	// 언팔로우 버튼을 눌렀을 때 Ajax통신으로 업데이트 발생, 
	// 성공시 공유리스트 삭제했다는 alert창과 함께 json으로 받아온 총 팔로워수로 업데이트
	// 실패시 이미 추가된 리스트라서 삭제가가 안되었는지, DB에 문제가 생겨 오류가 발생했는지 판단해 alert창 반환.	
	_onClickUnFollowButton : function (collNum, el) {
		var self = this;
		var oAjaxParam = {
				"collectionNum" : collNum
			};
		
		var oAjaxOption = {
			timeout : 5,
			method  : 'post',
			onload  : function(oResponse) {
				
				var oJson = oResponse.json();
				
				if (oJson) {
					
					if (oJson.result =="success") {
						
						alert("공유 리스트 삭제에 성공했습니다.");
						$(el).siblings(".amiFollowing").html(0);
						self._updateFollowers(el, oJson.totFollower);
						
					} else if (oJson.result == "duplicated"){
						
						alert("이미 삭제된 리스트입니다.");
					
					} else {
						
						alert("공유 리스트 해제에 실패했습니다.");
					}
					
				} else {
					
					alert("공유 리스트 해제에 실패했습니다.");
				}
				
			},
			
			ontimeout : function(oResponse) {
				alert("공유 리스트 해제에 실패했습니다.");
			},
		
			onerror : function(oResponse) {
				alert("공유 리스트 해제에 실패했습니다.");
			}
			
		};
		
		var sUrl = "deleteFollowList.do";
		var oAjax = new jindo.$Ajax(sUrl, oAjaxOption);
		oAjax.request(oAjaxParam);
	},
	
	
	
	//공유된 리스트 재생하기 버튼을 눌렀을때 실행하는 메소드
	_onClickPlaySharedList : function (collNum) {
		var newWindow = window.open("enterList.do?listNum="+ collNum);
        newWindow.resizeTo(screen.width, screen.height);
        newWindow.moveTo(0, 0);
	},
	
	
	
	//자세히보기 버튼을 눌렀을때 실행하는 메소드
	_onClickMoreInfoButton : function (collNum, amiFollowing) {
		
		location.href = "detailListInfo.do?collectionNum=" + collNum + "&amiFollowing=" + amiFollowing;
	},
	
	
	
	//총 팔로워수를 업데이트 하기 위한 메소드, 현재 쓰이지않고있음.
	_updateFollowers : function (el, totFollower) {
		
		followUpdate = $(el).siblings(".elementsDiv");
		
		$(followUpdate).children(".collFollower").html(totFollower);
		
	},
	
	
	
	// 체크박스 제출버튼 누르면 Ajax통신하는 메소드로 필터를 전달, 원래있는 element들을 지우고 새롭게 띄운다
	_onClickSubmitFilter : function (noTags) {
		
		var emotionalTags = document.checkBoxForm.emotional;
		var songAttrTags = document.checkBoxForm.songAttr;
		var yearTags = document.checkBoxForm.year;
		
		var emotionalTagsValue = this._vaildateCheckBoxValues(emotionalTags);
		var songAttrTagsValue  = this._vaildateCheckBoxValues(songAttrTags);
		var yearTagsValue 	   = this._vaildateCheckBoxValues(yearTags);
		
		if(noTags){
			
			emotionalTagsValue = noTags;
		}
		
		//필터링 체크박스 값이 들어왔나 확인
		if(emotionalTagsValue == null && songAttrTagsValue == null && yearTagsValue == null){
			$("#filteredList").html("<div id = 'noFilterResult'>검색 결과가 없습니다</div>");
			
		} else { // 없으면 검색결과가 없습니다를 띄워준다
			
			this._submitFilter(emotionalTagsValue, songAttrTagsValue, yearTagsValue);
			
		} // 있으면 값을 가지고 Ajax전송 메소드로 이동, 리스트를 받아와서 뿌려준다
	},
	
	

	//체크박스에 있는 값들을 확인한다 만약 없다면 checkBoxValues array에 null을 넣어줌
	_vaildateCheckBoxValues : function (checkBoxElement){
		
		var checkBoxValues = new Array();
		
		for(var i = 0; i < checkBoxElement.length ; i++){
			
			if(checkBoxElement[i].checked == true){
				
				checkBoxValues.push(checkBoxElement[i].value);
			}// 체크된 값들만 checkBoxValue Array에 값을 넣는다 
		}// 체크박스의 Element 갯수만큼 포문을 실행한다
		
		if(checkBoxValues == ""){
			
			checkBoxValues = null;
		}// 위 과정에서 하나도 넣어진 값이 없다면 checkBoxValues에 null 값을 넣는다.

		return checkBoxValues;
	},

	
	
	// 필터된 리스트들을 ajax통신 통해 받아오는 메소드
	_submitFilter : function(emotionalTagsValue, songAttrTagsValue ,yearTagsValue){
		var self = this;
		
		jQuery.ajax({
			url : "filteringList.do",
			data : {
				emotional : emotionalTagsValue,
 				songAttr : songAttrTagsValue,
				year : yearTagsValue,
			},
			type : "POST",
			dataType : "json",
			traditional : true,
			success : function(responseData){
				
				if(responseData != null && responseData != ""){
					
					self._setListIntoHTML(responseData, 0, responseData.length, "filteredList");
					
				} else if (responseData == ""){
					
					$("#filteredList").html("<div id = 'noFilterResult'>검색 결과가 없습니다</div>");
				
				} else {
					
					alert("DB와 통신 실패!");
				}
			}
		});
	},

	
	// filterList를 표시할때 이전에 element들 삭제한다.
	_settingSideBarResultList : function (){
		
		$('#filteredList div').remove();
		$('.triangle').hide();
		
		if($('#filteredList').attr("isSharedListsRemoved") == undefined){
			
			$('#topListLabel').hide();
			$('#topList').remove();
			$('#sharedListLabel').hide();
			$('#sharedPublicList').remove();
		
			$('#filteredListLabel').show();
			$('#filteredListLabel').css("display", "block");
			$('#filtered#filteredListTriangle').show();
			$('#filteredList').attr("isSharedListsRemoved", 1);
		
		} else {
			
			return false;
		}
	},
	
	
	
	_onFocusSearchBox : function(oEvent){
		$.ajax({
			url : "searchListByString.do",
			type : "post",
			data : {
				search : null,
			},
			dataType : "json",
			success : function(responseData){
				
				if(responseData){

					var sharedLists = eval(responseData);
					var jsonSharedLists ={};
					
					for(var i = 0; i < sharedLists.length; i++){
						
						var listsName = sharedLists[i].name;
						var listsDescription = sharedLists[i].description;
						
						jsonSharedLists[i] = listsName;
					}
					
					var listsArray = $.map(jsonSharedLists, function (value, key) { return { value: value, data: key }; });
					
					$.mockjax({
						url: 'test',
					    responseTime: 10000,
					    response: function (settings) {
					    	var query = settings.data.query,
					    	queryLowerCase = query.toLowerCase(),
					        re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi'),
					        suggestions = $.grep(listsArray, function (lists) {
					            return re.test(lists.value);
					        }),
				        response = {
				    		query: query,
				            suggestions: suggestions
				            
				        };
				            this.responseText = JSON.stringify(response);
				            return false;
				            oEvent.stop();
				        }
				    });
				  		
					 $('.box').autocomplete({
					        lookup: listsArray,
					        minChars: 1,
					        onSelect: function(suggestion) {
					        	for (var i=0; i < sharedLists.length; i++) {
					        		if (sharedLists[i].num == suggestion.data) {

					        		}
					        	}
					        }
					    });
				}// end of responseDataTest
				
			}//end of success
		})
		.fail(function(){
			alert("DB와 통신 실패");	
		})
		.error(function(){
			alert("DB와 통신 실패");
		});
	},
	
	
	
	_submitSearchBox : function(oEvent){
		if(oEvent.key().enter){
			var self = this;
			var searchString = $('#searchString').val();
			
			if(searchString != ""){
				
				this._settingSideBarResultList();
				
				jQuery.ajax({
					url : "searchListByString.do",
					data : {
						search : searchString,
					},
					type : "POST",
					dataType : "json",
					traditional : true,
					success : function(responseData){
						
						if(responseData != null && responseData != ""){
							
							self._setListIntoHTML(responseData, 0, responseData.length, "filteredList");
							
						} else if (responseData == ""){
							
							$("#filteredList").html("<div id = 'noFilterResult'>검색 결과가 없습니다</div>");
						
						} else {
							
							alert("DB와 통신 실패!");
						}
					}
				});
				
				$('#searchString').blur();
				$('#searchString').val("");

			} else {
			
				alert("아무것도 입력하지 않으셨습니다.");
			
			}// null test End
		}// enter Test end
	},
	
	
	
	_isScrollEnd : function(){
		
		if($(window).scrollTop() == $(document).height() - $(window).height()) {
			
			this._addListsToScroll();
			
		}
	},
	
	
	
	_myFollowingList : function() {
		var self = this;
		
		jQuery.ajax({
			url : "myFollowingList.do",
			type : "POST",
			dataType : "json",
			traditional : true,
			success : function(responseData){
				
				if(responseData != null && responseData != ""){
					
					self._setListIntoHTML(responseData, 0, responseData.length, "filteredList");
					
				} else if (responseData == ""){
					
					$("#filteredList").html("<div id = 'noFilterResult'>검색 결과가 없습니다</div>");
				
				} else {
					
					alert("DB와 통신 실패!");
				}
			}
		});
	},
	
	
	
	//스크롤을 끝까지 갔을때 자동으로 리스트 더해주는 메소드
	_addListsToScroll : function (oEvent) {
		
		var moreShowNum = 4; // 스크롤 끝까지 갔을때 보여주는 리스트 숫자 늘어난다
		
		if (showListNum + moreShowNum > sharedPublicList.length) { // sharedPublicList의 길이가 더보기 + 초기 보여주는 값보다 짧을경우?
			
			if((showListNum + moreShowNum) - sharedPublicList.length < 4){// showListNum이 sharedPublicList의 길이보다 8이상 크다면

				this._setListIntoHTML(sharedPublicList, showListNum, sharedPublicList.length, "sharedPublicList");
				showListNum = moreShowNum + showListNum;
				
				$('#contentWrapper').css("height", function(){
					return $('#contentWrapper').height()+250;
				});
				
				$('#contentWrapper #contents').css("height", function(){
					return $('#contentWrapper #contents').height()+250;
				});
			
			}
			
		} else {// sharedPublicList길이가 더 길경우
			
			this._setListIntoHTML(sharedPublicList, showListNum, moreShowNum + showListNum, "sharedPublicList");
						
			showListNum = moreShowNum + showListNum;
			
			$('#contentWrapper').css("height", function(){
				return $('#contentWrapper').height()+250;
			});
			
			$('#contentWrapper #contents').css("height", function(){
				return $('#contentWrapper #contents').height()+250;
			});

		}
		
//		var size = jindo.$Document().scrollSize(); // 스크롤된 사이즈 계산
//		
//		
//		if(size.height > 900) { // 세로길이가 900보다 크면 맨 위로 버튼이 활성화
//			
//			jindo.$Element('topButton').show();
//		}
	},

	
	
	//맨 위로 버튼을 눌렀을때 맨 위로 스크롤 이동하는 메소드
	_onClickTopButton : function (oEvent) {

		window.scroll(0,0);	
	},
	
	
	
	_isHoverImage : function (oEvent) {
		var self = this;
		var el = oEvent.element;
		var parent = jindo.$Element(el).parent();
		
		var collNum = $(el).siblings(".collNum").html();
		var amiFollowing = $(el).siblings(".amiFollowing").html();

		parent.prepend("<div class='overlayRect'></div>");
		parent.prepend("<img class='overlayAdd' id='play' src='static/img/play-Icon.png'></img>");
		parent.prepend("<img class='overlayAdd' id='more' src='static/img/more.png'></img>");
		if(amiFollowing == 0){
			parent.prepend("<img class='overlayAdd' id='follow' src='static/img/Add.png'></img>");//clickEvent걸어줌
		}else{
			parent.prepend("<img class='overlayAdd' id='unFollow' src='static/img/Subtract.png'></img>");
		}
		
		$('#follow').click(function(){
			self._onClickFollowButton(collNum, el);
		});
		
		$('#unFollow').click(function(){
			self._onClickUnFollowButton(collNum, el);
		});
		
		$('#play').click(function(){
			self._onClickPlaySharedList(collNum);
		});
		
		$('#more').click(function(){
			self._onClickMoreInfoButton(collNum, amiFollowing);
		});
		
		$('.overlayAdd').hover(function(){}, function(){
			$(el).siblings(".overlayAdd").remove();
			$(el).siblings(".overlayRect").remove();
		});
		
		$('.overlayRect').hover(function(){}, function(){
			$(el).siblings(".overlayAdd").remove();
			$(el).siblings(".overlayRect").remove();
		});
	},
});
