var Main = $Class({
	_oElement : null,
	_oTemplate : null,
	_oEvent : null,
	_URLS : {
		LOGIN : '/seiren/loginValidate.do',
		MY_LIST : '/seiren/myList.do'
	},
	_MESSAGES : {
		INPUT_ID : '아이디를 입력해주세요.',
		INPUT_PASSWORD : '비밀번호를 입력해주세요.',
		FAIL_LOGIN : '로그인에 실패하였습니다.',
		WRONG_PASSWORD : '비밀번호가 일치하지 않습니다.'
	},
	
	$init : function() {
		$Fn(this._initialize, this).attach(window, 'load');
		$Fn(this._destroy, this).attach(window, 'unload');
	},
	
	_initialize : function() {
		this._oElement = {};
		this._oTemplate = {};
		this._oEvent = {};
		
		this._setElement();
		this._setEvent();
	},
	
	_destroy : function() {
		
	},
	
	_setElement : function() {
		this._oElement['headerArea'] = $('header');
		this._oElement['contentArea1'] = $('content1');
		this._oElement['contentArea2'] = $('content2');
		this._oElement['contentArea3'] = $('content3');
		this._oElement['contentArea4'] = $('content4');
		this._oElement['footerArea'] = $('footer');
		this._oElement['loginButton'] = $('login_button');
		this._oElement['idArea'] = $('idArea');
		this._oElement['img_mailIcon'] = $('img_mailIcon');
		this._oElement['passwordArea'] = $('passwordArea');
		this._oElement['signInArea'] = $('signInArea');
		this._oElement['signUpButton'] = $('signUp');
		
		this._oTemplate['tpl_click_idArea'] = $Template('tpl_click_idArea');
		this._oTemplate['tpl_click_pwArea'] = $Template('tpl_click_pwArea');
	},
	
	_setEvent : function () {
		this._oEvent['clickIdArea'] = $Fn(this._onClickIdArea, this).attach(this._oElement['idArea'], 'click');
		this._oEvent['clickPwArea'] = $Fn(this._onClickPwArea, this).attach(this._oElement['passwordArea'], 'click');
		this._oEvent['clickSignInArea'] = $Fn(this._onClickSignInArea, this).attach(this._oElement['signInArea'], 'click');
		this._oEvent['clickSignUpButton'] = $Fn(this._onClickSignUp, this).attach(this._oElement['signUpButton'], 'click');
	},
	
	_onClickIdArea : function (oEvent) {
		var el = oEvent.element;
		
		if (el.tagName.toLowerCase() != 'input') {
			$Element(this._oElement['idArea']).html(this._oTemplate['tpl_click_idArea'].process());
			var elIdInputBox = $('idInputBox');
			if (elIdInputBox) {
				elIdInputBox.focus();
			}
		} else if (el.id == 'warningIdLayer') {
			var elWarningIdLayer = $('warningIdLayer');
			if (elWarningIdLayer) {
				$Element(elWarningIdLayer).toggle();
			}
		}
		
		oEvent.stop();
	},
	
	_onClickPwArea : function (oEvent) {
		var el = oEvent.element;
		
		if (el.tagName.toLowerCase() != 'input') {
			$Element(this._oElement['passwordArea']).html(this._oTemplate['tpl_click_pwArea'].process());
			var elPwInputBox = $('pwInputBox');
			if (elPwInputBox) {
				elPwInputBox.focus();
			}
			
		} else if (el.id == 'warningPwArea') {
			var elWarningPwArea = $('warningPwArea');
			if (elWarningPwArea) {
				$Element(elWarningPwArea).toggle();
			}
		}
		
		oEvent.stop();
	},
	
	_onClickSignInArea : function (oEvent) {
		var elIdInputBox = $('idInputBox');
		var elPwInputBox = $('pwInputBox');
		
		if (!elIdInputBox || elIdInputBox.value == "") {
			alert(this._MESSAGES.INPUT_ID);
			oEvent.stop();
			return;
		}
		
		if (!elPwInputBox || elPwInputBox.value == "") {
			alert(this._MESSAGES.INPUT_PASSWORD);
			oEvent.stop();
			return;
		}
		
		var oAjaxParam = {
			'userId' : elIdInputBox.value,
			'userPw' : elPwInputBox.value
		};
			
		var oAjaxOption = {
			timeout : 5,
			method : 'post',
			onload : function(oResponse) {
				var oJson = oResponse.json();
				if (oJson) {
					if (oJson.isSuccess) {
						location.href = this._URLS.MY_LIST;
					} else {
						var sErrorMsg = oJson.errorMsg || this._MESSAGES.FAIL_LOGIN;
						if (sErrorMsg == this._MESSAGES.WRONG_PASSWORD) {
							var elWarningPwLayer = $('warningPwLayer');
								
							elPwInputBox.value = "";
							$Element(elPwInputBox).hide();
								
							if (elWarningPwLayer) {
								$Element(elWarningPwLayer).show();
							}
						} else {
							var elWarningIdLayer = $('warningIdLayer');
								
							elIdInputBox.value = "";
							elPwInputBox.value = "";
							$Element(elIdInputBox).hide();
								
							if (elWarningIdLayer) {
								$Element(elWarningIdLayer).show();
							}
						}
					}
				} 
			}.bind(this),
			ontimeout : function(oResponse) {
				alert(this._MESSAGES.FAIL_LOGIN);
			}.bind(this),
			onerror : function(oResponse) {
				alert(this._MESSAGES.FAIL_LOGIN);
			}.bind(this)
		};
			
		var sUrl = this._URLS.LOGIN;
		var oAjax = new $Ajax(sUrl, oAjaxOption);
		oAjax.request(oAjaxParam);
			
		oEvent.stop();
	},
	
	_onClickSignUp : function(oEvent) {
		var target = "/seiren/signUp.do";
			
		location.replace(target);
	}
});