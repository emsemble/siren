/**
 *  1. 파일 이름 : detailListInfo.js
 *  
 *  2. 작성자 : 김성철
 *
 *  3. 목적 : 상세 콜렉션 정보보기에서 발생하는 이벤트 등..
 * 
 *  4. 이력 : 2014. 05. 14. :  최초작성
 * 			  2014. 05. 19. :  comment CRUD 작성
 * 			  2014. 05. 22  :  초기화하면서 그래프 그리기 완성
 */



//초기화 메소드
//리스트 안에 곡 정보, 장르별 랭크, 댓글 json으로 받은것을 div에 각각 넣어준다.
function _initialize (detailInfo, genreRank, comments){
	
	$("#collectionNum").html(detailInfo[0].collectionNum);
	$("#collectionName").html(genreRank[0].collectionName);
	$("#collectionDescription").html(genreRank[0].collectionDescription);
	
	for (var i = 0; i < detailInfo.length; i++) {
		
		var div			  = "<div id = songList"+ i +" class ='songList'>";
		var songName	  = "<div class='songName'><span>" + detailInfo[i].songName + "</span></div>";
		var artistName	  = "<div class='artistName'><span>" + detailInfo[i].artistName + "</span></div>";
		var genre		  = "<div class='genre'><span>" + detailInfo[i].genre + "</span></div>";
		var divEnd 		  = "</div>";
		$("#songLists").append(div + genre + artistName + songName + divEnd);
	} // 리스트 안에 곡 정보들 div에 집어넣기
	
	for (var i = 0; i < genreRank.length; i++) {
		
		var div = "<div id = genreRank" + (i+1) + " class='genreList'>";
		var genre = "<div class='genre2'><span>" + genreRank[i].genre + "</span></div>";
		var rank = "<div class='rank'><span>" + genreRank[i].rank + " " + "</span></div>";
		var count = "<div class='count'><span>" + genreRank[i].count + " " +"</span></div></div>";
		$("#rankInfo").append(div + genre + rank + count);
	} // 리스트의 장르별 랭킹 div에 집어넣기
	
	_makeGraph(); // 장르별 랭킹으로 그래프 그려준다.
	
	if(comments == null || comments == ""){
		
		$("#showComments").html("댓글이 없습니다.");
		
	} else {
		
		_setComment(comments, "init");
	} // 댓글이 없으면 댓글이 없습니다를 표시해주고, 있다면 댓글을 표시해준다.
	
	for(var i = 0; i < (detailInfo.length < 8 ? detailInfo.length : 8) ; i++){
		
		var imgsrc = getImgSrc(detailInfo[i].albumId);
		
		var img = "<img src ='"+ imgsrc + "' class = 'albumImg'></img>";
		
		$("#albumImages").append(img);
	} // 리스트의 노래들 앨범 이미지 가져와 배치해줌..
};



//highcharts 를 이용해서 그래프를 그려준다.
function _makeGraph(){
	
	var graphData = new Array(genreRank.length);
	
	for(var i=0; i < genreRank.length; i++){
		graphData[i] = [genreRank[i].genre, genreRank[i].count];
	}// 장르별 랭킹 받은것을 토대로 그래프로 넘겨줄 데이터 작성한다
	
	Highcharts.getOptions().plotOptions.pie.colors = (function () {
        var colors = [];

        var base = "#00cd99";

        for (var i = 10; i > 0; i--) {
            colors.push(Highcharts.Color(base).brighten((i - 10) / 10).get());
        } // 컬러 결정, 단색으로 하는 설정
        return colors;
	}());
	
    $('#graph').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false,
            width: 400,
            height: 250
        },
        title: {
            text: 'Genre <br>shares',
            align: 'center',
            verticalAlign: 'middle',
            y: 70
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    crop:false,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textShadow: '0px 1px 2px black'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '100%'],
                size: 400
            }
        },
        series: [{
            type: 'pie',
            name: 'Genre share',
            innerSize: '50%',
            data: graphData
        }]
    });
};



// 버튼 누를시 이벤트를 호출할수 있는 메소드
function _eventListner() {
	
	$("#showComments").click(function (){
		var target = event.target;
		
		if($(target).prop("id") == "modifyComment"){
			
			_settingModifyComment(target);
			
		} else if ($(target).prop("id") == "deleteComment"){
			
			_deleteComment(target);
		}
	});
	
	$("#saveComment").click(function(){
		_saveComment();
	});
	
	$("#follow").click(function(){
		if($(this).attr("ami") == 0){
			_followList();
			$(this).attr("ami", 1);
			$(this).html("UNFOLLOW");
			$(this).attr("src", "static/img/follow.png");
			$(this).css("background", "#00CD97");
		}
		
		else{
			_unFollowList();
			$(this).attr("ami", 0);
			$(this).html("FOLLOW");
			$(this).attr("src", "static/img/unfollow.png");
			$(this).css("background", "#e64c65");
		}
	});	
	
	$("#playList").click(function(){
		_playList();
	});
	
};



//초기화시 커멘트를 세팅
function _setComment(comments, flag){
	
	if(flag == "init") {
		for (var i  = 0; i < comments.length; i++) {
			
			var div = "<div id = 'comment" + comments[i].num + "' isModifyComment='0' class='commentsDiv'>";
			var num = "<input type='hidden' id ='commentNum' value = "+ comments[i].num + " class ='commentNum'></input>";
			var writer = "<div class='writer'><span>" + comments[i].writer + "</span></div>";
			var writeTime = "<div class='writeTime'><span>" + comments[i].writeTime + "</span></div>";
			var recentComments = "<div class='recentComments'><span>" + comments[i].contents + "</span></div>";
			
			var isMyComment = comments[i].isMyComment; // 내 코멘트인지 받는 변수
			var setModAndDel = _setCommentButtons(isMyComment); // 내 댓글이면 Modify와 Delete버튼을 활성화
			
			var endDiv = "</div>";
			
			
			if(setModAndDel != undefined){
			
				$("#showComments").append(div + num + writer + writeTime + recentComments + setModAndDel + endDiv);
				
			}else if(setModAndDel == undefined){
				
				$("#showComments").append(div + num + writer + writeTime + recentComments + endDiv);
			}// 내댓글이면 Modify와 Delete버튼 붙이고 undefiend면 버튼 안붙인다.
		}//초기화 할시 커멘트 세팅 종료
		
	} else if(flag == "insert"){
		
		var div = "<div id = 'comment" + comments.num + "' isModifyComment='0' class='commentsDiv'>";
		var num = "<input type='hidden' id ='commentNum' value = "+ comments.num + " class ='commentNum'></input>";
		var writer = "<div class='writer'><span>" + comments.writer + "</span></div>";
		var writeTime = "<div class='writeTime'><span>" + comments.writeTime + "</span></div>";
		var recentComments = "<div class='recentComments'><span>" + comments.contents + "</span></div>";
		
		var isMyComment = comments.isMyComment;
		var setModAndDel = _setCommentButtons(isMyComment);
		
		var endDiv = "</div>";
		
		
		$("#showComments").append(div + num + writer + writeTime + recentComments + setModAndDel + endDiv);
	}// 위와 같음, 무조건 나의 댓글이기 때문에 modify와 delete가 반드시 들어간다.
	
};// 커멘트 입력할때 커멘트 세팅 종료


// 커멘트 버튼을 세팅하는 메소드
function _setCommentButtons(isMyComment){
	
	if(isMyComment == true){
		
		var setModAndDel = "<img src ='static/img/modify2.png' id = 'modifyComment' class = 'modifyComment'>" + "<img src ='static/img/delete.png' id ='deleteComment' class = 'deleteComment'>";
		
		return setModAndDel;
	
	}
};



//커멘트 입력을 누를때 ajax통신으로 댓글 입력
function _saveComment() {
	
	var value = $("#commentContents").val();
	
	if(_validateCommentSize(value) == false){
		return false;
	};
	
	if(value == ""){
		return false;
	}
	
	$.ajax({
		url : "saveComment.do",
		data : {
			contents:$("#commentContents").val(),
			collectionNum:$("#collectionNum").html(),
		},
		type : "POST",
		dataType : "text",
		success : function(responseData){
			var _json = eval("("+responseData+")");
			
			if(_json.isSuccess == true){
				
				if($("#showComments").html() == "댓글이 없습니다."){
					
					$("#showComments").html("");
				}
				
				var comment = eval("("+_json.commentVO+")");
				_setComment(comment, "insert");
				$("#commentContents").val("");
				_getWindowHeight();
				
			} else if(_json.isSuccess == false){
				
				alert("댓글 입력에 실패하셨습니다.");
			}
			
		}//end of success
	});//end of $.ajax
}



//선택한 댓글에 수정하기 버튼을 눌렀을때 버튼 설정 및 값 전달.
function _settingModifyComment(target){

	var modifyComments = "<div class='commentModifyContents'><textarea rows='2' cols='45' id ='commentModifyContents'/></div>";
	var saveModifyComment = "<img src ='static/img/write.png' class='saveModifyComment'>";
	var	cancleModifyComment = "<img src ='static/img/delete.png' class='cancleModifyComment'>";

	_autoCancleModifyComment(); // 이전에 수정하기 버튼 눌렀던 것이 있으면 자동으로 원상태로 복귀
	
	var targetParent = $(target).parent();
	var commentContents = $(target).siblings(".recentComments").children("span").html();
	
	$(target).siblings(".recentComments").remove();
	$(target).siblings(".deleteComment").remove();
	$(target).remove();
	
	$(targetParent).append(modifyComments + saveModifyComment + cancleModifyComment);
	$(targetParent).children(".commentModifyContents").children("#commentModifyContents").html(commentContents);
	targetParent.attr("isModifyComment", "1");
	
	_modifyComment(targetParent);
};



// 실제로 수정하는 값을 전달해주는 메소드
function _modifyComment(targetParent){
	
	var targetCommentNum = $(targetParent).children("#commentNum");
	var modifyCommentContents = $(targetParent).children(".commentModifyContents").children("#commentModifyContents");

	$(this).unbind('click'); // 이벤트 버블링 없에기 위해 언바인드
	$(this).click(function(){
		var clickedDomID = $(event.target).parent().attr("id");
		if(clickedDomID == "inputComment"){
			_autoCancleModifyComment();
		}// 인풋 커멘드에 있는 요소들 선택할시(댓글입력창 클릭, 댓글등록 버튼 클릭) 자동으로 modify창 닫기
	});

	$(targetParent).children(".saveModifyComment").unbind('click'); // 이벤트 버블링 없에기 위해 언바인드
	$(targetParent).children(".saveModifyComment").click(function(){
		
		var value = modifyCommentContents.val();
		
		if(_validateCommentSize(value) == false){
			return false;
		};

		$.ajax({
			url : "updateComment.do",
			data : {
				contents : modifyCommentContents.val(),
				commentNum : targetCommentNum.val(),
			},
			type : "POST",
			dataType : "text",
			success : function(responseData){
				var _json = eval("("+responseData+")");
				
				if(_json.isSuccess == true){
					
					alert("댓글 수정에 성공했습니다");
					_autoCancleModifyComment();
					
				}else if(_json.isSuccess == false){
					
					alert("댓글 수정에 실패했습니다");
					_autoCancleModifyComment();
				}
			}
		}); // ajax통신으로 댓글 수정하기
	});
	
	$(targetParent).children(".cancleModifyComment").click(function(){
		_autoCancleModifyComment();
	});
};



// Ajax통신으로 댓글 지우기
function _deleteComment(target){
	
	var targetComment = $(target).siblings("#commentNum");
	var targetNum = targetComment.val();
	var targetParent;
	
	$.ajax({
		url : "deleteComment.do",
		data : {
			commentNum : targetNum,
		},
		type : "POST",
		dataType : "text",
		success : function(responseData){
			var _json = eval("("+responseData+")");
			
			if(_json.isSuccess == true){
				
				targetParent = $(target).parent();
				$(targetParent).remove();
				
				if($("#showComments").html() == ""){
					
					$("#showComments").html("댓글이 없습니다.");
				}
				
			} else if(_json.isSuccess == false){
				
				alert("댓글 삭제에 실패했습니다..");
			}
		}
	});
};



//ModifyComment창이 열린것을 자동으로 닫아주는 메소드
function _autoCancleModifyComment(){
	
	var prevModifyComment = $("#showComments").children("div[isModifyComment = 1]"); // ModifyComment 중인것을 선택
	
	if(prevModifyComment.attr("id") != undefined){
		var comment = prevModifyComment.children(".commentModifyContents").children("#commentModifyContents").val();
		var recentComments = "<div class='recentComments'><span>" + comment + "</span></div>";
		var modifyComment = "<img src ='static/img/modify2.png' id = 'modifyComment' class = 'modifyComment'>";
		var deleteComment =	"<img src ='static/img/delete.png' id ='deleteComment' class = 'deleteComment'>";
		
		prevModifyComment.children(".commentModifyContents").remove();
		prevModifyComment.children(".saveModifyComment").remove();
		prevModifyComment.children(".cancleModifyComment").remove();
		
		prevModifyComment.append(recentComments + modifyComment + deleteComment);
		
	}// ModifyComment가 되어있는것들은 모두 원상태로 복귀
	
	prevModifyComment.attr("isModifyComment" ,"0"); // ModifyComment상태를 0으로 다시 돌려놔
};



// 팔로우 리스트 버튼눌렀을때 팔로우 맺기
function _followList(){
	$.ajax({
		url : "addFollowList.do",
		data : {
			collectionNum : $("#collectionNum").html()
		},
		type : "POST",
		dataType : "text",
		success : function(responseData){
			var _json = eval("("+responseData+")");
			if (_json.result =="success") {
				
			} else if (_json.result == "duplicated"){
				
				alert("이미 추가된 리스트입니다.");
				
			} else if (oJson.result == "isMyList"){
				
				alert("자신의 리스트는 팔로우 할수 없습니다.");

			} else {
				
				alert("공유 리스트 추가에 실패했습니다.");
			}
		}
	});
};



// 언팔로우 버튼 눌렀을때 팔로우 해제
function _unFollowList(){
	$.ajax({
		url : "deleteFollowList.do",
		data : {
			collectionNum : $("#collectionNum").html()
		},
		type : "POST",
		dataType : "text",
		success : function(responseData){
			var _json = eval("("+responseData+")");
			if (_json.result =="success") {
				
			} else if (_json.result == "duplicated"){
				
				alert("이미 삭제된 리스트입니다.");
			
			} else {
				
				alert("공유 리스트 해제에 실패했습니다.");
			}
		}
	});
};



// 바로 리스트 재생
function _playList(){
	var collectionNum = $("#collectionNum").html();
	window.open("enterList.do?listNum="+collectionNum);
};



// Contents화면의 크기를 설정
function _getWindowHeight(){
	var documentHeight = $(document).height();
	var screenHeight = window.innerHeight;
		
	$("#contents").height(function(){
		return documentHeight >= screenHeight ? documentHeight : screenHeight;
	});
	
	$("#contentsWrapper").height(function(){
		return documentHeight >= screenHeight ? documentHeight+80 : screenHeight+80;
	});
};



function _validateCommentSize(value){
	
	var stringByteLength = ( function (s,b,i,c) {
		for(b=i=0;c=s.charCodeAt(i++);b+=c>>11?3:c>>7?2:1);
		return b;
		}) (value);
	
	if (stringByteLength < 300) {
		
		return true;

	} else {
		
		alert("댓글은 한글 100자, 영문 300자 이내로 입력해주세요.");
		return false;
	}
}