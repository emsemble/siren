/**
 * 1. 파일 이름 : player.js 
 * 
 * 2. 작성자 : 고상우
 * 
 * 3. 목적 : 플레이어와 관련된 스크립트
 * 
 * 4. 특이사항 : JQuery / D3 필요
 * 
 * jQuery의 버그인지, svg의 circle에 있는 custorAttribute인 songNum 자체를 인식하지 못한다.
 * 
 * 때문에 circle을 조작하는 것은 항상 자바스크립트의 엘리먼트 객체를 통해서 진행되며,
 * 
 * jQuery의 attribute필터와 같은 기능을 하는 함수를 만들어 사용한다.
 * 
 * 5. 이력 : 
 * 
 * 2014. 04. 24. player.jsp로 부터 분리
 * 
 * 2014. 04. 27. line의 좌표가 circle 좌표에 의존적이게 하고, 해당 라인이 어떤 노래간의 연결인지 알 수 있게끔 수정. 
 * 
 * 2014. 04. 29. 곡 재생 순서 메커니즘 수정. 다음 곡으로 서로 지목한 경우 무한 루프에 빠지지 않도록 플레이 리스트를 사용하는 방식
 * 
 * 2014. 05. 02. zooming / panning / auto focusing 기능 구현
 * 
 * 2014. 05. 24. WebGL(three.js)를 사용해 공간 형성
 */
window.onload = function() {
	  var element = document.getElementById('content');
	  element.onselectstart = function () { return false; }; // ie
	  element.onmousedown = function () { return false; }; // mozilla
}

//기본적인 정보
var collectionNum, collectionName;

// 플레이어 : 비쥬얼라이져에서 사용할 변수들
var _jsonPlayer,
	_width,
	_height,
	$d3Svg,
	$d3Lines,
	_alColor,
	_alCluster = [],
	_alPlayList,
	$d3Circles,
	$d3focus,
	$_detailDiv,	
	$d3ZoomB;

//실제 재생에 사용할 변수들
var _streamServer = "http://seiren.gonetis.com:8081/";
var	_fileName,
	_alPlayList;

var isPlayerLoaded = false;
var isLinearListOn = false;
var _volume = 1; 

//3d



function _loadPlayer(listNum){

		$.ajax({
			url : "getPlayer.do",
			data : {
				listNum : listNum,
				width : screen.width,
				height : screen.height
			},
			type : "POST",
			dataType : "text",
			success : function(responseData){
				_initBackGround();
				$("#loading").slideUp(function(){					

					var _json = eval("("+responseData+")");
					collectionNum = _json.collectionNum;
					collectionName = _json.collectionName;
					
					$("#collectionNum").text(collectionNum);
					$("#collectionName").text(collectionName);
					
					_initialize(_json.player);
		
					_mkCircles();
					_mkLines();
					_mkText(); 
					_popUpCircles();
					_mkController();
					_bindEventListner();
					_saveThumbnail($("#collectionNum").text());					
				});			
			}//end of success	
		});//end of $.ajax
}



//플레이어를 생성하기 위한 초기값 준비
function _initialize(strJson) {
	_jsonPlayer = eval("("+strJson+")");
	_width = _jsonPlayer.width;
	_height = _jsonPlayer.height;
	
	$d3ZoomB = d3.behavior.zoom()
		.size([_width, _height])
		.scaleExtent([1, 3])
		.on("zoom", zoom);
	
	_alColor = ["rgb(0, 182, 148)", "rgb(74, 113, 181)", "rgb(165, 121, 181)", "rgb(247, 97, 99)", "rgb(255, 170, 115)",
	            "rgb(165, 211, 148)", "rgb(0, 170, 115)", "rgb(41, 89, 165)", "rgb(132, 89, 165)", "rgb(206, 93, 165)",
	            "rgb(239, 48, 74)", "rgb(247, 134, 82)", "rgb(255, 207, 90)", "rgb(247, 97, 99)", "rgb(214, 227, 90)",
	            "rgb(115, 195, 107)", "rgb(0, 174, 173)", "rgb(0, 130, 206)", "rgb(115, 146, 206)", "rgb(206, 162, 206)",
	            "rgb(239, 44, 107)", "rgb(247, 138, 132)", "rgb(255, 211, 165)", "rgb(214, 231, 181)", "rgb(107, 199, 181)",
	            "rgb(0, 186, 206)", "rgb(156, 215, 214)", "rgb(0, 162, 222)", "rgb(156, 186, 231)", "rgb(247, 97, 140)", "rgb(255, 174, 173)"
	            ];
	
	
	//svg를 바디 안에 생성한다.
	$d3Svg = d3.select("#visualizeArea").append("svg").attr("width", _width)
			.attr("height", _height)
			.call($d3ZoomB)
			.append("g");
	
	//바닥에 깔릴 사각형을 생성한다. 투명한 배경
	$d3Svg
	.append("rect")
    .attr("class", "overlay")
    .attr("width", _width)
    .attr("height", _height);
	
	//각 노래들 사이의 연결선
	$d3Lines = $d3Svg.selectAll("path").data(_jsonPlayer.lines).enter()
	.append("path");
	
	//각 노래를 대표할 원
	$d3Circles = $d3Svg.selectAll("circle").data(_jsonPlayer.circles).enter()
	.append("circle");
	
	//노래 제목
	$d3Text = $d3Svg.selectAll("text").data(_jsonPlayer.circles).enter()
	.append("text");
};



//데이터에 맞게 노래제목을 생성한뒤 숨김
function _mkText(){
	$d3Text.attr("transform", function(d){
		return "translate(" + 	(d.location[0] * _jsonPlayer.scaleX + _jsonPlayer.errorX) + "," 
				+ (d.location[1] * _jsonPlayer.scaleY + _jsonPlayer.errorY +27) +")";							
	})	
	.attr("songNum", function(d) {
		return d.sonNum;
	})
	.attr("songNum", function(d) {
		return d.songNum;
	})
	.attr("cluster", function(d) {
		
		if($.inArray(d.color, _alCluster) == -1){
			_alCluster.push(d.color);
		}
		return d.color;
	})
	.text(function(d){
		var source = _jsonPlayer.songList[d.songNum].songName;
		
		if(source.length > 10){
			return source.slice(0,10).concat("...");
		}
		
		else{
			return source;
		}		 
	})
	.attr("fill","white")
	.attr("text-anchor", "middle");
	
	_alCluster.sort();
	$("text").hide(0);
}



//데이터에 맞게 연결선을 생성한뒤 숨김
function _mkLines(){
	$d3Lines.attr("d", function(d) {
		return "M"+ (d.sourceLoc[0] * _jsonPlayer.scaleX + _jsonPlayer.errorX)
				+ " " + (d.sourceLoc[1]  * _jsonPlayer.scaleY + _jsonPlayer.errorY)				
				+"L" + (d.targetLoc[0] * _jsonPlayer.scaleX + _jsonPlayer.errorX)
				+ " " + (d.targetLoc[1] * _jsonPlayer.scaleY + _jsonPlayer.errorY);
	})
	.attr("corr", function(d) {
		return d.corr;
	})
	.attr("class","line")
	.attr("stroke-opacity", function(d) {
		return d.corr * (10.0 / 9) - (1.0 / 9);
	})
	.attr("source", function(d){
		return d.source;
	})
	.attr("target", function(d){
		return d.target;
	})
	
	$("path").hide(0); 
};//end of mkLines()



//데이터에 맞게 원을 생성한뒤 숨김
function _mkCircles(){
	$d3Circles
	.attr("cx", function(d){
			return d.location[0] * _jsonPlayer.scaleX + _jsonPlayer.errorX;
	})	
	.attr("cy", function(d){
		return d.location[1] * _jsonPlayer.scaleY + _jsonPlayer.errorY;
	})	
	.attr("songNum", function(d) {
		return d.sonNum;
	})
	.attr("r", function(d) {
		return 9;
	})
	.attr("class", "circle").attr("songNum", function(d) {
		return d.songNum;
	})
	.attr ("songName", function (d){
		return _jsonPlayer.songList[d.songNum].songName;
	})
	.attr ("albumId", function (d){
		return _jsonPlayer.songList[d.songNum].albumId;
	})
	.attr ("artistName", function (d){
		return _jsonPlayer.songList[d.songNum].artistName;
	})
	.attr("cluster", function(d) {
	
		if($.inArray(d.color, _alCluster) == -1){
			_alCluster.push(d.color);
		}
		return d.color;
	})
	.style("fill", function(d) {
		return _alColor[d.color];
	});
		
	$( "circle" ).hide(0);  
	_initLinearList();
};



//선형 리스트 초기화
function _initLinearList(){
	var $_tempUl;
	var $_tempLi;
	var $_titleSpan;
	var _targetSong;
	
	$("#linearList")
	.css("height", _height)
	.css("width", function(){return (_width * 0.28) +"px";});
	
	for(var i = 0; i < _alCluster.length; i++){
		$("#linearList").append("<ul>");
		_circles = _getCircleByCluster(i);
	
		for(var j = 0; j < _circles.length; j++){
			$_tempUl = $("#linearList").children().eq(i).append("<li>").attr("group", i);
			$_tempLi = $_tempUl.children().eq(j);
			_targetSong = _jsonPlayer.songList[_circles[j].getAttribute("songNum")];
			
			$_tempLi.attr("songNum", _circles[j].getAttribute("songNum"))
					.attr("isSelected", 0);
			$_tempLi.append("<span>");
			
			$_titleSpan = $_tempLi.children().eq(0)
			$_titleSpan.css("vertical-align","middle")
						.attr("class", "labelBlock");
			
			$_titleSpan.append("<div>");
			$_titleSpan.children().eq(0)
			.css("backgroundColor" , function(){
				return _alColor[i];
			}).css("width" , 3)
			.css("height" , 16)
			.css("display", "inline-block")
			.css("margin-right", 10);
			
			
			$_tempLi.append(_targetSong.songName + " - " + _targetSong.artistName);
			
			$_tempLi.append("<div>");
			
			$_detailDiv = $_tempLi.children().eq(1);
			
			$_detailDiv.attr("class", "detail")
						.css("height", 150)
						.css("width", function(){return document.width/4})
						.hide(0);
							
			$_detailDiv.append("<img src='"+_getImgSrc(_targetSong.albumId)+"'height='110px' width='110px'/>")
			.append(_targetSong.songName + "<br>")
			.append(_targetSong.artistName + "<br>")
			.append(_targetSong.genre + "<br>")
			.append(_targetSong.issueDate + "<br>")
			.append(_targetSong.nationalityName + "<br>")
			.append(_targetSong.actType);
		}
	}
	
	$("#linearList").hide(0);	
}



function _mkController(){
	$("#controlArea").append("<div id='controller'></div>");
	
	$("#controller").append("<div id='hiddenArea' class='ui-helper-hidden'></div>")
		.append("<span class='glyphicon glyphicon-play comp' id='pause' style='margin-top: 4px;font-size:25px;'></span>")
		.append("<span class='glyphicon glyphicon-forward comp' id='shuffle' style='margin-left: 8px;'></span>")		
		.append("<div class='songInfo'></div>")
		.append("<span class='glyphicon glyphicon-repeat comp'  id='loop' style='left:360px;'></span>")
		.append("<span class='glyphicon glyphicon-list comp' id='showList' style='left:370px;'></span>");
	
	$(".songInfo").append("<img class='albumImg' src='static/img/albumImg.PNG'>")
		.append("<span class='albumInfo'></span>")
		.append("<div id='playControl'  class='slider'></div>");
	
	
		/*.append("<div id='volumeControl' class='slider' style='width:260px;'></div>")
		.append("<div id='playTime' ></div>");*/
	
	
	
	//voulumeController 생성
	$("#volumeControl").slider({
		value : 100,
		orientation : "horizontal",
		range : "min",
		animate : true
	});		

	
	// 플레이 타임 콘트롤러 생성
	$("#playControl").slider({
		value : 0,
		orientation : "horizontal",
		range : "min",
		max : 100,
		animate : true
	});
}



// 이벤트 리스너 붙이는 메소드
function _bindEventListner() {
	
	$("circle").single_double_click(function(){
		
			if(isPlayerLoaded){
				$("li[songNum="+ this.getAttribute("songNum") +"]").click();
				_autoScroll($("li[songNum="+ this.getAttribute("songNum") +"]"));
			}
			
			if(!isLinearListOn){
				$("#showList").click();
			}
		},function(){
			
			if(isPlayerLoaded){
			_playMusic(this);
			_setPlayList(this.getAttribute("songNum"));
		}		
	});


	$("#pause").click(function(){

		if(isPlayerLoaded){
			_pause();
		}
	})


	$("#loop").click(function(){

		if(isPlayerLoaded){
			_loop(this);
		}				
	})


	$("#shuffle").click(function(){

		if(isPlayerLoaded){
			_shuffle();					
		}				
	})



	$("#showList").click(function(){
		if(isPlayerLoaded){
			_toggleList(this);					
		}	
	})


	
	$("li").hover(function(){
		_mouseOverLinearList(this);
	}, function(){
		_mouseOffLinearList(this);
	});



	$("li").single_double_click(function(){
		$("li[isSelected = 1]")
		.filter("[songNum != "+this.getAttribute("songNum") +"]")
		.attr("isSelected", 0).children().eq(1).slideUp();
		
			 			
			if($(this).attr("isSelected") == 1){
				$(this).children().eq(1).slideUp();
				$(this).attr("isSelected", 0);
			}
			
			else{
				$(this).children().eq(1).slideDown();
				$(this).attr("isSelected", 1);
			}			
		},
		function(){
		_dblClickLinearList(this);
	});

	
	
	//volumeController EventListening : slide
	$("#volumeControl").on("slide", function(event, ui){
		_changeVolume(ui.value);
	});
	
	
	
	//playControl EventListening : slide
	$("#playControl").on("slide", function(event, ui){
		_changePlayTime(ui.value);
	});
	
	$("#playerHeader").hover(function(){
		$(this).animate({opacity : 1}, "fast");
	}, function(){
		$(this).animate({opacity : 0}, "fast");
	});
};



//원 팝업 애니메이션
function _popUpCircles(){
	$( "circle" ).first()
	.show( 100, function showNext() {
	    $( this ).next( "circle" ).show( 100, showNext );
	});
	
	_fadeInLines();
	_fadeInTexts();
};



//연결선 페이드 인
function _fadeInLines(){
	var waitTime = ($("circle").length + 3) * 100;
	$( "path" ).delay(waitTime, "fx").fadeIn(400);
};



//제목 페이드 인
function _fadeInTexts(){
	var waitTime = ($("circle").length + 3) * 100;
	$( "text" ).delay(waitTime, "fx").fadeIn(400, function(){ isPlayerLoaded= true });
}



//전달받은 노래를 재생한다.
function _playMusic(_target) {
	
	_stopAnimation($("circle"));
	_stopAnimation($(".labelBlock"));
	_playAnimation($(_target));	
	_playAnimation($("li[songNum="+_target.getAttribute("songNum") +"]").children().eq(0));
	
	$("#pause").attr("class", "glyphicon glyphicon-pause comp");
	
	if(_jsonPlayer != null){
		$("#hiddenArea").html(
		'<audio controls id ="player" autoplay="true"><source src="" type="audio/mpeg"></audio>');
		
		_fileName = _jsonPlayer.songList[_target.getAttribute('songNum')].fileName;
		
	
		$("#player").children().first().attr("src", _streamServer + _fileName);
		
		$(".albumImg").attr("src", _getImgSrc(_target.getAttribute('albumId')));
		$(".albumInfo").html(_target.getAttribute('songName') + " - " + _target.getAttribute('artistName'));
		
		$("#player")[0].volume = _volume;
		_focusOn(_target);
		
		$("#player").data("songNum", _target.getAttribute("songNum"));
		_showPlayTime();
		
		$("#player").data("cluster", _target.getAttribute("cluster"));
		_showPlayTime();
		
		$("#player").bind("ended", function(){
			_stopAnimation($(_target));
			_incrPlayNum(_target.getAttribute("songNum"));
			_playMusic(_getNext(_target.getAttribute("songNum")));									
		});
	}; //end of if
}; //end of .dblclick() : event handle



// 재생되고 있을때 애니메이션
function _playAnimation($_target){
	$_target.animate({opacity : 0}, "slow", function(){
		$_target.animate({opacity : 1}, "slow", _playAnimation($_target));
	})
};



// 대상의 애니메이션 정지
function _stopAnimation($_target){
	$_target.clearQueue();
	$_target.css({opacity : 1});	
	$_target.stop();
};



// 다음 노래를 얻는다
function _getNext(_nowNum){
	var _nextNum = _alPlayList[(_alPlayList.indexOf(parseInt(_nowNum)) + 1) % _alPlayList.length];
	return _getCircleBySongNum(_nextNum);
}



// 클러스터에 따라 플레이리스트를 생성한다
function _setPlayList(_nowPlayingNum){
	_alPlayList = [];
	_alPlayList.push(parseInt(_nowPlayingNum));
	
	var _harmonious;
	var _next;
	var _circlesInCluster = _getCircleByCluster(_getPresentCluster());
	
	for(var i = 1; i < _circlesInCluster.length; i++){
		_harmonious = _jsonPlayer.harmonyList[_alPlayList[i-1]]
		
		for(var j = 0; j < _harmonious.length; j++){
			_next = _harmonious[Math.floor((Math.random()*_harmonious.length))];
			
			if(_alPlayList.indexOf(parseInt(_next)) == -1){
				break;
			}
		}
			
		if(_alPlayList.indexOf(parseInt(_next)) != -1){
			
			for(var j = 0; j < _circlesInCluster.length; j++){
				_next = _circlesInCluster[j].getAttribute("songNum");
				
				if(_alPlayList.indexOf(parseInt(_next)) == -1){
					break;
				}
			}
		}
		_alPlayList.push(parseInt(_next));
	}//end of for()
}//end of setplayList



//특정 songNum을 대표하는 원을 가져온다
function _getCircleBySongNum(_songNum) {
	var _allCircles = document.getElementsByTagName('circle');
	var _matchingCircle;
	
	for (var i = 0; i < _allCircles.length; i++) {
		
		if (_allCircles[i].getAttribute("songNum") == _songNum ) {
			// Element exists with attribute. Add to array.
			_matchingCircle = _allCircles[i];
			break;
		}
	}
	return _matchingCircle;
}



//특정 cluster에 속하는 노래를 대표하는 원을 모두 회수한다
function _getCircleByCluster(_clusterNum) {
	var _allCircles = document.getElementsByTagName('circle');
	var _matchingCircles = [];
	
	for (var i = 0; i < _allCircles.length; i++) {
		
		if (_allCircles[i].getAttribute("cluster") == _clusterNum ) {
			// Element exists with attribute. Add to array.
			_matchingCircles.push(_allCircles[i]);
		}
	}
	return _matchingCircles;
}



// 섬네일을 생성한다
function _saveThumbnail(_collectionNum){
	var _thumbnail = $("#visualizeArea").html();
	setTimeout(function(){
		$.ajax({
			url : "saveThumbnail.do",
			data : {
				thumbnail : _thumbnail,
				collectionNum : _collectionNum
			},
			type : "POST",
			dataType : "text",
			success : function(responseData){
				//do nothing
			}//end of success
		});//end of $.ajax
	}, 1500);
}	



// 현재 재생중인 클러스터
function _getPresentCluster(){
	return $("#player").data("cluster");
}
	


// 다음곡 버튼이 눌렸을 때, 10초이내이면 클러스터를 전환하고 10초 이후면 플레이리스트 상의 다음 곡을 재생
function _shuffle(){
	
	if($("#player")[0].currentTime < 10){
		var _present = _getPresentCluster();
		var _nextCluster = (parseInt(_present) + 1) % _alCluster.length;
		var _nextMembers = _getCircleByCluster(_nextCluster)
		
		_playMusic(_nextMembers[Math.floor((Math.random()*_nextMembers.length))]);
		_setPlayList(($("#player").data("songNum")));
	}
	
	else{
		_playMusic(_getNext($("#player").data("songNum")));
	}	
}



// 일시 정지
function _pause(){
	
	if($("#player")[0].paused){
		$("#player")[0].play();
		_playAnimation($(_getCircleBySongNum($("#player").data("songNum"))));
		$("#pause").attr("class", "glyphicon glyphicon-pause comp");		
	}
	
	else{
		$("#player")[0].pause();
		_stopAnimation($("circle"));
		$("#pause").attr("class", "glyphicon glyphicon-play comp");
	}
}



// 루프
function _loop(bttn){
	if($("#player")[0].loop){
		$("#player")[0].loop = false;
		$(bttn).css("color", "white")
	}
	
	else{
		$("#player")[0].loop = true;
		$(bttn).css("color", "#00ce9b")
	}
}



//리스트 버튼이 눌렸을때 실행될 메소드. 리스트 토글.
function _toggleList(bttn){	
	
	if(isLinearListOn){
		$(".detail").hide(0);
		$(".detail").parent().attr("isSelected", 0);
		$("#linearList").animate({width: 'toggle'});
		isLinearListOn = false;	
		$(bttn).css("color", "white")
	}
	
	else{
		$("#linearList").animate({width: 'toggle'});
		isLinearListOn = true;
		_focusOut();
		$(bttn).css("color", "#00ce9b")
	}	
}



//리니어 리스트의 각 노래에 마우스가 오버되었을때 실행될 메소드
function _mouseOverLinearList(_target){
	_getCircleBySongNum($(_target).attr("songNum")).setAttribute("r", 20);
	_focusWithoutZoom(_getCircleBySongNum($(_target).attr("songNum")));
}



//리니어 리스트의 위에서 마우스가 떨어졌을대 실행될 메소드
function _mouseOffLinearList(_target){
	_getCircleBySongNum($(_target).attr("songNum")).setAttribute("r", 9);
	//_focusOn(_getCircleBySongNum($("#player").data("songNum")));	
}



//선형리스트의 특정 li에 오토 스크롤
function _autoScroll($_li){
	$("#linearList").scrollTop($_li.position().top - 100);
}



//리니어리스트가 더블클릭 되었을때 실행될 메소드
function _dblClickLinearList(_target){
		_playMusic(_getCircleBySongNum($(_target).attr("songNum")));
		_setPlayList($(_target).attr("songNum"));
}



//볼륨 바꾸는 메소드
function _changeVolume(_change){
	_volume = _change * 0.01;
	
	$("#player")[0].volume = _volume;
}



//플레이타임을 띄우는 메소드
function _showPlayTime(){
	$("#player").bind("timeupdate", function(){
		$("#playTime").text($("#player")[0].currentTime + "/" + $("#player")[0].duration);
		$("#playControl").slider("option", "value", 100  * $("#player")[0].currentTime / $("#player")[0].duration);
	});
}



//재생 슬라이더에서 받은 인풋을 기준으로 현재 재생 타임을 바꾸는 메소드
function _changePlayTime(_change){
	$("#player")[0].currentTime = _change * 0.01 * $("#player")[0].duration;
}



// 줌을 위한 메소드
function zoom() {

	if(d3.event.sourceEvent != null){
		var targNN = d3.event.sourceEvent.target.nodeName, deTP = d3.event.sourceEvent.type;
		
		if ((targNN == "circle" || targNN == "text" || targNN === "tspan") 
				&& (deTP == "dblclick" || deTP == "touchstart" || deTP == "click")) {
			//do nothing : jquery Dblclick / click과 충돌하는 문제 해결을 위해 필요.
		} 
		
		else {
			$d3Svg.attr("transform", "translate(" + d3.event.translate + ")scale("
					+ d3.event.scale + ")");
		}
	} 
	
	else{
		$d3Svg.attr("transform", "translate(" + d3.event.translate + ")scale("
				+ d3.event.scale + ")");
	}
}



// 특정 원에 포커스
function _focusOn(_circle){	
	d3.select("svg").transition().duration(1000)
		.call($d3ZoomB.
				translate([(_width/2 - parseInt($(_circle).attr("cx")) * 3) 
				       , (_height/2 - parseInt($(_circle).attr("cy")) * 3)])
				.scale([3]).event		
		);	
}



// 스케일링이 없는 포커스
function _focusWithoutZoom(_circle){
	var nowScale = $d3ZoomB.scale();

	d3.select("svg").transition().duration(1000)
	.call($d3ZoomB.
			translate([(_width/2 - parseInt($(_circle).attr("cx")) * nowScale) 
			       , (_height/2 - parseInt($(_circle).attr("cy")) * nowScale) ])
			.event		
	);	
}



// 전체화면으로 변화
function _focusOut(){	
	d3.select("svg").transition()
					.duration(1000)
					.call($d3ZoomB.translate([0,0]).scale([1]).event);	
}



//앨범 아이디를 통해 이미지 주소를 파싱하는 메소드
function _getImgSrc(albumId){
    
	   albumId = albumId.trim();
	   
	   var imgPath = "http://image.melon.co.kr/cm/album/images/";
	   var first = "";
	   var middle = "";
	   var last ="";
	   var temp = albumId;
	   
	   for(var  i = 0 ; i < 8 - albumId.length; i++ ){
	         
	      temp = "0".concat(temp);   
	   }
	   
	   first = temp.substr(0, 3);
	   
	   middle = temp.substr(3, 2);
	   
	   last = temp.substr(5, 3);
	   
	   return  imgPath + first + "/" + middle + "/" + last + "/" + albumId +  ".jpg";

};



$.fn.single_double_click = function(single_click_callback, double_click_callback, timeout) {
    return this.each(function() {
        var clicks = 0,
            self = this;
        $(this).click(function(event) {
            clicks++;
            
            if (clicks == 1) {
                setTimeout(function() {
                    
                	if (clicks == 1) {
                        single_click_callback.call(self, event);
                    } 
                    
                    else {
                        double_click_callback.call(self, event);
                    }
                    clicks = 0;
                }, timeout || 200);//end of setTimeOut
            }//end of if
        });
    });
}



//재생수 컬럼을 증가시킨다.
function _incrPlayNum(_songNum){
	$.ajax({
		url : "incrPlayNum.do",
		data : {
			listNum : collectionNum,
			songNum : _songNum
		},
		type : "POST",
		dataType : "text",
		success : function(responseData){
			//do nothing
		}//end of success
	});//end of $.ajax	
}



//webGl 을 통한 공간 형성
function _initBackGround(){
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 20000);
    scene.add(camera);
    
    
    clock = new THREE.Clock();

    
    var light = new THREE.PointLight(0xffffff);
    light.position.set(0, 250, 0);
    scene.add(light);
    
    var skyBoxGeometry = new THREE.CubeGeometry( 10000, 10000, 10000 );
    var skyBoxMaterial = new THREE.MeshBasicMaterial( { color: 0x000000, side: THREE.BackSide } );
    var skyBox = new THREE.Mesh( skyBoxGeometry, skyBoxMaterial );
     scene.add(skyBox);

    scene.fog = new THREE.FogExp2( 0x2e343e, 0.00025 );
    
    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    
    camera.position.z = 30;
    
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.autoRotate = true;
    controls.autoRotateSpeed = 0.5;
    
    engine = new ParticleEngine();
   
    engine.setValues({
        positionStyle    : Type.CUBE,
        positionBase     : new THREE.Vector3( 0, 0, 0 ),
        positionSpread   : new THREE.Vector3( 600, 400, 600),
                    
        velocityStyle    : Type.CUBE,
        velocityBase     : new THREE.Vector3( 0, 0, 0 ),
        velocitySpread   : new THREE.Vector3( 0.5, 0.5, 0.5 ), 

        angleBase               : 0,
        angleSpread             : 720,
        angleVelocityBase       : 0,
        angleVelocitySpread     : 4,
		
        particleTexture : THREE.ImageUtils.loadTexture( 'static/img/particle.png' ),
        
        sizeBase    : 2.0,
        sizeSpread  : 1.0,				
        colorBase   : new THREE.Vector3(0.15, 1.0, 0.9), // H,S,L
        colorSpread : new THREE.Vector3(0.2, 0.2, 0.2),
        opacityBase : 1,

        particlesPerSecond : 20000,
        particleDeathAge   : 50.0,		
        emitterDeathAge    : 0.1
    });    
    engine.initialize();
    setInterval(function(){initParticleEngine();}, 30000);

    
    function render(){
        requestAnimationFrame(render);
        
        var dt = clock.getDelta();
        engine.update(dt * 0.5);  
        controls.update();
        
        renderer.render(scene, camera);            
    }

    render();	
}



//파티클 엔진 초기화.
function initParticleEngine(){
	engine.destroy();
	engine = new ParticleEngine();
    engine.setValues({
        positionStyle    : Type.CUBE,
        positionBase     : new THREE.Vector3( 0, 0, 0 ),
        positionSpread   : new THREE.Vector3( 600, 400, 600),
                    
        velocityStyle    : Type.CUBE,
        velocityBase     : new THREE.Vector3( 0, 0, 0 ),
        velocitySpread   : new THREE.Vector3( 0.5, 0.5, 0.5 ), 

        angleBase               : 0,
        angleSpread             : 720,
        angleVelocityBase       : 0,
        angleVelocitySpread     : 4,
		
        particleTexture : THREE.ImageUtils.loadTexture( 'static/img/particle.png' ),
        
        sizeBase    : 2.0,
        sizeSpread  : 1.0,				
        colorBase   : new THREE.Vector3(0.15, 1.0, 0.9), // H,S,L
        colorSpread : new THREE.Vector3(0.2, 0.2, 0.2),
        opacityBase : 1,

        particlesPerSecond : 20000,
        particleDeathAge   : 60.0,		
        emitterDeathAge    : 0.1
    });    
    engine.initialize();
}