/* 
	
	1. 파일이름 : songBrowse.js
	2. 작성자 : 김시온
	3. 목적 : songBrowse.jsp에 관련된 메소드들 구현 
	4. 이력
		2014. 05. 13 페이지에 들어올때 마다 RANDOM으로 장르뽑아서 곡 가져오는 기능 구현
		2014. 05. 15 해당장르에 있는 노래들을 5개씩 잘라서 slide 구현 
		2014. 05. 18 곡추가 버튼 누르면 현재 유저가 가지고 있는 곡의 목록을 해당 영역에 뿌려주는 기능 구현			
		2014. 05. 19 해당 리스트에 선택한 곡 추가하는 기능 구현
		2014. 05. 24 filter 기능 구현
		2014. 05. 29 player 기능 구현
*/ 
function _insertSongByGenre (songList) {
		
	var jsonString = songList;
	var arraySongList = eval(jsonString);
	var index = 0;
	
	for (var i = 0; i < arraySongList.length; i++) {
		
		
		wrapper = "<div class='songList' isSelected='0'>"
							+ "<div  isSelected='0'  class='genreDIV' style=''>"
							+ "<div  isSelected='0' class='triangle'></div>"
							+ "<span  isSelected='0' class='genreTitle'>"+arraySongList[i][0].genre+"</span>"
							+ (arraySongList[i].length > 5 ? "<img src='static/img/rightBtn.png' isSelected='0' class='nextBtn btn' >" : "")
							+ (arraySongList[i].length > 5 ? "<img src='static/img/leftBtn.png' isSelected='0' class='preBtn btn' >" : "")
							+ "</div>"
							+ "<div isSelected='0' class='songWrapper index_1'>" 
							+ "</div>"
							+ "<hr  class='hr'></div> ";
		
		$(".songArea").append(wrapper);

		for(var j = 0; j < arraySongList[i].length; j++) {
			temp = "<div  isSelected='0' class='songContent _"+(arraySongList[i])[j].fileName+"' >"
						+ "<div isSelected='0' class='album_border "+(arraySongList[i])[j].num+"' style='display:table;'>"
						+ "<img  isSelected='0' class='albumImg' src = '"+getImgSrc((arraySongList[i])[j].albumId)+"' ></img></div>"
						+ "<div isSelected='0' class='albumInfo'>" 
						+ "<span class='artistInfo'  isSelected='0'>"+arraySongList[i][j].songName+"</span><br>"
						+ "<span class='titleInfo'  isSelected='0'>"+arraySongList[i][j].artistName+"</span>"
						+ "</div>"
						+ "</div>";
			$($(".songWrapper")[i]).append(temp);
				
		}
	}//end of for () : songList	
	
	_hoverAlbumBorder();
	
}//end of insertBy


//hover 됫을 때 overlay 되면서 곡 추가, 재생, 상세보기 버튼 보이는 메소드
function _hoverAlbumBorder () {
	$(".album_border").hover(function(){
		
		var _streamServer = "http://seiren.gonetis.com:8081/";
		var songClass = $(this).parent().attr("class");
		var splitClass = songClass.split("_");
		var _fileName = splitClass[1];
		var sSrc = _streamServer + _fileName;
		
		var sImageUrl = "static/img/play-Icon.png";
		var sId = "play";
		
		if (sSrc == $("#player").attr("src") && $("#player")[0].paused == false) {
			sImageUrl = "static/img/pause.png";
			sId = "pause";
		}
		
		$(this).prepend("<div class='overlayRect'></div>");
		$(this).prepend("<img class='overlayAdd' id='more' src='static/img/more.png'></img>");
		$(this).prepend("<img class='overlayAdd _plus' src='static/img/Add.png' id='plus'></img>");
		$(this).prepend("<img class='overlayAdd' src='"+sImageUrl+"' onclick='_playSong(this)' id='"+sId+"'></img>");

		$(this).children("._plus").click(function () {
			var _target = this;
			_showAddSongArea(_target, event);
		});
	
	}, function(){
		$(this).children(".overlayAdd").remove();
		$(this).children(".overlayRect").remove();
	});
}


//해당 장르에 속해있는 곡들을 slide로 보여주기 위한 버튼 - pre버튼 클릭시 발생하는 이벤트
function _showPreSong(btn) {
	var sClassName = $(btn).parent().siblings(".songWrapper")[0].className;
	var nIndex = parseInt(sClassName.split("_")[1]);
	
	if( nIndex != 1 ) {
		$(btn).parent().siblings(".songWrapper").removeClass("index_" + nIndex);
		nIndex--;
		$(btn).parent().siblings(".songWrapper").css("transform","translateX("+ ((nIndex - 1) * -760) +"px)");
		$(btn).parent().siblings(".songWrapper").addClass("index_" + nIndex);
	}
}


//이벤트 리스너 붙이는 메소드
function _bindEventListener() {
	
	 $('.preBtn').click(function() {
		 var btn = this;
		 _showPreSong(btn);
	 });//앨범 slide 왼쪽으로 넘어가는 버튼

	 
	$('.nextBtn').click(function() {
		var btn = this;
		_showNextSong(btn);
	});// 앨범 slide 오른쪽으로 넘어가는 버튼
	
	$(".box").click (function() {
		var searchBox = this;
		_clickSearchBox(searchBox, event);
	});//serarch box 선택시 발생하는 이벤트
	
	$(".unifiedSearch").click(function (event) {
		var searchBox = this;
		_clickuUnifiedSearchBox(searchBox, event);
	});//통합 검색 창
	
	
	//통합검색창 엔터했을 때 실행하는 메소드
	$(".unifiedSearch").keydown(function (event) {
		if (event.keyCode == 13){ //엔터키
	      fncUnifiedSearch();
	      return false;
	   }
	});
}

//해당 장르에 속해있는 곡들을 slide로 보여주기 위한 버튼 - next버튼 클릭시 발생하는 이벤트
function _showNextSong(btn) {
	var sClassName = $(btn).parent().siblings(".songWrapper")[0].className;
	var nIndex = parseInt(sClassName.split("_")[1]);
	var nChild = $(btn).parent().siblings(".songWrapper").children().length;
	nChild = Math.ceil(nChild/5);

	if (nIndex != (nChild)) {
	
		$(btn).parent().siblings(".songWrapper").css("transform","translateX("+ (nIndex * -760) +"px)");
		$(btn).parent().siblings(".songWrapper").removeClass("index_" + nIndex);
		nIndex++;
		$(btn).parent().siblings(".songWrapper").addClass("index_" + nIndex);

	}
}


//+ 버튼 누를 때 곡을 추가할 수 있게 하는 영역 보여주는 메소드
function _showAddSongArea(_target, event) {

	var myLists;
	var sListsName = [];	
	var imgClass = $(_target).parent().attr("class");
	var splitClass = imgClass.split(" ");
	var num = parseInt(splitClass[1]);
	
	$.ajax({
		url : "myCollLists.do",
		type : "get",
		dataType : "json",
		success : function(responseData){
			
			if (responseData.isSuccess) {
				 
				myLists =  responseData.myLists;
				myLists = eval(myLists);
				
				
				for (var i = 0; i < myLists.length; i ++ ) {
					sListsName += "<div id='"+myLists[i].num+"' class='nameArea' style='display:table;'>"
										+ "<img class='music_img' src='static/img/music.png'>"
										+ "<span style='display:table-cell;margin-left:30px;position:absolute;'>" 
										+ myLists[i].name + "</span></div><br>";
				}//로그인 한 유저의 리스트를 배열로 저장
				
				
				var sAddSongArea = "<div isSelected='1' class='addSongArea'>"
											+ "<div class='showMyList plus'>"
											+ "<div style='display:table-cell;position:relative;width:200px;height:150px;'>"
											+ "<div style='text-align: center;'><span style='font-size: 17px;font-weight: bold;'>Add to...</span></div><hr style='margin-top:7px;margin-bottom:7px;'>"
											+ "<div class='newListArea plus' style='position:relative;' >" 
											+ "<span class='glyphicon glyphicon-plus next' style='padding-right:10px;padding-left:10px;'></span>New Lists</div>" 
											+ "<div class='nameWrapper' style='position:relative;padding-left:5px;'></div>" 
											+ "</div>"//table-cell1 끝
											+ "<div style='cursor:pointer;display:table-cell;position:relative;width:200px;height:150px;color:white;'>"
											+ "<span class='glyphicon glyphicon-chevron-left next' style='padding-right:10px;padding-left:10px;top:5px;'></span>"
											+ "<div style='display:inline; margin-left:20px;position:relative;z-index:201;'>Make New Lists</div><hr style='margin-top:9px;margin-bottom:7px;>"
											+ "<form isSelected='1' ><input isSelected='1'  type='text' class='listName'></input><br>" 
											+ "<input isSelected='1'  type='button' value='CREATE' class='submitBtn'></input></form>"
											+ "</div>"//table-cell2 끝
											+ "</div>"//index_1끝	
											+ "</div>";//전체 끝;
			
				
				$(_target).before(sAddSongArea);
				$(".nameWrapper").append(sListsName);
				$('.addSongArea').show();
				$('.addSongArea').attr("tabindex", -1).focus();
				
				$(document).on("click", function(){
					if($(document).find(":focus").is(".addSongArea") || $(document).find(":focus").is("input.listName")){
						//alert("still")
					}
					else{
						$(document).off("click");
						$(".addSongArea").remove();
					}
				});
				
 				$('.newListArea').click(function(){
					var newListBtn = this;
					_showNewListArea(newListBtn, num);
				}); //list 추가 slideShow
				
				$('.next').click(function(){
					$(this).parent().parent().css("transform","translateX("+ (+0) +"px)");
				});// 곡 추가 slideShow
			
				$('.nameArea').click( function (event) {
					var song = this;
					_addSong(song, num);
				});
				
			}
		}//end of success
	},false)	
	.fail(function(){
		alert("리스트 불러오기에 실패했습니다");	
	})
	.error(function(){
		alert("리스트 불러오기에 실패했습니다");
	});
	
}

function _showNewListArea(newListBtn, num) {
	$(newListBtn).parent().parent().css("transform","translateX("+ (-220) +"px)");
	
	$('.submitBtn').one('click', function (event) {
		var btn = this;
		_makeList(btn, num);
	});
}


//해당 나의 리스트에 선택한 곡 추가하가는 메소드
function _addSong(song, num) {
	
	var collNum = $(song).attr("id");
	var songNum = num;

	$.ajax({
	   data : {songNum : songNum,
		   		  listNum : collNum
			},
		url : "addSong.do",
		type : "post",
		dataType : "json",
		success : function(responseData){
			alert("곡이 추가 되었습니다.");
			$('.addSongArea').hide();
			$('.addSongArea').remove();
			
		}//end of success
	})	
	
	.fail(function(){
		alert("곡 추가에 실패했습니다");	
	})
	.error(function(){
		alert("곡 추가에 실패했습니다");
	});
}  


//리스트 생성 동시에 생성한 리스트에 선택한 곡 추가하는 메소드
function _makeList (_target, num) {
	console.log(_target);
	var collName = $(_target).siblings('.listName').val();
	var songNum = num;

	if (collName == " " || collName.length == 0) {
		alert("리스트 이름을 입력해주세요");
	} else {
		
		$.ajax({
		   data : {songNum : songNum,
			   		  listName : collName
					 },
			url : "makeList.do",
			type : "get",
			dataType : "json",
			success : function(responseData){
				if(responseData.isSuccess) {
					alert("리스트가 생성되었습니다.");
				}
				
				$('.makeNewListArea').remove();
				$('.addSongArea').remove();
			}//end of success
		})	
		.fail(function(){
			alert("곡 추가에 실패했습니다");	
		})
		.error(function(){
			alert("곡 추가에 실패했습니다");
		});
	}
}


function _clickSearchBox(searchBox, event) {
	
	$.ajax({
		url : "selectAllSongList.do",
		type : "get",
		dataType : "json",
		success : function(responseData){
			
			if(responseData.isSuccess) {
				var lAllSongList = responseData.selectAllSongList;
				songList = eval(lAllSongList);
				console.log(songList);
				var jsonSongList ={};
				
				for (var i=0; i < songList.length; i++) {
					var songNum = songList[i].num;
					var songName = songList[i].songName;
					var artistName = songList[i].artistName;
					jsonSongList[songNum] = artistName.concat(" "+ " " +songName);
				};				 	
					
				var songArray = $.map(jsonSongList, function (value, key) { return { value: value, data: key }; });

				// Setup jQuery ajax mock:
				$.mockjax({
					url: '/test',
				    responseTime: 2000,
				    proxyType:'GET',
				    global: false,
				    response: function (settings) {
				    	var query = settings.data.query,
				    	queryLowerCase = query.toLowerCase(),
				        re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi'),
				        suggestions = $.grep(songArray, function (songs) {
				        	// return country.value.toLowerCase().indexOf(queryLowerCase) === 0;
				            return re.test(songs.value);
				        }),
			        response = {
			    		query: query,
			            suggestions: suggestions
			        };
			            this.responseText = JSON.stringify(response);
			        }
			    });
				 // Initialize autocomplete with local lookup:
				 $('.box').autocomplete({
			        lookup: songArray,
			        minChars: 1,
			        onSelect: function(suggestion) {
			        	return false;
			        }
				 });
			}
		}//end of success
	})	
	.fail(function(){
	})
	.error(function(){
	});
	
	event.preventDefault();
	
}


function fncEvtSearch(event) {
	if (event.keyCode == 13){ //엔터키
      fncSearch();
      return false;
   }
}

//song검색 창
function fncSearch() {
	
	$(".box").unbind("click");
	
	var values= $(".box").val().split(" "+ " ");
	var keyword1 = values[0];
	var keyword2 = values[1];

	if ( keyword2 == null || keyword2 == " ") {
		keyword2 = "noValues";
	}
	
	$.ajax({
	   data : {
		   keyword1 : keyword1,
		   keyword2 : keyword2
		},
		url : "searchKeyword.do",
		type : "post",
		dataType : "json",
		success : function(responseData){
			
			if(responseData.isSuccess) {
				var result = eval(responseData.resultList);
				var sResultList;
				
				var temp = "<div class='temp' style='left:33px;top:88px;width:799px;height:1000px;max-width:799px;max-height:1000px;margin-right:20px;position:absolute;'></div>";
				
				$(".right_content").html(temp);
				
				$(".right_content").prepend("<div style='left:33px;position:absolute;top:28px;'>" 
						+ "<div style='top:24px;' isSelected='0' class='triangle'></div>" 
						+ "<h2 style='margin-left: 23px;'>Result</h2></div>");

				
				for ( var i = 0; i < result.length; i++) {
					sResultList  = "<div isSelected='0' class='songContent _"+result[i].fileName+"'>"
										+ "<div isSelected='1' class='album_border "+result[i].num+"'>"
										+ "<img  isSelected='0' class='albumImg' src = '"+getImgSrc(result[i].albumId)+"' ></img></div>"
										+ "<div isSelected='0' class='albumInfo'>" 
										+ "<span class='artistInfo'  isSelected='0'>"+result[i].songName+"</span><br>"
										+ "<span class='titleInfo'  isSelected='0'>"+result[i].artistName+"</span>"
										+ "</div>"
										+ "</div>";	
					
					if ( i%5 == 0) {
						console.log(i%5);
						$(".temp").append("<br>");
					}
					
					$(".temp").append(sResultList);
				
				}//end of for 
				
				_hoverAlbumBorder ();
				
				 $('.box ').val('');
			
			}// end of if
			
			else {
				alert("검색결과가 존재하지 않습니다.");	
			}
		}//end of success
	})	
	.fail(function(){
		alert("검색결과가 존재하지 않습니다.");	
	})
	.error(function(){
		alert("검색결과가 존재하지 않습니다.");	
	});
	
	return false;
}


//filter에있는 checkbox value들 받음
function _submitFilterCB(event) {
	
	var genres = [];
	var years = [];
	var NTL = [];
	var sex  = [];
	
	$("input[name=genre]:checked").each(function() { 
		if (genres) genres += $(this).val()+"_";else genres = $(this).val();
	});//genre checkbox value추출
	
	$("input[name=year]:checked").each(function() { 
		 if (years) years+= $(this).val()+"_";else years = $(this).val();
	});//year checkbox value추출
	
	$("input[name=NTL]:checked").each(function() { 
		 if (NTL) NTL += $(this).val()+"_";else NTL = $(this).val();
	});//artist checkbox value추출
	
	$("input[name=sex]:checked").each(function() { 
		 if (sex) sex += $(this).val()+"_";else sex = $(this).val();
	});//artist checkbox value추출
	
	if(genres.length== 0 ) {
		genres = "noResult";
	} 
	if(years.length== 0 ) {
		years = "noResult";
	}
	if (NTL.length== 0 ) {
		NTL = "noResult";
	}
	if (sex.length== 0 ) {
		sex = "noResult";
	}
	
	$.ajax({
	   data : {
		   genres : genres,
		   years : years,
		   NTL : NTL,
		   sex : sex
		},
		url : "submitFilterCB.do",
		type : "get",
		dataType : "json",
		success : function(responseData){
			
			if(responseData.isSuccess) {
				var result = eval(responseData.resultList);
				var sResultList;
				
				var temp = "<div class='temp' style='left:33px;top:88px;width:799px;height:1000px;max-width:799px;max-height:1000px;margin-right:20px;position:absolute;'></div>";
				
				$(".right_content").html(temp);
				
				$(".right_content").prepend("<div style='left:33px;position:absolute;top:28px;'>" 
						+ "<div style='top:24px;' isSelected='0' class='triangle'></div>" 
						+ "<h2 style='margin-left: 23px;'>Result</h2></div>");

				
				for ( var i = 0; i < result.length; i++) {
					sResultList  = "<div isSelected='0' class='songContent _"+result[i].fileName+"'>"
										+ "<div isSelected='1' class='album_border "+result[i].num+"'>"
										+ "<img isSelected='0' class='albumImg' src = '"+getImgSrc(result[i].albumId)+"' ></img></div>"
										+ "<div isSelected='0' class='albumInfo'>" 
										+ "<span class='artistInfo'  isSelected='0'>"+result[i].songName+"</span><br>"
										+ "<span class='titleInfo'  isSelected='0'>"+result[i].artistName+"</span>"
										+ "</div>"
										+ "</div>";	
					
					if ( i%5 == 0) {
						console.log(i%5);
						$(".temp").append("<br>");
					}
					
					$(".temp").append(sResultList);
				
				}//end of for 
				
				_hoverAlbumBorder ();
			}// end of if
			
			else {
				alert("검색결과가 존재하지 않습니다.");	
			}
		}//end of success
	})	
	.fail(function(){
		alert("검색결과가 존재하지 않습니다.");	
	})
	.error(function(){
		alert("검색결과가 존재하지 않습니다.");	
	});
	
	return false;
	
}


//play 메소드
function _playSong(song) {
	
	var _streamServer = "http://seiren.gonetis.com:8081/";
	
	var songClass = $(song).parent().parent().attr("class");
	var splitClass = songClass.split("_");
	var _fileName = splitClass[1];
	
	
	if ($("#player")[0].paused == false) {
		if($("#player").attr("src") == _streamServer + _fileName ){
			$(song).attr("src", "static/img/play-Icon.png");
			$(song).attr("id", "play");
			$("#player")[0].pause();
		} 
		
		else {
			$(song).attr("src", "static/img/pause.png");
			$(song).attr("id", "pause");
			$("#player").attr("src", _streamServer + _fileName);
		}
	} 
	
	else {
		
		if($("#player").attr("src") == _streamServer + _fileName ){
			$("#player")[0].play();
		} 
		
		else {
			$("#player").attr("src", _streamServer + _fileName);
		}
		
		$(song).attr("src", "static/img/pause.png");
		$(song).attr("id", "pause");
	}
}


function _clickuUnifiedSearchBox(searchBox, event) {
	
	$.ajax({
		url : "autounifiedSearchBox.do",
		data : { 
		      search : null, 
		}, 
		type : "get",
		dataType : "json",
		success : function(responseData){ 
			
			if(responseData.isSuccess) {
				var lAllSongList = responseData.selectAllSongList;
				var lAllSharedList = responseData.allSharedList;
				
				songList = eval(lAllSongList);
				sharedLists = eval(lAllSharedList);
				
				var jsonSongAndShared ={};
				
				for (var i=0; i < songList.length; i++) {
					var songNum = songList[i].num;
					var songName = songList[i].songName;
					var artistName = songList[i].artistName;
					jsonSongAndShared[songNum] = artistName.concat(" "+ " " +songName);
				};	
				
				/*for(var i = 0; i < sharedLists.length; i++){ 
                    
                    var listsName = sharedLists[i].name; 
                    var listsDescription = sharedLists[i].description; 
                      
                    jsonSongAndShared[i] = listsName; 
                } */
				
					
				var songAndShared = $.map(jsonSongAndShared, function (value, key) { return { value: value, data: key }; });
	
				// Setup jQuery ajax mock:
				$.mockjax({
					url: '/test',
				    responseTime: 2000,
				    proxyType:'GET',
				    global: false,
				    response: function (settings) {
				    	var query = settings.data.query,
				    	queryLowerCase = query.toLowerCase(),
				        re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi'),
				        suggestions = $.grep(songAndShared, function (songShared) {
				        	// return country.value.toLowerCase().indexOf(queryLowerCase) === 0;
				            return re.test(songShared.value);
				        }),
			        response = {
			    		query: query,
			            suggestions: suggestions
			        };
			            this.responseText = JSON.stringify(response);
			        }
			    });
				 // Initialize autocomplete with local lookup:
				 $('.unifiedSearch').autocomplete({
			        lookup: songAndShared,
			        minChars: 1,
			        onSelect: function(suggestion) {
			        	return false;
			        }
				 });
			}
		}//end of success
	})	
	.fail(function(){
	})
	.error(function(){
	});
	
	event.preventDefault();
}


//통합 검색 창에서 받은  value들을 받아서 DB에 전달해주는 메소드
function fncUnifiedSearch () {

	$(".unifiedSearch").unbind("click");
	
	var values= $(".unifiedSearch").val().split(" "+ " ");
	var keyword1 = values[0];
	var keyword2 = values[1];

	
	if ( keyword2 == null || keyword2 == " ") {
		keyword2 = "noValues";
	}
	
	window.location.href = "unifiedSearchPage.do?keyword1="+keyword1+"&&keyword2="+keyword2+"";
}