var MyList = jindo.$Class ({
	_oElement : null,
	_oEvent : null,
	
	$init : function () {
		jindo.$Fn(this._initialize, this).attach(window, 'load');
		jindo.$Fn(this._destroy, this).attach(window, 'unload');
	},
	
	
	
	_initialize : function () {
		this._oElement = {};
		this._oEvent = {};
		
		this._setElement();
		this._setEvent();
	},
	
	
	
	_destroy : function () {
		
	},
	
	
	_setElement : function () {
		this._oElement['headerArea'] 			= jindo.$('header');
		this._oElement['content'] 				= jindo.$('content');
		this._oElement['logOut'] 				= jindo.$('logout');

		this._oElement['rolling']				= new jindo.CircularRolling(jindo.$('rolling'), {
														   nFPS : 50, // (Number) 초당 롤링 이동을 표현할 프레임수
														   nDuration : 400, // (ms) jindo.Effect, jindo.Transtition 참고
														   sDirection : 'horizontal', // || 'vertical'
														   fEffect : jindo.Effect.linear, //jindo.Effect 참고
														});
		this._oElement['prevBttn']				= jindo.$('prev');
		this._oElement['nextBttn']				= jindo.$('next');
		
		this._oElement['myList']				= jindo.$$('.myList');
		this._oElement['createListArea'] 		= jindo.$('createList');
		this._oElement['foggy'] 				= jindo.$('foggy');
		this._oElement['inputColletionLayer'] 	= jindo.$('inputColletionLayer');
		this._oElement['confirmCollectionName'] = jindo.$('confirmCollectionName');
		
	},
	
	
	_setEvent : function () {
		this._oEvent['createListArea'] 	 = jindo.$Fn(this._onClickCreateList, this).attach(this._oElement['createListArea'], 'click');
		this._oEvent['clickFoggy'] 		 = jindo.$Fn(this._onClickFoggy, this).attach(this._oElement['foggy'], 'click');
		this._oEvent['clickSubmitName']  = jindo.$Fn(this._onClickSubmitName, this).attach(this._oElement['confirmCollectionName'], 'click');
		this._oEvent['clickPrevBttn']	= jindo.$Fn(this._onClickPrevBttn, this).attach(this._oElement['prevBttn'], 'click');
		this._oEvent['clickNextBttn']	= jindo.$Fn(this._onClickNextBttn, this).attach(this._oElement['nextBttn'], 'click');
		this._oEvent['myList']			= jindo.$Fn(this._onClickmyList, this).attach(this._oElement['myList'], 'click');
		this._oEvent['myList']			= jindo.$Fn(this._overMyList, this).attach(this._oElement['myList'], 'mouseenter');
		this._oEvent['myList']			= jindo.$Fn(this._offMyList, this).attach(this._oElement['myList'], 'mouseleave');
	},
	
	_overMyList : function(oEvent){
		$(oEvent.currentElement).first().css({
			background : "black"		
		}, 500);
		
		$(oEvent.currentElement).children($("svg")).css({
			opacity : "0.2"		
		}, 500);
		
		$(oEvent.currentElement).first().prepend("<div class='overlayBttn'><span><img id='playBttn' src='static/img/play-Icon.png'><span></img></div>")
		
		
		$(".overlayBttn").css("line-height", function(){
			return (screen.height - 250) * 0.7 + "px";
		});
		
		
		$("#playBttn").click(function(){
			var listNum = $(this).parent().parent().parent().attr("listNum");

			$.ajax({
				url : "validateThirtySong.do",
				data : {
					listNum : listNum
				},
				type : "GET",
				dataType : "text",
				success : function(responseData){
					var response = eval("("+responseData+")");
					
					if(response.result == "success"){
						var newWindow = window.open("enterList.do?listNum="+ listNum);
						newWindow.resizeTo(screen.width, screen.height);
						newWindow.moveTo(0, 0);
					}else{
						alert("해당 리스트에 곡이 하나도 들어있지 않습니다. \n 곡 수정 페이지로 이동합니다.")
						location.href = "modifyList.do?listNum="+listNum;
					}				
				}//end of success
			});//end of $.ajax
		});
	},
	
	_offMyList : function(oEvent){
		$(oEvent.currentElement).first().css({
			background : "none"		
		}, 500);
		
		$(oEvent.currentElement).children($("svg")).css({
			opacity : "1"		
		});
		
		$(".overlayBttn").remove();
	},
	
	_onClickmyList : function(oEvent){
		
		var listNum = oEvent.currentElement.firstChild.getAttribute("listNum");
			
	},
	
	
	_onClickPrevBttn : function(oEvent){
		this._oElement['rolling'].moveBy(-1);
		return false;
	},
	
	_onClickNextBttn : function(oEvent){
		this._oElement['rolling'].moveBy(1);
		return false;		
	},
	
	
	_onClickLogout : function(oEvent) {
		location.href = "logout.do";
	},
	
	
	
	_onClickCreateList : function(oEvent) {
		 var oLayerEffect  = new jindo.LayerEffect(jindo.$Element(this._oElement['foggy']));
		 var oLayerEffect2 = new jindo.LayerEffect(jindo.$Element(this._oElement['inputColletionLayer']));

		 oLayerEffect.fadeIn();
		 oLayerEffect2.fadeIn();
		 		
		 oEvent.stop();
	},
	
	
	
	_onClickFoggy : function(oEvent) {
		 var oLayerEffect  = new jindo.LayerEffect(jindo.$Element(this._oElement['foggy']));
		 var oLayerEffect2 = new jindo.LayerEffect(jindo.$Element(this._oElement['inputColletionLayer']));

		 oLayerEffect.fadeOut();
		 oLayerEffect2.fadeOut();
		 
		 oEvent.stop();
	},	
	
	_onClickSubmitName : function(oEvent) {
		
		var inputValue = jindo.$('inputValue').value;
		var stringByteLength = ( function (s,b,i,c) {
			for(b=i=0;c=s.charCodeAt(i++);b+=c>>11?3:c>>7?2:1);
			return b;
			}) (inputValue);
		
		if (stringByteLength < 28 && stringByteLength != 0) {
			
			location.href = "createCollectionService.do?inputValue="+inputValue;
		
		} else if (stringByteLength == 0){
		
			alert("리스트 이름이 빈칸입니다")
			
		} else {
			
			alert("리스트는 한글 9자, 영문 27자 미만으로 작성해 주세요")
			
		}
	}
});