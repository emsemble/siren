var ModifyUser = $Class({
	_oElement : null,
	_oTemplate : null,
	_oEvent : null,
	_oAjaxUrl : {
		confirmPresentPasswordUrl : '/seiren/presentPasswordConfirm.do',
		updateNewPasswordUrl : '/seiren/updateNewPassword.do',
		deleteUserUrl : '/seiren/deleteUser.do',
		updateNewNameUrl : '/seiren/updateNewName.do'
	},
	
	
	$init : function() {
		$Fn(this._initialize, this).attach(window, 'load');
		$Fn(this._destroy, this).attach(window, 'unload');
	},
	
	
	_initialize : function() {
		this._oElement = {};
		this._oTemplate = {};
		this._oEvent = {};
		
		this._setElement();
		this._setEvent();
	},
	
	
	_destroy : function() {
		
	},
	
	
	_setElement : function() {
		this._oElement['headerArea'] = $('header');
		this._oElement['contentArea'] = $('contents');
		this._oElement['footerArea'] = $('footer');
		this._oElement['deleteUser'] = $('deleteBtn');
		this._oElement['userPasswordArea'] = $('userPasswordArea');
		this._oElement['userPasswordInput'] = $('userPasswordInput');
		this._oElement['newPassword'] = $('newPassword');
		this._oElement['userNameArea'] = $('userNameArea');
		this._oElement['userNameInputArea'] = $('userNameInputArea');
		this._oElement['presentUserName'] = $('presentUserName');
		this._oElement['foggy'] = $('foggy');
		this._oElement['deleteUserLayer'] = $('deleteUserLayer');
		this._oElement['closeDeleteUserLayerBtn'] = $('closeDeleteUserLayerBtn');
		this._oElement['inputUserPasswordArea'] = $('inputUserPasswordArea');
		this._oElement['inputUserNameArea'] = $('inputUserNameArea');
		this._oElement['acceptDeleteUserLayerBtn'] = $('acceptDeleteUserLayerBtn');
		
		this._oTemplate['modifyPasswordResult'] = $Template('tpl_modify_password_result');
		this._oTemplate['modifyNameResult'] = $Template('tpl_modify_name_result');
	},
	
	
	_setEvent : function() {
		//this._oEvent['moseoverProfile'] = $Fn(this._onMouseoverProfile, this).attach(this._oElement['profile'], 'mouseover'); 
		this._oEvent['deleteUser'] = $Fn(this._onDeleteUserButton, this).attach(this._oElement['deleteUser'], 'click');
		this._oEvent['clickUserPasswordArea'] = $Fn(this._onClickUserPasswordArea, this).attach(this._oElement['userPasswordArea'], 'click');
		this._oEvent['clickUserNameArea'] = $Fn(this._onClickUserNameArea, this).attach(this._oElement['userNameArea'], 'click');
		this._oEvent['hideDeleteUserLayerOfBtn'] = $Fn(this._hideDeleteUserLayer, this).attach(this._oElement['closeDeleteUserLayerBtn'], 'click');
		this._oEvent['hideDeleteUserLayerOfFoggy'] = $Fn(this._hideDeleteUserLayer, this).attach(this._oElement['foggy'], 'click');
		this._oEvent['clickAcceptDeleteUserLayerBtn']= $Fn(this._onClickAcceptDeleteUserLayerBtn, this).attach(this._oElement['acceptDeleteUserLayerBtn'], 'click');
		this._oEvent['inputNewPassword'] = $Fn(this._inputNewPassword, this).attach(this._oElement['newPassword'], 'change');
	},
	
	
	_inputNewPassword : function (oEvent) {
		var newPassword = $('newPassword').value;
		
		var newPasswordChecknum = newPassword.search(/[0-9]/); 
		var newPasswordCheckeng = newPassword.search(/[a-zA-Z]/);
			
		if( newPassword.match(/^[a-zA-Z0-9]{6,12}$/) && newPasswordChecknum > -1 && newPasswordCheckeng > -1 ){
			$Element('nextNewPasswordConfirmBase').html("비밀번호를 사용할 수 있습니다.");
			$('checkNewPassword').value = 1;
			
		} else {
			$Element('nextNewPasswordConfirmBase').html("사용가능한 비밀번호가 아닙니다.");
			$('checkNewPassword').value = 0;
			
		}
		
	},
	
	_onClickUserPasswordArea : function(oEvent) {
		var newPassword = $('newPassword').value;
		var newPasswordConfirm = $('newPasswordConfirm').value;

		var checkPresentPassword = $('checkPresentPassword').value;
		var checkNewPassword = $('checkNewPassword').value;
		
		var el = oEvent.element;
		
		if (el.tagName.toLowerCase() != 'input') {
			oEvent.stop();
			return;
		}
		
		var sId = $Element(el).attr("id");
		
		if (sId == "modifyUserPw") {
			$Element(this._oElement['userPasswordInput']).show();
			$Element(this._oElement['inputUserPasswordArea']).hide();
			
		} else if (sId == "cancelPassword") {
			$Element(this._oElement['inputUserPasswordArea']).show();
			$Element(this._oElement['userPasswordInput']).hide();
			
			$('newPassword').value = "";
			$('presentPassword').value = "";
			$('newPasswordConfirm').value = "";
			
			$Element('nextPresentPassword').html("");
			$Element('nextNewPasswordConfirmBase').html("");
			$Element('nextNewPasswordConfirm').html("");

		
		} else if (sId == "savePassword") {
			if(checkPresentPassword == 1 && checkNewPassword == 1){	
				if(newPassword == newPasswordConfirm) {
					var oAjaxParam = {
						"newPassword" : newPassword
					};
					
					var oAjaxOption = {
						timeout : 5,
						method : 'post',
						onload : $Fn(this._onUpdateNewPassword, this),
						ontimeout : function(oResponse) {
							$Element('nextNewPasswordConfirm').html("오류가 발생했습니다." );
						},
						onerror : function(oResponse) {
							$Element('nextNewPasswordConfirm').html("오류가 발생했습니다." );
						}
					};
					
					var sUrl = this._oAjaxUrl.updateNewPasswordUrl;
					var oAjax = new $Ajax(sUrl, oAjaxOption);
					oAjax.request(oAjaxParam);
					
					$Element(this._oElement['inputUserPasswordArea']).show();
					$Element(this._oElement['userPasswordInput']).hide();
					
					$('newPassword').value = "";
					$('presentPassword').value = "";
					$('newPasswordConfirm').value = "";
					
					$Element('nextPresentPassword').html("");
					$Element('nextNewPasswordConfirmBase').html("");
					$Element('nextNewPasswordConfirm').html("");
					
				} else {
					$Element('nextNewPasswordConfirm').css("color", "red");
					$Element('nextNewPasswordConfirm').html("비밀번호가 서로 불일치합니다." );
				}
				
			} else if(checkPresentPassword == 0){
				alert('현재 비밀번호와 일치하지 않습니다');
				
			} else if(checkNewPassword == 0){
				alert('사용 가능한 비밀번호가 아닙니다.');
			}
		}
		oEvent.stop();
	},
	
	
	_onClickUserNameArea : function (oEvent) {
		
		var el = oEvent.element;
		if (el.tagName.toLowerCase() != 'input') {
			oEvent.stop();
			return;
		}
		
		var sId = $Element(el).attr("id");
		if (sId == "modifyUserName") {
			$Element(this._oElement['presentUserName']).hide();
			$Element(this._oElement['userNameInputArea']).show();
			
		} else if (sId == "cancelName") {
			$Element(this._oElement['userNameInputArea']).hide();
			$Element(this._oElement['presentUserName']).show();
			
		} else if (sId == "saveName") {
			var newNameValue = $('newName').value;
			var stringByteLength = ( function (s,b,i,c) {
				for(b=i=0;c=s.charCodeAt(i++);b+=c>>11?3:c>>7?2:1);
				return b;
				}) (newNameValue);
			
			if (stringByteLength > 18) {
				$Element('nameCheck').html("이름이 너무 깁니다.");
				
				alert("이름은 한글 6자, 영문 18자 이내로 작성해주세요");
				
			} else {
				$Element('nameCheck').html("");
				
				var oAjaxParam = {
						"newName" : newNameValue
					};

					var oAjaxOption = {
						timeout : 5,
						method : 'post',
						onload : $Fn(this._onUpdateNewName, this),
						ontimeout : function(oResponse) {
							alert("오류가 생겼습니다.");
						},
						onerror : function(oResponse) {
							alert("오류가 생겼습니다.");
						}
						
					};
					
					var sUrl = this._oAjaxUrl.updateNewNameUrl;
					var oAjax = new $Ajax(sUrl, oAjaxOption);
					oAjax.request(oAjaxParam);
					
					$Element(this._oElement['presentUserName']).show();
					$Element(this._oElement['userNameInputArea']).hide();
					oEvent.stop();

			}
		}
	},
	
	
	_onBlurPresentPassword : function() {
		var oAjaxParam = {
				"presentPassword" : $('presentPassword').value
			};

			var oAjaxOption = {
				timeout : 5,
				method : 'post',
				onload : $Fn(this._onConfirmPresentPassword, this),
				ontimeout : function(oResponse) {
					$Element('nextNewName').html("오류가 발생했습니다.");
				},
				onerror : function(oResponse) {
					$Element('nextNewName').html("오류가 발생했습니다.");
				}
			};

			var sUrl = this._oAjaxUrl.confirmPresentPasswordUrl;
			var oAjax = new $Ajax(sUrl, oAjaxOption);
			oAjax.request(oAjaxParam);
	},
	
	
	_onUpdateNewPassword : function(oResponse){
		var oJson = oResponse.json();
		if (oJson){
			if (oJson.isSuccess) {
				$Element(this._oElement['inputUserPasswordArea']).html(this._oTemplate['modifyPasswordResult'].process());
			}else {
				var sErrorMsg = oJson.errorMsg || "오류가 생겼습니다.";
				$Element('nextNewPasswordConfirm').html(sErrorMsg);
			}
		}
	},
	
	
	_onUpdateNewName : function(oResponse) {
		var oJson = oResponse.json();
		if (oJson){
			if (oJson.isSuccess) {
				$Element(this._oElement['presentUserName']).html(this._oTemplate['modifyNameResult'].process({
					name : oJson.newName
				}));
			}else {
				var sErrorMsg = oJson.errorMsg || "오류가 생겼습니다.";
				$Element('nextNewName').css("color","red");
				$Element('nextNewName').html(sErrorMsg);
			}
		}
	},
	
	_onConfirmPresentPassword : function(oResponse) {
		var oJson = oResponse.json();
		if (oJson) {
			if (oJson.isSuccess) {
				$Element('nextPresentPassword').css("color","blue");
				$Element('nextPresentPassword').html(oJson.successMsg);
				$('checkPresentPassword').value = 1;
			} else {
				var sErrorMsg = oJson.errorMsg || "오류가 생겼습니다.";
				$Element('nextPresentPassword').html(sErrorMsg);
				$('checkPresentPassword').value = 0;
			}
		}
	},
	
	/*_onMouseoverProfile : function(oEvent) {
		var profileLayer = new jindo.LayerManager('profileLayer', {
			sCheckEvent : "mouseover", 
			nHideDelay : 200
		}).show(); return false;
		
		oEvent.stop();
	},*/
	
	_onDeleteUserButton : function(oEvent) {
		var wElFoggy = $Element(this._oElement['foggy']);
		var wElDeleteUserLayer = $Element(this._oElement['deleteUserLayer']);

		wElFoggy.show();
		
		var oClientSize = $Document().clientSize();
		var nWidth = oClientSize.width;
		var nHeight = oClientSize.height;
		
		wElDeleteUserLayer.css("top", nHeight / 2 - 150);
		wElDeleteUserLayer.css("left", nWidth / 2 - 150);
		wElDeleteUserLayer.show();
		
		oEvent.stop();
	},
	
	
	_hideDeleteUserLayer : function(oEvent) {
		var wElFoggy = $Element(this._oElement['foggy']);
		var wElDeleteUserLayer = $Element(this._oElement['deleteUserLayer']);
		wElFoggy.hide();
		wElDeleteUserLayer.hide();
		
		oEvent.stop();
	},
	
	
	_onClickAcceptDeleteUserLayerBtn : function(oEvent) {	
		var oAjaxOption = {
				timeout : 5,
				method : 'post',
				onload : $Fn(this._onDeleteUser, this),
				ontimeout : function(oResponse) {
					alert("오류가 생겼습니다.");
				},
				onerror : function(oResponse) {
					alert("오류가 생겼습니다.");
				}
			};
			
		var sUrl = this._oAjaxUrl.deleteUserUrl;
		var oAjax = new $Ajax(sUrl, oAjaxOption);
		oAjax.request();
			
		oEvent.stop();
	},
	
	
	_onDeleteUser : function (oResponse) {
		var oJson = oResponse.json();
		
		if (oJson) {
			if (oJson.isSuccess) {
				$Element('deleteMsg').html(oJson.successMsg);
				
				$Element(this._oElement['acceptDeleteUserLayerBtn']).hide();
				setTimeout(function() {
					location.href = "/seiren/logout.do";
				}, 1000);
			} else {
				var sErrorMsg = oJson.errorMsg;
				$Element('deleteMsg').html(sErrorMsg);
			}
		} 
	}
	
});