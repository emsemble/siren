/**
 *  1. 파일 이름 : collectionInfo.js
 *  
 *  2. 작성자 : 김성철
 *
 *  3. 목적 : 콜렉션을 만들고 나서 곡의 공개여부, 설명 작성하게 하는 페이지
 * 
 * _onClickSaveCollection : 공개여부와 설명을 저장하는 버튼 클릭시 이벤트발생
 * _onClickPreviousPage : createCollection 페이지로 돌아가지
 *
 *  4. 이력 : 2014. 05. 24 검증하는 메소드 분리, 태그 저장하는 메소드 추가.
 */

var CollectionInfo = $Class({
	
	_oElement : null,
	_oEvent : null,
	
	
	
	$init : function() {
		$Fn(this._initialize, this).attach(window, 'load');
		$Fn(this._destroy, this).attach(window, 'unload');
	},
	
	
	
	_initialize : function() {
		this._oElement = {};
		this._oEvent = {};
		
		this._setElement();
		this._setEvent();
	},
	
	
	
	_destroy : function() {
		
	},
	
	
	
	_setElement : function() {
		this._oElement['saveCollection'] = jindo.$('saveCollection');
		this._oElement['previousPage']	 = jindo.$('previousPage');
	},

	
	
	_setEvent : function() {
		this._oEvent['clickSaveCollection'] = $Fn(this._onClickSaveInfo, this).attach(this._oElement['saveCollection'], 'click');
		this._oEvent['clickPreviousPage']   = $Fn(this._onClickPreviousPage, this).attach(this._oElement['previousPage'], 'click');
	},
	
	
	
	//정보를 받아서 저장하는 메소드. 입력하는 설명이 너무 길때 alert 발생시킨다
	_onClickSaveInfo: function (oEvent) {
		
		var form 			 = jindo.$Form(inputCollectionInfo);
		
		var descriptLengthTest = this._varifyDescription(oEvent);
		this._varifyTagsNull(oEvent);
		
		if(descriptLengthTest == true){
			this._varifyTagsNull(oEvent);
			form.submit();
		
		} else {
			
			alert("한글 80자, 영문 255자 이내로 입력해주세요");
		}
		
		oEvent.stop();
	},
	
	
	
	//태그가 비어있는지 검증하는 메소드, 태그가 하나도 체크 안됬으면 NoTag라는 분류를 단 채로 들어간다.
	_varifyTagsNull : function(oEvent){
		
		var checkBoxCount = 0;
		
		for(var i = 0; i < document.inputCollectionInfo.tags.length; i++){
			
			if(document.inputCollectionInfo.tags[i].checked == true){
				checkBoxCount++;
			}
		}
		
		if(checkBoxCount == 0){
			
			jindo.$Element('inputTags').append("<input type = 'hidden' name = 'tags' value = 'noTags'/>");
		}
	},
	
	
	
	//설명 길이를 검증하는 메소드, 입력하는 설명이 너무 길때 alert 발생시킨다
	_varifyDescription : function(oEvent){
		
		var description 	 = jindo.$('inputDescription').value;
		var stringByteLength = ( function (s,b,i,c) {
			for(b=i=0;c=s.charCodeAt(i++);b+=c>>11?3:c>>7?2:1);
			return b;
			}) (description);
		
		if (stringByteLength < 255) {
			
			return true;

		} else {
			
			return false;
		}

	},
	
	
	
	//이전 페이지로 (콜렉션 만들기) 이동하는 메소드
	_onClickPreviousPage : function (oEvent) {
		history.back();
	}
});