<!-- 
	1. 파일이름 : index.jsp
	2. 작성자 : 김시온
	3. 목적 : seiren site 첫 페이지 생성
	4. 이력
		2014. 03 : index페이지 생성, 로그인 폼 및 기본 마크업 작업
		2014. 06. 01 : index페이지 전면 개편. webGl 도입(고상우)
 -->
 <%
 	if(session.getAttribute("userId") != null){
		 	RequestDispatcher rd = request.getRequestDispatcher("/myList.do");
		 	rd.forward(request, response);
 	}
 %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<c:set var="JS_DIR" value="static/js" />
<c:set var="CSS_DIR" value="static/css" />
<c:set var="IMG_DIR" value="static/img" />
<link rel="stylesheet" href="${CSS_DIR}/common.css" />
<link rel="stylesheet" href="${CSS_DIR}/main.css" />
<script src="${JS_DIR}/jquery-1.10.1.js"></script>
<script src="${JS_DIR}/three/three.js"></script>
<script src="${JS_DIR}/three/OrbitControl.js"></script>
<script src="${JS_DIR}/three/ParticleEngine.js"></script>
<script src="${JS_DIR}/three/tween.js"></script>
</head>
<body>
<div id="container" role="main" >
	<div id="content">
		<div id="logoArea"></div>
		<div id="loginArea" class="table_cell">
			<div id="idArea" >
				<div class="iconArea">
					<img  src="${IMG_DIR}/Mail-icon.png" id="img_mailIcon" class="cLoginArea">
				</div>
				
				<div id="idMsg" class="cLoginArea" >yourname@email.com</div>		
			</div>
			
			<div id="passwordArea">
				<div class="iconArea" >
					<img  src="${IMG_DIR}/Lock-icon.png" id="img_LockIcon" class="cLoginArea">
				</div>
				<div id="pwMsg" class="cLoginArea">Password</div>
			</div>
			
			<div id="signInArea">
				<div id="signInMsg">SIGN IN</div>
			</div>
			<div id="signUp">회원가입</div>
		</div>
	</div><!--content 끝  -->	
	
	<div id="footer">
	</div>
</div>

<script>
//GLOBAL VAR
var scene;
var cssScene;
var camera;
var clock;
var engine;
var controls;
var renderer;
var rendererCSS;
var sphere;
var isCamLookAtMList = true;
var isRotating = true;
var rotateUpNum = 0;
var cameraPosition = {
	sphere : new THREE.Vector3(0, -75, 1)
};

$("#logoArea").append("<img src='static/img/logo.png'>").css("width", window.innerWidth).hide(0).delay(11000).fadeIn(3000);

var engineValues = {
	    positionStyle    : Type.CUBE,
	    positionBase     : new THREE.Vector3( 0, 0, 0 ),
	    positionSpread   : new THREE.Vector3( 800, 600, 600),
	                
	    velocityStyle    : Type.CUBE,
	    velocityBase     : new THREE.Vector3( 0, 0, 0 ),
	    velocitySpread   : new THREE.Vector3( 0.5, 0.5, 0.5 ), 

	    angleBase               : 0,
	    angleSpread             : 720,
	    angleVelocityBase       : 0,
	    angleVelocitySpread     : 4,
		
	    particleTexture : THREE.ImageUtils.loadTexture( 'static/img/particle.png' ),
	    
	    sizeBase    : 2.0,
	    sizeSpread  : 1.0,				
	    colorBase   : new THREE.Vector3(0.15, 1.0, 0.9), // H,S,L
	    colorSpread : new THREE.Vector3(0.2, 0.2, 0.2),
	    opacityBase : 1,

	    particlesPerSecond : 20000,
	    particleDeathAge   : 60.0,		
	    emitterDeathAge    : 0.1
	};
	
initWebGl();


//METHOD
function initWebGl(){
	scene = new THREE.Scene();
	
	camera = new THREE.PerspectiveCamera(45, window.innerWidth/ window.innerHeight, 0.1, 20000);
	camera.position.copy(cameraPosition.sphere);
	scene.add(camera);
	
	renderer = new THREE.WebGLRenderer({alpha : true});
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);
	
 	controls = new THREE.OrbitControls(camera, renderer.domElement);
	controls.autoRotate = true;
	controls.autoRotateSpeed = 1;
	controls.maxDistance = 500;

	var skyBoxGeometry = new THREE.CubeGeometry(10000, 10000, 10000);

	var skyBoxMaterial = new THREE.MeshBasicMaterial({
		color : 0x000000,
		side : THREE.BackSide
	});
	var skyBox = new THREE.Mesh(skyBoxGeometry, skyBoxMaterial);
	scene.add(skyBox);
	
	scene.fog = new THREE.FogExp2(0x2e343e, 0.00025);	

	var light = new THREE.PointLight(0xffffff);
	light.intensity = 1.5;
	light.position.set(0, 170, 200);
	scene.add(light);
	
	
	var ambientLight = new THREE.AmbientLight(0x111111);
	scene.add(ambientLight); 	
	
	 
	//SPHERE : 지구 모양 텍스쳐 렌더링.
	var sphereTexture = THREE.ImageUtils.loadTexture('static/img/mainPlanetTexture.png')
	var sphereGeometry = new THREE.SphereGeometry(70, 100, 100);
	var sphereMaterial = new THREE.MeshLambertMaterial({
	 	map : sphereTexture,
		color :  0xd3d3d3
	});

	sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
 
	sphere.position.x = 0;
	sphere.position.y = 0;
	sphere.position.z = 0;
	camera.lookAt({x : 0, y : 15, z : 0});
	controls.center.copy({x : 0, y : 90, z : 0});

	scene.add(sphere); 

	 
	clock = new THREE.Clock();

	engine = new ParticleEngine();	
	engine.setValues(engineValues);
	engine.initialize();
	setInterval(function(){initParticleEngine();}, 30000);
	
	new TWEEN.Tween(camera.position).to(0.5, 1000).easing(TWEEN.Easing.Cubic.InOut).start(); 
	
	render();
}




//애니메이션이 실행되는 부분 (매 프레임 갱신되어야할 부분)
function render() {
	requestAnimationFrame(render);

	var dt = clock.getDelta();
	engine.update(dt * 0.5);
	controls.update();
 	
	if(rotateUpNum < 600){		
		controls.rotateUp();
		rotateUpNum++;
	} 
	TWEEN.update();
	renderer.render(scene, camera);
}



//반지름, 중심각, 원점 x좌표를 통해 x좌표를 뽑는 삼각함수
function getXPos(r, angle, Ox) {
	return r * Math.cos(angle) + Ox;
}



//반지름, 중심각, 원점 y좌표를 통해 z좌표를 뽑는 삼각함수
function getZPos(r, angle, Oz) {
	return r * Math.sin(angle) + Oz;
}



//파티클 엔진 초기화
function initParticleEngine(){
	engine.destroy();
	engine = new ParticleEngine();
    engine.setValues(engineValues);
    engine.initialize();
}
</script>

<%@include file="common/footer.jsp"%>

<!--Template Area  -->
<script type="text/template" id="tpl_click_idArea"  >
	<input type="text" id="idInputBox" style="width:290px;height:50px;background-color:#50597b;z-index:3;border-radius:5px;border-color:#2e343e;color:white;padding-left:10px;" >	
	<div id="warningIdLayer" style="display:none;width:290px;height:50px;background-color:#50597b;z-index:3;border-radius:5px;border-color:#2e343e;color:white;padding-left:10px;">아이디가 존재하지 않습니다.</div>	
</script>

<script type="text/template" id="tpl_click_pwArea">
	<input type="password" id="pwInputBox" style="width:290px;height:50px;background-color:#50597b;z-index:3;border-radius:5px;border-color:#2e343e;color:white;padding-left:10px;" >	
	<div id="warningPwLayer" style="display:none;width:290px;height:50px;background-color:#50597b;z-index:3;border-radius:5px;border-color:#2e343e;color:white;padding-left:10px;">비밀번호가 일치하지 않습니다.</div>	
</script>
<!-- // TemplateArea -->


<script src="${JS_DIR}/jindo.desktop.all.js"></script>
<script src="${JS_DIR}/jindo_component.js"></script>
<script src="${JS_DIR}/main.js"></script>

<script type="text/javascript">
var main = new Main();
</script>