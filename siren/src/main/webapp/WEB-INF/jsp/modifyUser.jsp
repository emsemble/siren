<!-- 
	1. 작성자 : 김시온
	2. 파일 이름 : modifyUser.jsp
	3. 목적 : 회원가입 때 기입했던 회원 정보를 수정할 수 있게 해주는 페이지
	
 -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<c:set var="title" value="modifyUser" />
<%@include file="common/header.jsp"%>
<link rel="stylesheet" href="static/css/bootstrap.css" />
<link rel="stylesheet" href="${CSS_DIR}/modifyUser.css" />

	<div id="foggy" style="display:none"></div>
	<div id="deleteUserLayer" >
		<input type="button" value="X" id="closeDeleteUserLayerBtn" />
		<div  id="deleteMsg">탈퇴하시겠습니까?</div>
		 <a href="http://localhost:8080/seiren/index.do">	
		 	<input type="button" class="btn" style="left: 205px;" value="확인" id="acceptDeleteUserLayerBtn"/>
		 </a>
	</div>
	
	<div class="contentWrapper">
		<div class="contents">
			<div id="modifyUser">
				<div class='triangle'></div> 
				<h2>회원정보 수정</h2>
			
				<div class="modifyWrapper">
					<div id="userIdArea"	 style="display:table;width:70%;padding:20px 0">
						<div style="display:table-cell;width:140px">사용자 아이디</div>
						<div style="display:table-cell">${userId}</div>
					</div>
						
					<hr>
					
					<div id="userNameArea" style="display:table;width:70%;padding:20px 0">
						<div style="display:table-cell;width:140px">사용자 이름</div>
						<div id="presentUserName" style="display:table-cell" id="inputUserNameArea">
							${userName}
							<input type="button" class="btn" value="수정" id="modifyUserName" />
						</div>
						
						<div id="userNameInputArea" style='display:none'>
							<input type='text' id='newName' class="inputArea" style='display:table-cell'/><span id='nextNewName' style='display:table-cell'></span>
							<div id = 'nameCheck'></div><br>
							<input type="button" class="modifyBtn" value="수정" id="saveName" />
							<input type="button" class="modifyBtn" value="취소" id="cancelName" />
						</div>
					</div>
				
					<hr>
				
					<div id="userPasswordArea" style="display:table;width:90%;padding:20px 0">
						<div style="display:table-cell;width:140px">비밀번호</div>
						<div style="display:table-cell" id="inputUserPasswordArea" >
							******
						<input type="button" class="btn" style="left: 30px;" value="수정" id="modifyUserPw" />
					</div>
				
					<div id ="userPasswordInput" style='display:none'>
						<div style='display:table'>
							<input type='password' class="inputArea" placeholder='현재비밀번호' id='presentPassword' style='display:table-cell' onBlur='onBlurPresentPassword();return false;'/><span id='nextPresentPassword' class="valiSpan"></span><br>
							<input type="hidden" value = "0" id = "checkPresentPassword" />
						</div>
						<div style='display:table'>
							<input type='password' placeholder='새 비밀번호' class="inputArea" id='newPassword' style='display:table-cell'/><span id='nextNewPasswordConfirmBase' class="valiSpan"></span><br>
							<input type="hidden" value = "0" id = "checkNewPassword" />
						</div>
						<div style='display:table'>	
							<input type='password' placeholder='새 비밀번호 확인' class="inputArea" id='newPasswordConfirm' style='display:table-cell'/><span id='nextNewPasswordConfirm' class="valiSpan"></span><br>
			   			</div>	
						<input type="button" value="저장" class="modifyBtn" id="savePassword"/>
						<input type="button" value="취소" class="modifyBtn" id="cancelPassword"/>
					</div>
				</div>
				
				<hr>
			</div>
			
			<a href="#"><input type="button" value="계정 폐쇄" id="deleteBtn"></a>
		</div><!-- contents end -->
	</div><!-- contentWrapper end -->
	
	<div id="footer">
	</div>
</div>

<%@include file="common/footer.jsp"%>

<!-- Template Area -->

<script type="text/template" id="tpl_modify_password_result">
	******
	<input type="button" style="left: 30px;" value="수정"  class="btn" id="modifyUserPw" />
</script>

<script type="text/template" id="tpl_modify_name_result">
	{=name}
	<input type="button"  value="수정" class="btn" id="modifyUserName" />
</script>

<!-- // Template Area -->

<script src="${JS_DIR}/jindo.desktop.all.js"></script>
<script src="${JS_DIR}/jindo_component.js"></script>
<script src="${JS_DIR}/modifyUser.js"></script>
<script type="text/javascript">
var modifyUser = new ModifyUser();

function onBlurPresentPassword() {
	modifyUser._onBlurPresentPassword();
}
</script>