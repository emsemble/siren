<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<c:set var = "title" value="Sign Up With Seiren"/>
<link rel="stylesheet" href="static/css/SignUp.css" />
<link rel="stylesheet" href="static/css/common.css" />
</head>
<body>
	<header>
		<div class='menubar'>
			<a href="clickLogo.do" ><span id='logoArea' class='menu'><img src="static/img/logo-m.png"></span></a>
		</div>
	</header><!-- header end -->

	<div class="contentWrapper">
		<div class="contents">
			
			<form name="signUpForm" action="signUpService.do" method="post">
				<div id="signUpRapper">
					<div class="triangle"></div>
					<h2 id = signUpTitle>SIGN UP</h2>
					
					<div id = signUpWrapper>
						<div id = "signUpIdInput" class="content" style="display:table;">
							 <label id = "signUpIdArea" class="titleArea" style="display:table-cell;">ID</label>
							 <input type = "email" class="inputArea" style="display:table-cell;" placeholder = "Please Input Your E-Mail" id = "signUpId" name = "signUpId"/>
							 <input type = "hidden" id = "isClickIdCheck" value = 0 />
							 <div id = ajaxCheck class="check">가입하고자 하는 아이디(이메일)를 적어주세요</div>
						</div>
						
						<hr>
						
						<div id = "signUpPwInput" class="content" style="display:table;">
							 <label id = "signUpPwArea" class="titleArea" style="display:table-cell;">PW</label>
							 <input type = "password" class="inputArea" style="display:table-cell;" placeholder = "Please Input Your PW" id = "signUpPw" name = "signUpPw"/><br>
							 <input type = "password" class="inputArea" style="display:table-cell;"placeholder = "Please Input Your PW Again!" id = "signUpPw2"/>
							 <input type = "hidden" id = "isPwCheck" value = 0 />
							 <div id = pwCheck class="check">비밀번호는 6-12자리의 영문+숫자로 입력해주세요</div>
						</div>			
						
						<hr>
						
						<div id = "signUpNameInput" class="content" style="display:table;">
							 <label id = "signUpNameArea" class="titleArea" style="display:table-cell;">NAME</label>
							 <input type = "text" class="inputArea" style="display:table-cell;" placeholder = "Please Input Your Name" id = "signUpName" name = "signUpName"/>
							 <input type = "hidden" id = "stringByteLength" value = 0 />
							 <div id = nameCheck class="check">이름은 한글 6자, 영문 18자 이내로 입력해주세요</div>
						</div>
						
						<hr>
						
						 <input type = "button" id ="signUpButton" value = "회원가입"/>
					</div>
				</div>
			</form>
			
		</div><!-- contents end -->
	</div><!-- contentWrapper end -->

<%@ include file="common/footer.jsp"%>

<script src="static/js/jindo.desktop.all.js"></script>
<script src="static/js/jindo_component.js"></script>
<script src="static/js/signUp.js"></script>

<script type = "text/javascript">
var signup = new SignUp();
</script>