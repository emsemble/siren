<!-- 
 1. 파일 이름 : detailListInfo.jsp
 2. 작성자 : 김성철
 3. 목적 : 콜렉션의 설명과 공개여부를 받는 페이지
 4. 이력 : 2014. 05. 24 중간 완성
-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<c:set var="title" value="Detail List" />
<%@include file="common/header.jsp"%>
<link rel="stylesheet" href="${CSS_DIR}/detailListInfo.css" />
<div id="contentsWrapper">
	<div id="contents">
		<div id = "basics">
			<div id = "basicInfo">
				<label id = "collectionNum"></label>
				<label id = "collectionName"></label>
				<label id = "collectionDescription"></label>
				<div id = "basicButtons">
					<c:choose>
						
						<c:when test="${amiFollowing==0}">
							<!-- ami == 0 팔로우 안함 -->
<%-- 							<img src='${IMG_DIR}/unfollow.png' id="follow" ami="0"> --%>
								<button id="follow" ami="0" style="background : #e64c65;">FOLLOW</button>
						</c:when>
						
						<c:when test="${amiFollowing==1}">
							<!-- ami == 1 팔로우 하고있음 -->
<%-- 							<img src='${IMG_DIR}/follow.png' id="follow" ami="1">	 --%>
								<button id="follow" ami="1">UNFOLLOW</button>
						</c:when>
						<c:otherwise>
							<!-- ami == 2 내 소유 -->
						</c:otherwise>
					</c:choose>					
<%-- 					<img src='${IMG_DIR}/playBtn.png' id="playList"> --%>
						<button id="playList">PLAY</button>
				</div>
			</div>
		</div>
	
		<div id = "details">
			<div id = "imagesAndComments">
				<div id = "albumImages"></div>
				<div id = "comments">
					<div id = "inputComment">
						<textarea rows="3" cols="45" id = "commentContents" placeholder="."></textarea>
						<img src ="${IMG_DIR}/write.png" id = "saveComment">
					</div>
				
					<div id = "showComments"></div>
				</div>
			</div>
				
			<div id ="songRankAndList">
				<div id = "genreRank">
					<div id = "graph"></div>
					<div id = "rankInfo">
						<div id = genreRank0 class='genreList'>
							<div class='genre2'><span>Genre</span></div>
							<div class='rank'><span>Rank</span></div>
							<div class='count'><span>Songs</span></div>
						</div>
					</div>
				</div>
				<div id = "songLists"></div>
			</div>
		</div>
	</div>
</div>

<%@include file="common/footer.jsp"%>

<script src="${JS_DIR}/jquery-1.10.1.js"></script>
<script src="${JS_DIR}/plugin/highcharts-custom.js"></script>
<script src="${JS_DIR}/dev.js"></script>
<script src="${JS_DIR}/detailListInfo.js"></script>
<script type="text/javascript">
var detailJson   = ${requestScope.detailResult};
var rankJson     = ${requestScope.rankResult};
var commentsJson = ${requestScope.comments};

var detailInfo = eval(detailJson);
var genreRank  = eval(rankJson);
var comments   = eval(commentsJson);

if (detailInfo == "" || detailInfo == null) {

	$('#contents').html("<span>곡이 하나도 없습니다..<span>");
	alert("리스트에 곡이 하나도 없네요?");

} else {

	_initialize(detailInfo, genreRank, comments);
	_eventListner();
	
	_getWindowHeight();
};
</script>