<!-- 
	1. 파일이름 : index.jsp
	2. 작성자 : 김시온
	3. 목적 : 로그인 후 해당 회원의 모든 리스트를 보여주는 화면 
	4. 이력
		2014. 03
			: 로그인 한 해당 회원의 모든 리스트들을 slide 형식으로 썸네일로 보여줌
				1) 회원가입 직후 들어온 회원 , 보유 리스트가 없는 회원 
					-> default로 내장된 이미지가 보임(click시 createList로 이동할 수 있음)
				
				2) 보유 리스트가 있는 회원
					2-1) 한 번도 재생하지 않은 리스트 : 해당 리스트 부분에 NEW라는 글씨 보임(click시 player화면으로 이동)
					2-2) 재생한 리스트 : 해당 리스트에 해당하는 svg가 썸네일로 보여짐.
		
		2014.05.02 공통으로 들어가는 navigation을 common/header.jsp로 분리			
 -->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<c:set var="title" value="myList" />
<%@include file="common/header.jsp"%>
<script src="${JS_DIR}/jquery-1.10.1.js"></script>
<script src="${JS_DIR}/jquery-ui-1.10.4.js"></script>
<script src="${JS_DIR}/three/three.js"></script> 
<script src="${JS_DIR}/three/OrbitControl.js"></script> 
<script src="${JS_DIR}/three/ParticleEngine.js"> </script>
<script src="${JS_DIR}/three/CSS3DRenderer.js"> </script>
<script src="${JS_DIR}/three/tween.js"> </script>
<script src="${JS_DIR}/myList.js"></script>
<link rel="stylesheet" href="${CSS_DIR}/bootstrap.css" />
<link rel="stylesheet" href="${CSS_DIR}/player.css" />
<link rel="stylesheet" href="${CSS_DIR}/myList.css" />




<div id="container" role="myList">
	<!-- content start -->
	<div id="content">
		<div id="bttnArea">
			<div class='glControl' id='listToggle'>MY &nbsp;&nbsp;&nbsp;▶</div>
			<div class='glControl' id='mousePanel'></div>
			<div class='glControl' id='rotatingToggle'></div>
			<div class='glControl' id='createList'></div>
			<div id="dialog-form" title="Create My List">
			    <label for="title">title</label>
			    <input type="text" name="inputValue" id="inputValue" value="" class="text ui-widget-content ui-corner-all">
			</div>
		</div>		
		<div id="myLists"></div>
		<div id="followLists"></div>
	</div>
	<!--content end  -->
</div>
<script type="text/javascript">
$(document).ready(function(){
	var jsonCStr = ${collectionList};
	var jsonFStr = ${followList};
	var jsonCollectionList = eval(jsonCStr);
	var jsonFollowList = eval(jsonFStr);
	
	
  	for (var i = 0; i < jsonCollectionList.length; i++) {
	   
	   if(jsonCollectionList[i].thumbnail != null) {
	   	   
		   $("#myLists").append("<div class='mList' isMouseOver=0 isSelected=0 listName='"+ jsonCollectionList[i].name
				   				+"'listNum='" + jsonCollectionList[i].num +"'>"
				     			+jsonCollectionList[i].thumbnail
				     			+"<div class='listBttn'><img class='modify' src='static/img/modify.png'>"
				     			+"<img class='play' src='static/img/play-Icon.png'></img>"
				     			+"<img class='more' src='static/img/more.png'></div>"
				     			+"</div>"
				     			);
	   } 
	   
	   else {
		   $("#myLists").append("<div class='mList' isMouseOver=0 isSelected=0 listName='" + jsonCollectionList[i].name
				   				+ "'listNum='" + jsonCollectionList[i].num +"'>"
				   				+'<svg width="640" height="480" xmlns="http://www.w3.org/2000/svg">'
				   				 //<!-- Created with SVG-edit - http://svg-edit.googlecode.com/ -->
				   				+ '<defs> <linearGradient id="svg_6" x1="0" y1="0" x2="1" y2="1">'
				   				+ '<stop offset="0" stop-color="#00ce9b"/><stop offset="1" stop-color="#00b0cc"/>'
				   			 	+ '</linearGradient></defs>'
				   			    + '<g>'
				   			    + '<path fill="url(#svg_6)" stroke="#ffffff" stroke-width="5" stroke-linejoin="null" stroke-linecap="null" d="m260,159.45999l69.27213,0l57.72787,63.67398l-57.72787,63.67395l-69.27213,0l0,-127.34793z" id="svg_2" transform="rotate(90 323.50000000000006,223.13395690917972) "/>'
				   			    + '<text fill="#ffffff" stroke="#ffffff" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" opacity="0.97" x="324" y="218" id="svg_3" font-size="24" font-family="Fantasy" text-anchor="middle" xml:space="preserve">NEW</text>'
				   			  	+ '</g></svg>'				   			
				     			+ "<div class='listBttn'><img class='modify' src='static/img/modify.png'>"
				   			  	+ "<img class='play' src='static/img/play-Icon.png'></img>"
				     			+ "<img class='more' src='static/img/more.png'></div>"
				   				+"</div>");
	   }
   };
   
   
	for (var i = 0; i < jsonFollowList.length; i++) {
		   
		   if(jsonFollowList[i].thumbnail != null) {
		   	   
			   $("#followLists").append("<div class='fList' isMouseOver=0 isSelected=0 listName='"+ jsonFollowList[i].name 
					   				+"'listNum='" + jsonFollowList[i].num +"'>"
					     			+jsonFollowList[i].thumbnail
					     			+"<div class='listBttn'><img class='play' src='static/img/play-Icon.png'></img>"
					     			+ "<img class='more' src='static/img/more.png'></div>"
					     			+"</div>"
					     			);
		   } 
		   
		   else {
			   $("#followLists").append("<div class='fList' isMouseOver=0 isSelected=0 listName='"+ jsonFollowList[i].name 
		   							+"'listNum='" + jsonFollowList[i].num +"'>"
					   				+'<svg width="640" height="480" xmlns="http://www.w3.org/2000/svg">'
					   				 //<!-- Created with SVG-edit - http://svg-edit.googlecode.com/ -->
					   				+ '<defs> <linearGradient id="svg_6" x1="0" y1="0" x2="1" y2="1">'
					   				+ '<stop offset="0" stop-color="#00ce9b"/><stop offset="1" stop-color="#00b0cc"/>'
					   			 	+ '</linearGradient></defs>'
					   			    + '<g>'
					   			    + '<path fill="url(#svg_6)" stroke="#ffffff" stroke-width="5" stroke-linejoin="null" stroke-linecap="null" d="m260,159.45999l69.27213,0l57.72787,63.67398l-57.72787,63.67395l-69.27213,0l0,-127.34793z" id="svg_2" transform="rotate(90 323.50000000000006,223.13395690917972) "/>'
					   			    + '<text fill="#ffffff" stroke="#ffffff" stroke-width="0" stroke-linejoin="null" stroke-linecap="null" opacity="0.97" x="324" y="218" id="svg_3" font-size="24" font-family="Fantasy" text-anchor="middle" xml:space="preserve">NEW</text>'
					   			  	+ '</g></svg>'			
					     			+"<div class='listBttn'><img class='play' src='static/img/play-Icon.png'></img>"
					     			+ "<img class='more' src='static/img/more.png'></div>"
					   				+"</div>");
		   }
	   };

   
  	$("circle").fadeIn(0);
   	$("path").fadeIn(0);
  	$("text").fadeIn(0);
	$(".listBttn").hide(0);
	$("#createInput").hide(0);
	initWebGl();   
	bindEvent();
});
</script>
<%@include file="common/footer.jsp"%>