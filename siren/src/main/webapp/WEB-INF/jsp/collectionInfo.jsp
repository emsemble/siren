<!-- 
 1. 파일 이름 : collectionInfo.jsp
 2. 작성자 : 김성철
 3. 목적 : 콜렉션의 설명과 공개여부를 받는 페이지
 4. 이력 : 2014. 04. 24 최초작성
 		   2014. 06. 04 div 구조 수정
-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<c:set var="title" value="Input Your collectionInfo" />
<%@include file="common/header.jsp"%>
<link rel="stylesheet" href="static/css/bootstrap.css" />
<link rel="stylesheet" href="${CSS_DIR}/collectionInfo.css" />
	<div id = "contentWrapper">
		<div id= "contents">
			<div id="listNameLayer">
				<img id="previousPage" src="${IMG_DIR}/leftBtn.png"/><span id="listName">${requestScope.listName}</span>
			</div>
			<div id= "collectionInfoLayer">
				<form id = "inputCollectionInfo" name = "inputCollectionInfo" method="post" action="inputCollectionInfo.do">
					
					<div id = "title">
						<div class="triangle"></div>
						<span class = "title">OPTION</span>
					</div>
					
					<div id="shareAndDesc">
						Description : 	  <input type = "text" id ="inputDescription" name = "inputDescription"/>
						<input type = "radio" id = "isPublic" checked="checked" name = "isPublic" value = "1"/>공개
						<input type = "radio" id = "isPublic" name = "isPublic" value = "2"/>비공개
						<input type = "hidden" name = "listNum" value = "${requestScope.listNum}"/>
					</div>
	
					<div id = "inputTags">
						<div>
							<div class="triangle" style="top:150px;"></div>
							<span class="title">TAG</span>
						</div>
						<div id = "emotionalTags" class="tagsDiv">
							<span class="classifytitle">EMOSIONAL</span>
							<ul class="tagLists">
								<li><input type = "checkbox" value = "Happy" name ="tags" class = "tags" id="checkbox1"><label class="tags" for="checkbox1"></label>Happy</li>
								<li><input type = "checkbox" value = "Gloomy" name ="tags" class = "tags" id="checkbox2"><label class="tags" for="checkbox2"></label>Gloomy</li>
								<li><input type = "checkbox" value = "Lovely" name ="tags" class = "tags" id="checkbox3"><label class="tags" for="checkbox3"></label>Lovely</li>
								<li><input type = "checkbox" value = "Exciting" name ="tags" class = "tags" id="checkbox4"><label class="tags" for="checkbox4"></label>Exciting</li>
								<li><input type = "checkbox" value = "Calm" name ="tags" class = "tags" id="checkbox5"><label class="tags" for="checkbox5"></label>Calm</li>
								<li><input type = "checkbox" value = "Peaceful" name ="tags" class = "tags" id="checkbox6"><label class="tags" for="checkbox6"></label>Peaceful</li>
							</ul>
							</div>
						
						<div id = "songAttrTags" class="tagsDiv">
							<span class="classifytitle">GENRE</span>
							<ul class="tagLists">
								<li><input type = "checkbox" value = "Pop" name ="tags" class = "tags" id="checkbox7"><label class="tags" for="checkbox7"></label>Pop</li>
								<li><input type = "checkbox" value = "KPop" name ="tags" class = "tags" id="checkbox8"><label class="tags" for="checkbox8"></label>KPop</li>
								<li><input type = "checkbox" value = "Dance" name ="tags" class = "tags" id="checkbox9"><label class="tags" for="checkbox9"></label>Dance</li>
								<li><input type = "checkbox" value = "Rock" name ="tags" class = "tags" id="checkbox10"><label class="tags" for="checkbox10"></label>Rock</li>
								<li><input type = "checkbox" value = "R&B" name ="tags" class = "tags" id="checkbox11"><label class="tags" for="checkbox11"></label>R&B</li>
							</ul>
						</div>
						
						<div id = "yearTags" class="tagsDiv">
							<span class="classifytitle">YEAR</span>
							<ul class="tagLists">
								<li><input type = "checkbox" value = "1960s" name ="tags" class = "tags" id="checkbox12"><label class="tags" for="checkbox12"></label>1960's</li>
								<li><input type = "checkbox" value = "1970s" name ="tags" class = "tags" id="checkbox13"><label class="tags" for="checkbox13"></label>1970's</li>
								<li><input type = "checkbox" value = "1980s" name ="tags" class = "tags" id="checkbox14"><label class="tags" for="checkbox14"></label>1980's</li>
								<li><input type = "checkbox" value = "1990s" name ="tags" class = "tags" id="checkbox15"><label class="tags" for="checkbox15"></label>1990's</li>
								<li><input type = "checkbox" value = "2000s" name ="tags" class = "tags" id="checkbox16"><label class="tags" for="checkbox16"></label>2000's</li>
								<li><input type = "checkbox" value = "2010s" name ="tags" class = "tags" id="checkbox17"><label class="tags" for="checkbox17"></label>2010's</li>
							</ul>
						</div>
					</div>
				</form>
				<div id = "controllButton">
					<button id = "saveCollection">SAVE</button>
				</div>
			</div>
		</div>
	</div>
<%@include file="common/footer.jsp"%>

<script src="${JS_DIR}/jindo.desktop.all.js"></script>
<script src="${JS_DIR}/jindo_component.js"></script>
<script src="${JS_DIR}/collectionInfo.js"></script>
<script type="text/javascript">
var collectionInfo = new CollectionInfo();
</script>
