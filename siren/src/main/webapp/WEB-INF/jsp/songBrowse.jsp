<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<c:set var="title" value="songBrowse" />
<%@include file="common/header.jsp"%>

<link rel="stylesheet" href="static/css/bootstrap.css" />
<link rel="stylesheet" href="static/css/songBrowse.css" />
	
	<div isSelected='0' class="contentWrapper">
		<div isSelected='0' class="contents">
			<div isSelected='0' class="left_content">
				<div isSelected='0' class="cnt_search_box">
					<span isSelected='0' class="title">Search</span>
					<input type="text" class="box" onkeydown="fncEvtSearch(event)" placeholder="Song Name, Artist..">
					<hr class="hr" >
				</div>
			
				<div isSelected='0' class="filterArea">
					<form action="" class="filter" onsubmit=" return _submitFilterCB(event)">
						<span class="title">Filter</span>
						<input type="submit" style="float:right" id="submitBtn" value='apply'>
						<div class="formWrapper">
							<div class="selectArea" >
								<span class="title">Genre</span><br>
								<div class="formArea"> 
								 	<input type="checkbox" class="option" name="genre" id="1" value="Animation"><label class="option" for="1"></label>
								 	<span class="ckValue">Animation</span><br>
								 	<input type="checkbox" class="option" name="genre" id="2" value="Blues"><label class="option" for="2"></label>
								 	<span class="ckValue">Blues</span><br>
								 	<input type="checkbox" class="option" name="genre" id="3" value="Country"><label class="option" for="3"></label>
								 	<span class="ckValue">Country</span><br>
								 	<input type="checkbox" class="option" name="genre" id="4" value="Dance"><label class="option" for="4"></label>
								 	<span class="ckValue">Dance</span><br>
								 	<input type="checkbox" class="option" name="genre" id="5" value="Electronica"><label class="option" for="5"></label>
								 	<span class="ckValue">Electronica</span><br>
								 	<input type="checkbox" class="option" name="genre" id="6" value="Folk"><label class="option" for="6"></label>
								 	<span class="ckValue">Folk</span><br>
								 	<input type="checkbox" class="option" name="genre" id="7" value="Foreign Movie"><label class="option" for="7"></label>
								 	<span class="ckValue">Foreign Movie</span><br>
								 	<input type="checkbox" class="option" name="genre" id="8" value="Jazz"><label class="option" for="8"></label>
								 	<span class="ckValue">Jazz</span><br>
								 	<input type="checkbox" class="option" name="genre" id="9" value="New Age"><label class="option" for="9"></label>
								 	<span class="ckValue">New Age</span><br>
								 	<input type="checkbox" class="option" name="genre" id="10" value="Pop"><label class="option" for="10"></label>
								 	<span class="ckValue">Pop</span><br>
								 	<input type="checkbox" class="option" name="genre" id="11" value="R&B / Soul"><label class="option" for="11"></label>
								 	<span class="ckValue">R&B / Soul</span><br>
								 	<input type="checkbox" class="option" name="genre" id="12" value="Rap / Hip-hop"><label class="option" for="12"></label>
								 	<span class="ckValue">Rap / Hip-hop</span><br>
								 	<input type="checkbox" class="option" name="genre" id="13" value="Rock"><label class="option" for="13"></label>
								 	<span class="ckValue">Rock</span><br>
								 	<input type="checkbox" class="option" name="genre" id="14" value="Vocal/Choral"><label class="option" for="14"></label>
								 	<span class="ckValue">Vocal/Choral</span><br>
								 	<input type="checkbox" class="option" name="genre" id="15" value="World"><label class="option" for="15"></label>
								 	<span class="ckValue">World</span><br>
								</div>	
							</div>
							
							<div class="selectArea">
								<span class="title">Year</span><br>
								<div class="formArea">
									<input type="checkbox" class="option" name="year" id="16" value="197%"><label class="option" for="16"></label>
								 	<span class="ckValue" style="position: relative;">1970's</span><br>
								 	<input type="checkbox" class="option" name="year" id="17" value="198%"><label class="option" for="17"></label>
								 	<span class="ckValue" style="position: relative;">1980's</span><br>
								 	<input type="checkbox" class="option" name="year" id="18" value="199%"><label class="option" for="18"></label>
								 	<span class="ckValue" style="position: relative;">1990's</span><br>
									<input type="checkbox" class="option" name ="year" id="19" value="20%"><label class="option" for="19"></label>
								 	<span class="ckValue" style="position: relative;">2000's</span><br>
								</div>	
							</div>	
							
							<div class="selectArea">
								<span class="title">Artist</span><br>
								<div class="formArea">
									<input type="checkbox" class="option" name="NTL" id="20" value="대한민국%"><label class="option" for="20"></label>
								 	<span class="ckValue" style="position: relative;">Korean</span><br>
								 	<input type="checkbox" class="option" name="NTL" id="21" value="foreign%"><label class="option" for="21"></label>
								 	<span class="ckValue" style="position: relative;">Foreign</span><br>
									<input type="checkbox" class="option" name="sex" id="22" value="M%"><label class="option" for="22"></label>
								 	<span class="ckValue" style="position: relative;">Male</span><br>
								 	<input type="checkbox" class="option" name="sex" id="23" value="F%"><label class="option" for="23"></label>
								 	<span class="ckValue" style="position: relative;">Female</span><br>
								</div>	
							</div>	
						</div>
					</form>
				</div><!-- filterArea end -->
			
			</div><!-- left_content end -->
			
			<audio id ="player" controls="controls" src="" autoplay="autoplay" style="display:none;"></audio>
			
			<div isSelected='0' class="right_content">
				<div isSelected='0' class="songArea" style="padding-left:25px;"></div>
			</div><!-- right_content end -->
	
		</div><!-- contents end -->
	</div><!-- contentWrapper end -->
	

</body>
<%@include file="common/footer.jsp"%>

<script src="${JS_DIR}/jquery-1.10.1.js"></script>
<script src="${JS_DIR}/dev.js"></script>
<script src="${JS_DIR}/jquery.mockjax.js"></script>
<script src="${JS_DIR}/jquery.autocomplete.js"></script>
<script src="${JS_DIR}/songBrowse.js"></script>
<script type="text/javascript">
$(document).ready( function() {
	var songList = ${songList};
	_insertSongByGenre(songList);
	_bindEventListener();
});
</script>

</html>