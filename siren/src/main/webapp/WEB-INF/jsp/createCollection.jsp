<!-- 
/**
 *  1. 파일 이름 : createCollection.jsp
 *  
 *  2. 작성자 : 김시온
 *
 *  3. 목적 : 콜렉션을 만들때 곡 추가, 삭제 등  기능
 *
 *  4. 이력 : 
 *  	2014. 05. 26 : 최초생성
 *  			  ~ 27 : list에 곡 추가, 삭제 , fiilter, 저장 기능 구현
 *
 -->
 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<title>${createCollection}</title>

<%@include file="common/header.jsp"%>
<link rel="stylesheet" href="${CSS_DIR}/bootstrap.css" />
<link rel="stylesheet" href="${CSS_DIR}/createCollection.css" />

	<div class="contentWrapper">
		<div class="contents">
			<div class="left_content">

				<div class='userInfoArea' >
					<input type="hidden" value="${listNum}" id='listNum'>
					<div><h2>${listName}</h2></div>
					<a href="collectionInfo.do?listNum=${listNum}&listName=${listName}">
					<button class="saveList">option</button></a>
				</div>
			
				<div class="filterArea">
					<ul class="selectArea" >
						<li class="option"><span class="genre">New Relase</span></li>
						<li class="option"><span class="genre">Animation</span></li>
						<li class="option"><span class="genre">Blues</span></li>
						<li class="option"><span class="genre">Country</span></li>
						<li class="option"><span class="genre">Dance</span></li>
						<li class="option"><span class="genre">Electronica</span></li>
						<li class="option"><span class="genre">Folk</span></li>
						<li class="option"><span class="genre">Foreign Movie</span></li>
						<li class="option"><span class="genre">Jazz</span></li>
						<li class="option"><span class="genre">New Age</span></li>
						<li class="option"><span class="genre">Pop</span></li>
						<li class="option"><span class="genre">R&B / Soul</span></li>
						<li class="option"><span class="genre">Rap / Hip-hop</span></li>
						<li class="option"><span class="genre">Rock</span></li>
						<li class="option"><span class="genre">DVocal/Choralance</span></li>
						<li class="option"><span class="genre">World</span></li>
						<li class="option"><span class="genre">1970's</span></li>
						<li class="option"><span class="genre">1980's</span></li>
						<li class="option"><span class="genre">1990's</span></li>
						<li class="option"><span class="genre">2000's</span></li>
					</ul><!-- selectArea end -->
				</div><!-- filterArea end -->
			</div><!-- left_content end -->
			
			<div class="right_content">
				<div class="songArea" style="padding-left:25px;"></div>
			</div><!-- right_content end -->
		
		</div><!-- contents end -->
			
<%-- 		<div id = "selectPage">
			<!-- 이전 버튼은 페이지가 10개 이상이고 이미 앞에 10개의 페이지가 있을때만 노출한다. -->
			<c:if test="${pageCount > pagePerPage && startPage > pagePerPage}">
				<a href="/seiren/createCollection.do?page=${startPage - 1}&sortType=${sortType}">이전 | </a>
			</c:if>
	
			<!-- 해당 pageSetGroup의 시작과 끝 페이지를 출력한다. -->
			<c:forEach begin="${startPage}" end="${endPage}" step="1" varStatus="status">
				<a href="/seiren/createCollection.do?page=${status.current}&sortType=${sortType}&listNum=${listNum}"
				<c:if test="${status.current eq currentPage}"> style="color:red"</c:if>>${status.current}</a>
				<c:if test="${!status.last}"> | </c:if> 
			</c:forEach>
	
			<!-- 다음 버튼은 페이지가 10개 이상이고 총 페이지갯수가 마지막 페이지보다 클 경우(아직 페이지가 남았을 경우)에 노출한다. -->
			<c:if test="${pageCount > pagePerPage && endPage < pageCount}">
				<a href="/seiren/createCollection.do?page=${endPage + 1}&sortType=${sortType}&listNum=${listNum}"> | 다음</a>
			</c:if>
		</div>
		
		<hr> --%>

	</div><!-- contentWrapper end -->
	
	<!-- 저장한  songList -->
	<audio id ="player" controls="controls" src="" autoplay="autoplay" style="display:none;"></audio>
	
	<div id ="saveSongInfo">
		<div class='savedWrapper'>
		</div>
	</div>
	
	

</body>
<%@include file="common/footer.jsp"%>

<script src="${JS_DIR}/jindo.js"></script>
<script src="${JS_DIR}/jindo_component.js"></script>
<script src="${JS_DIR}/createCollection.js"></script>
<script src="${JS_DIR}/jquery-1.10.1.js"></script>
<script src="${JS_DIR}/jquery.mockjax.js"></script>
<script src="${JS_DIR}/jquery.autocomplete.js"></script>
<script src="${JS_DIR}/dev.js"></script>
<script>
	var listNum = ${listNum};
	$(document).ready(function(){
		var songList = ${songList};
		
		_onloadMySongs();
		_insertSongByGenre(songList);
		_bindEventListener();
	
	});
	
	//해당 페이지를 벗어날때 플레이어에 관한 정보를 갱신하고 섬네일을 제거한다.
	$(window).on("beforeunload", function(){
		$.ajax({
			url : "savePlayer.do",
			async : false,
			data : {
				listNum : listNum,
				width : screen.width,
				height : screen.height
			},
			type : "POST",
			dataType : "text",
			success : function(responseData){
				console.log("플레이어 갱신 성공") ;
				
				$.ajax({
					url : "deleteThumbnail.do",
					async : false,
					data : {
						listNum : listNum,
					},
					type : "POST",
					dataType : "text",
					success : function(responseData){
						console.log("섬네일 제거 성공") ;
					},
					fail : function(responseData){
						console.log("섬네일 제거 실패") ;
					}//end of success : function
				})//end of $.ajax
			},
			fail : function(responseData){
				console.log("플레이어 갱신 실패") ;
			}//end of success : function
		})//end of $.ajax
		
		
	})
</script>

<%@include file="common/footer.jsp"%>

<!-- Template  -->
<script type="text/template" id="tpl_searchSongList" >
	<div id= "{=songNum}">
		<label id = "songImg"><img src ={=songImg}></label>
		<label id = "songNum">{=songNum}</label>
		<label id = "songName">{=songName}</label>
		<label id = "artistName">{=artistName}</label>
		<label id = "genre">{=genre}</label>
		<label id = "issueDate">{=issueDate}</label>
		<label id = "nationalityName">{=nationalityName}</label>
		<label id = "actTypeName">{=actTypeName}</label>
	</div>	
</script>
