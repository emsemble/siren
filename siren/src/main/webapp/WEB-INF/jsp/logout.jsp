<!-- 
 1. 파일 이름 : logout.jsp
 2. 작성자 : 김성철
 3. 목적 : Logout 요청에 대한 페이지
 4. 이력 : 2014. 04. 16 최초 작성
-->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% session.invalidate(); %>
<script type="text/javascript">
	location.href = "index.do";
</script>