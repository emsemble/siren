<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@include file="common/header.jsp"%>
		<script src="${JS_DIR}/d3.js"></script>
        <script type="text/javascript">
        	var dataset = [ 5, 10, 15, 20, 25 ];
            
            d3.select("body").selectAll("p")
            .data(dataset)
            .enter()
            .append("p")
            .text(function(d){
            	return d;
            }
            );
            
            
        </script>
    </body>
</html>     