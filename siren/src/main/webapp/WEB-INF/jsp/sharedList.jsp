j<!-- 
 1. 파일 이름 : sharedList.jsp
 2. 작성자 : 김성철
 3. 목적 : sharedList 브라우징시 나오는 페이지
 4. 이력 : 
 
 2014. 05. 14. 중간 완성
 
 2014. 06. 05. 공유된 리스트 아무것도 없을떄 나오는 화면 수정
-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<c:set var="title" value="sharedList" />
<%@include file="common/header.jsp"%>

<link rel="stylesheet" href="static/css/bootstrap.css" />
<link rel="stylesheet" href="${CSS_DIR}/sharedList.css" />

<div id ="contentWrapper">
	<div id ="contents">
		<div id = "sideBar">
			<div id = "search">
				<span class="title">Search</span>
				<input type ="text" id="searchString" class="box" name="searchString" placeholder="Input some Keyword or ListName.."/>
				<hr class="hr">
			</div>

			<div class = "selectArea">
				<span id = "myFollowingList" class = "title">My Follow List</span>
			</div>

			<form name ='checkBoxForm' id ='checkBoxForm' method ='post'>
				<div id="filter">
					<span class="title">Filter</span>
					<button type="button" id = "submitFilter">apply</button>
					<div class = "selectArea">
						<span class ="title">Emotional</span>
						<div id = "emotionalTags" class ="formArea">
							<p><input type = "checkbox" value = "Happy" name ="emotional" class = "tags" id="checkbox1"><label class="tags" for="checkbox1"></label>Happy
							<p><input type = "checkbox" value = "Gloomy" name ="emotional" class = "tags" id="checkbox2"><label class="tags" for="checkbox2"></label>Gloomy
							<p><input type = "checkbox" value = "Lovely" name ="emotional" class = "tags" id="checkbox3"><label class="tags" for="checkbox3"></label>Lovely
							<p><input type = "checkbox" value = "Exciting" name ="emotional" class = "tags" id="checkbox4"><label class="tags" for="checkbox4"></label>Exciting
							<p><input type = "checkbox" value = "Calm" name ="emotional" class = "tags" id="checkbox5"><label class="tags" for="checkbox5"></label>Calm
							<p><input type = "checkbox" value = "Peaceful" name ="emotional" class = "tags" id="checkbox6"><label class="tags" for="checkbox6"></label>Peaceful
						</div>
					</div>
					
					<div class = "selectArea">
						<span class="title">Genre</span>
						<div id = "songAttrTags" class ="formArea">
							<p><input type = "checkbox" value = "Pop" name ="songAttr" class = "tags" id="checkbox7"><label class="tags" for="checkbox7"></label>Pop
							<p><input type = "checkbox" value = "KPop" name ="songAttr" class = "tags" id="checkbox8"><label class="tags" for="checkbox8"></label>KPop
							<p><input type = "checkbox" value = "Dance" name ="songAttr" class = "tags" id="checkbox9"><label class="tags" for="checkbox9"></label>Dance
							<p><input type = "checkbox" value = "Rock" name ="songAttr" class = "tags" id="checkbox10"><label class="tags" for="checkbox10"></label>Rock
							<p><input type ="checkbox" value = "R&B" name ="songAttr" class = "tags" id="checkbox11"><label class="tags" for="checkbox11"></label>R&B
						</div>
					</div>
					
					<div class = "selectArea">
						<span class = "title">Year</span>
						<div id = "yearTags" class ="formArea">
							<p><input type = "checkbox" value = "1960s" name ="year" class = "tags" id="checkbox12"><label class="tags" for="checkbox12"></label>1960's
							<p><input type = "checkbox" value = "1970s" name ="year" class = "tags" id="checkbox13"><label class="tags" for="checkbox13"></label>1970's
							<p><input type = "checkbox" value = "1980s" name ="year" class = "tags" id="checkbox14"><label class="tags" for="checkbox14"></label>1980's
							<p><input type = "checkbox" value = "1990s" name ="year" class = "tags" id="checkbox15"><label class="tags" for="checkbox15"></label>1990's
							<p><input type = "checkbox" value = "2000s" name ="year" class = "tags" id="checkbox16"><label class="tags" for="checkbox16"></label>2000's
							<p><input type = "checkbox" value = "2010s" name ="year" class = "tags" id="checkbox17"><label class="tags" for="checkbox17"></label>2010's
						</div>
					
					<div class = "selectArea">
						<span id = "noTags" class="title">No Tags</span>
					</div>
					
					</div>
				</div>
			</form>
		</div>
		
		<div id = "lists">
			<div class="triangle"></div>
			<span id="topListLabel" class = "listsLabel">Seiren's HOT 3 List</span>
			<div id = "topList"></div>

			<div class="triangle"></div>
			<span id = "sharedListLabel" class = "listsLabel">Shared Lists</span>
			<div id = "sharedPublicList"></div>

			<div class="triangle" id ="filteredListTriangle"></div>
			<span id = "filteredListLabel" class = "listsLabel">검색 결과</span>
			<div id = "filteredList"></div>
		</div>
		
		<div id = "buttons">
		<button id = "topButton" style="display:none">맨위로</button>
		</div>
	</div>
</div>



<%@include file="common/footer.jsp"%>

<script src="${JS_DIR}/jindo.js"></script>
<script src="${JS_DIR}/jindo_component.js"></script>
<script src="${JS_DIR}/jquery-1.10.1.js"></script>
<script src="${JS_DIR}/jquery.autocomplete.js"></script>
<script src="${JS_DIR}/jquery.mockjax.js"></script>
<script src="${JS_DIR}/dev.js"></script>
<script src="${JS_DIR}/sharedList.js"></script>
<script type="text/javascript">
	var topJsonStr = ${requestScope.topFiveList};
	var sharedJsonStr = ${requestScope.sharedCollection};
	var topList = eval(topJsonStr);
	var sharedPublicList = eval(sharedJsonStr);
	
	if(topList && sharedPublicList == "") {
		
		_noLists();
	
	} else {
	
		var sharedList = new SharedList(topList, sharedPublicList);
	}
	
</script>

