<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>${title}</title>
<c:set var="JS_DIR" value="static/js" />
<c:set var="CSS_DIR" value="static/css" />
<c:set var="IMG_DIR" value="static/img" />
<link rel="stylesheet" href="${CSS_DIR}/common.css" />
<link rel="stylesheet" href="${CSS_DIR}/bootstrap.css"/>

</head>
<body>
	<header>
		<div class='menubar'>
			<a href="clickLogo.do" ><span id='logoArea' class='menu'><img src="static/img/logo-m.png"></span></a>
			<a href="songBrowse.do"><span id='songArea' class='menu' >SONG</span></a>
			<a href="sharedList.do" ><span id='listArea' class='menu '>LIST</span></a>
			<div class="drop_list">
				<li id="modifyBtn"><a href="#"><img src="static/img/updateUser.png" id="modifyImg"></a>
					<ul id="modifyUl">
						<li class="modifyLi"><a href="modifyUserPage.do" ><span>PROFILE</span></a></li><hr style="margin:0;">
						<li class="modifyLi"><a href="logout.do"><span>LOG OUT</span></a></li>
					</ul>
				</li>
			</div>
		</div>
	</header><!-- header end -->