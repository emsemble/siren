<!-- 
 1. 파일 이름 : player.jsp
 2. 작성자 : 고상우
 3. 목적 : Player 생성 리퀘스트에 대한 결과 페이지
 4. 이력 : 
 2014. 04. 12. 최초 작성
 2014. 04. 18. jasonPlayer를 회수하여 visualize하는 스크립트 작성
 2014. 04. 24. 스크립트, 스타일 외부 파일로 분리
-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>${title}</title>
<c:set var="JS_DIR" value="static/js" />
<c:set var="CSS_DIR" value="static/css" />
<c:set var="IMG_DIR" value="static/img" />

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="${CSS_DIR}/jquery-ui-1.10.4.css" />
<link rel="stylesheet" href="${CSS_DIR}/bootstrap.css"/>
<link rel="stylesheet" href="${CSS_DIR}/common.css" />
<link rel="stylesheet" href="${CSS_DIR}/player.css" />

<script src="${JS_DIR}/jquery-1.10.1.js"></script>
<script src="${JS_DIR}/jquery-ui-1.10.4.js"></script>
<script src="${JS_DIR}/d3.js"></script>
<script src="${JS_DIR}/three/three.js"></script> 
<script src="${JS_DIR}/three/OrbitControl.js"></script> 
<script src="${JS_DIR}/three/ParticleEngine.js"> </script>
<script src="${JS_DIR}/player.js"></script>

</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ko_KR/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

<div id="loading">
<img id="loadingIcon" src='${IMG_DIR}/Seiren-Player.png'></img>
</div>


<!-- header start -->
<div id="playerHeader">
	<img src="${IMG_DIR}/Seiren-Player-m.png"/>
	<label id='collectionName'></label>
	<div class="share">
		<div class="fb-share-button" data-href="http://localhost/seiren/enterList.do?listNum=${listNum}"  data-type="button_count"></div>
		<a href="https://twitter.com/share" class="twitter-share-button" data-lang="en">Tweet</a>
	</div>
</div>

<div id="content">
	<div id="visualizer">
		<span id='visualizeArea' ></span>
		<span id="linearList"></span>
		
		<div id='info'>
			<label id='collectionNum'></label>
			<label id='collectionOwner'></label>
		</div>
	</div>
</div>
<div id="controller" class="panel"></div> 


<script>
	$(document).ready(function(){
		var listNum = ${listNum};
		_loadPlayer(listNum);
	});
</script>


<br>
<br>
<br>
<%@include file="common/footer.jsp"%>