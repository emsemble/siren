<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<c:set var="title" value="Login" />
<%@include file="common/header.jsp"%>
<link rel="stylesheet" href="${CSS_DIR}/Login.css" />

<div id="container" role="login">
		<div id="content">
		<div id="loginSection">
			<form>	
				<div id="text1">Siren</div>
				<p>
				 <label for="userId" >사용자 아이디 :</label> 
				 <input type="email"  class="loginValues" name="userId" id="userId" placeholder="이메일을 입력하세요"  required pattern="^[a-zA-Z0-9]+@[a-zA-Z0-9]+.+$"><br>
				</p>
				
				<p>
				<label for="userPw" >비밀번호 :</label>
				<input type="password" class="loginValues" name="userPw" id="userPw" required><br>
				</p> 
				
				<a id="text2">아이디/비밀번호를 잊으셧나요?</a>
				<input type="button" value="로그인" id="submitLogin" />
			</form>
		</div>
	</div>
	
	<div id="footer">
	</div>
</div>

<%@include file="common/footer.jsp"%>

<script src="${JS_DIR}/jindo.desktop.all.js"></script>
<script src="${JS_DIR}/loginValidate.js"></script>
<script type="text/javascript">
var loginValidate = new LoginValidate();
</script>