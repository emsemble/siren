# README #

### Summary ###

* [Learn Markdown](https://sion3608@bitbucket.org/emsemble/siren.git)
* Summary 
:  ‘군집 음원 리스팅 서비스’로, 사용자가 원하는 음악들을 리스트에 담으면 그 음악들을 분석해 비슷한 음원끼리 묶어주어 ‘군집화’한 형태로 음악을 제공

### Development SPEC ###

* Language and Library
: JAVA, JSP, Servlet, myBatis, Spring, 
  JavaScript (jQuery, jQuery-UI, Ajax, jindo, autocompleteBehavior
		d3, three, tween, CSSDRender,OrbitControl, ParticleEngine) 
  bootstrap

* DB
: Cubrid 9.1
